package com.ravineteam.uboatnetparser.data.shiplosses;

import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ResourceFilesTest {

    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
    }

    @Test
    public void allResourceFilesAreCorrect() throws ParserConfigurationException, SAXException, IOException {
        int counter = 0;
        for (int year = 1939; year <= 1945; year++) {
            for (int month = 1; month <= 12; month++) {
                if (year == 1939 && month == 1) month = 9;
                String filename = year + "-" + (month > 9 ? "" : "0") + month + ".html";
                File file = new File("src/test/resources/uboatnet/shiplosses/losses/" + filename);
                Document doc = xmlParser.parse(file);
                assertNotNull(doc);
                NodeList titleList = doc.getElementsByTagName("title");
                if (titleList != null && titleList.getLength() > 0) {
                    Node titleNode = titleList.item(0);
                    String title = titleNode.getTextContent();
                    if (StringUtils.isNotEmpty(title)) {
                        title = title.replace("Ship losses by month -", "").replace("- uboat.net", "").trim();
                        String[] parts = title.split(" ");
                        if (parts != null && parts.length == 2) {
                            String yearParsed = parts[1];
                            String monthParsed = "";
                            switch(parts[0]) {
                                case "January" : monthParsed = "01"; break;
                                case "February" : monthParsed = "02"; break;
                                case "March" : monthParsed = "03"; break;
                                case "April" : monthParsed = "04"; break;
                                case "May" : monthParsed = "05"; break;
                                case "June" : monthParsed = "06"; break;
                                case "July" : monthParsed = "07"; break;
                                case "August" : monthParsed = "08"; break;
                                case "September" : monthParsed = "09"; break;
                                case "October" : monthParsed = "10"; break;
                                case "November" : monthParsed = "11"; break;
                                case "December" : monthParsed = "12"; break;
                            }
                            String dateParsed = yearParsed + "-" + monthParsed;
                            assertEquals( filename.replace(".html", ""), dateParsed);
                            counter++;
                        }
                    }
                }
                if (year == 1945 && month == 5) {
                    break;
                }
            }
        }
        assertEquals(69, counter);
    }


}
