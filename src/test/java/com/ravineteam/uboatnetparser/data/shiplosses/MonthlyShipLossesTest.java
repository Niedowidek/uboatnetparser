package com.ravineteam.uboatnetparser.data.shiplosses;

import com.ravineteam.uboatnetparser.parsers.shiplosses.MonthlyLossesParser;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class MonthlyShipLossesTest {

    XmlParser xmlParser;
    MonthlyLossesParser parser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        parser = new MonthlyLossesParser();
    }



    @Test
    public void shouldReturnShipsHitList() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1945-03.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1945, 3, 1), losses.getStartDate());
        assertNotNull(losses.getShipsHit());
        assertEquals(21 , losses.getShipsHit().size());
    }

    @Test
    public void shouldReturnShipsSunk() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1941-03.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1941, 3, 1), losses.getStartDate());
        assertNotNull(losses.getShipsSunk());
        assertEquals(40 , losses.getShipsSunk().size());

    }

    @Test
    public void shouldReturnShipsDamaged() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1942-11.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1942, 11, 1), losses.getStartDate());
        assertNotNull(losses.getShipsDamaged());
        assertEquals(19 , losses.getShipsDamaged().size());
    }

    @Test
    public void shouldReturnShipsSunkTonnage() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1943-09.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1943, 9, 1), losses.getStartDate());
        assertNotNull(losses.getShipsSunkTonnage());
        assertEquals(98476 , losses.getShipsSunkTonnage());

    }

    @Test
    public void shouldReturnShipsDamagedTonnage() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1944-01.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1944, 1, 1), losses.getStartDate());
        assertNotNull(losses.getShipsDamagedTonnage());
        assertEquals(16901 , losses.getShipsDamagedTonnage());

    }

    @Test
    public void shouldReturnNumberOfShipsHitByMine() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-09.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1939, 9, 1), losses.getStartDate());
        assertNotNull(losses.getHitByMine());
        assertEquals(6 , losses.getHitByMine().size());

    }

    @Test
    public void shouldReturnNumberOfShipsDamagedByMine() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-09.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1939, 9, 1), losses.getStartDate());
        assertNotNull(losses.getDamagedByMine());
        assertEquals(1 , losses.getDamagedByMine().size());

    }

    @Test
    public void shouldReturnNumberOfShipsSunkByMine() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-09.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1939, 9, 1), losses.getStartDate());
        assertNotNull(losses.getSunkByMine());
        assertEquals(5 , losses.getSunkByMine().size());

    }

    @Test
    public void shouldReturnNumberOfShipsCaptured() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-11.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1939, 11, 1), losses.getStartDate());
        assertNotNull(losses.getCaptured());
        assertEquals(1, losses.getCaptured().size());

    }

    @Test
    public void shouldReturnListOfShipsTotalLoss() throws Exception {

        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1940-02.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(LocalDate.of(1940, 2, 1), losses.getStartDate());
        assertNotNull(losses.getTotalLoss());
        assertEquals(2 , losses.getTotalLoss().size());
    }

}