package com.ravineteam.uboatnetparser.utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DateConverterTest {

    private DateConverter converter;

    @Before
    public void setUp()  {
        converter = new DateConverter();
    }

    @Test
    public void convert_d_MMM_yyyy_to_yyyyMMdd() {
        String date1 = "30 May 1938";
        String date2 = "1 Aug 1940";
        String dateConverted1 = converter.to_yyyyMMdd(date1);
        String dateConverted2 = converter.to_yyyyMMdd(date2);
        assertNotNull(dateConverted1);
        assertNotNull(dateConverted2);
        assertEquals("1938-05-30", dateConverted1);
        assertEquals("1940-08-01", dateConverted2);
    }

    @Test
    public void convert_d_MMM_yyyy_to_yyyyMMdd_null() {
        String dateConverted1 = converter.to_yyyyMMdd(null);
        assertNotNull(dateConverted1);
        assertEquals("", dateConverted1);
    }

    @Test
    public void convert_d_MMM_yyyy_to_yyyyMMdd_emptyString() {
        String dateConverted1 = converter.to_yyyyMMdd("");
        assertNotNull(dateConverted1);
        assertEquals("", dateConverted1);
    }

    @Test
    public void convert_d_MMM_yyyy_to_yyyyMMdd_notDate() {
        String dateConverted1 = converter.to_yyyyMMdd("notDate");
        assertNotNull(dateConverted1);
        assertEquals("", dateConverted1);
    }
}