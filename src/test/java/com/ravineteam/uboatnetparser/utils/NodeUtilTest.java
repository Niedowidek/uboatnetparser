package com.ravineteam.uboatnetparser.utils;

import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class NodeUtilTest {


    private  NodeUtil util;

    @Before
    public void setUp()  {
        util = new NodeUtil();
    }

    @Test
    public void getAttributeValueTest() throws ParserConfigurationException, SAXException, IOException {
        XmlParser xmlParser = new XmlParser();
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        NodeList nodes = doc.getElementsByTagName("node");
        String value = util.getAttributeValue(nodes.item(0), "type");
        assertEquals("any", value);
    }
}