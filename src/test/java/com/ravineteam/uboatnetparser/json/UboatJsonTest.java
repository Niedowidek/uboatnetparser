package com.ravineteam.uboatnetparser.json;

import com.ravineteam.uboatnetparser.data.uboat.Uboat;
import com.ravineteam.uboatnetparser.parsers.uboat.UboatParser;
import com.ravineteam.uboatnetparser.parsers.uboat.UboatParserTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UboatJsonTest {

    private UboatJson json;
    private UboatParserTest uboatParserTest;

    @Before
    public void setUp() throws IOException, SAXException, ParserConfigurationException {
        json = new UboatJson();
        uboatParserTest = new UboatParserTest();
        uboatParserTest.setUp();
    }

    @Ignore
    @Test
    public void writeJson()  {
        File file = new File("src/test/resources/output/uboat.json");
        Uboat uboat = uboatParserTest.getTestUboat();
        assertNotNull(uboat);
        try (OutputStream os = new FileOutputStream(file)) {
            json.writeJson(uboat, os );
        }  catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(file.exists());
    }

    @Test
    public void writeU96json() throws ParserConfigurationException, SAXException, IOException {
        File file = new File("src/test/resources/output/uboat.json");
        Uboat uboat = new UboatParser().getUboat(new File("src/test/resources/uboatnet/boats/u96.htm"));
        assertNotNull(uboat);
        try (OutputStream os = new FileOutputStream(file)) {
            json.writeJson(uboat, os );
        }  catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(file.exists());
    }
}