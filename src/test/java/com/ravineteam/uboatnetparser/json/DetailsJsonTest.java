package com.ravineteam.uboatnetparser.json;

import com.ravineteam.uboatnetparser.data.patrol.Details;
import com.ravineteam.uboatnetparser.parsers.patrol.DetailsParser;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DetailsJsonTest {

    private DetailsJson json;

    @Before
    public void setUp() throws IOException, SAXException, ParserConfigurationException {
        json = new DetailsJson();
    }



    @Test
    public void writeU9DetailsJson() throws ParserConfigurationException, SAXException, IOException {
        File output = new File("src/test/resources/output/details.json");
        Details details = new DetailsParser().parse(new XmlParser().parse(new File("src/test/resources/uboatnet/details/details.php@boat=9_date=1943-10-19.html")));
        assertNotNull(details);
        try (OutputStream os = new FileOutputStream(output)) {
            json.writeJson(details, os);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(output.exists());
    }

}