package com.ravineteam.uboatnetparser.parsers.crews;

import com.ravineteam.uboatnetparser.data.crews.Crew;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class CrewParserTest {

    CrewParser crewParser;
    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        crewParser = new CrewParser();
    }

    @Test
    public void shouldParseX_39crew() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/crews/X_39.html"));
        assertNotNull(doc);
        Crew crew = crewParser.parse(doc);
        assertNotNull(crew);
        assertTrue(crew.isNotEmpty());
        assertEquals(100, crew.getTrainedCommanderList().size());
        assertEquals("Baldus, Hugo", crew.getTrainedCommanderList().get(6).getName());
        assertEquals("Kptlt.", crew.getTrainedCommanderList().get(53).getRank());
        assertEquals(2, crew.getTrainedCommanderList().get(84).getCommandList().size());
        assertEquals("U-621", crew.getTrainedCommanderList().get(84).getCommandList().get(1).getName());
        assertEquals("/boats/u1007.htm", crew.getTrainedCommanderList().get(93).getCommandList().get(2).getHref());
        assertEquals("X/39", crew.getName());
    }

    @Test
    public void shouldParseI_42crew() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/crews/I_42.html"));
        assertNotNull(doc);
        Crew crew = crewParser.parse(doc);
        assertNotNull(crew);
        assertTrue(crew.isNotEmpty());
        assertEquals(1, crew.getTrainedCommanderList().size());
        assertEquals("Strübing, Werner", crew.getTrainedCommanderList().get(0).getName());
        assertEquals("Oblt.", crew.getTrainedCommanderList().get(0).getRank());
        assertEquals(1, crew.getTrainedCommanderList().get(0).getCommandList().size());
        assertEquals("U-1003", crew.getTrainedCommanderList().get(0).getCommandList().get(0).getName());
        assertEquals("/boats/u1003.htm", crew.getTrainedCommanderList().get(0).getCommandList().get(0).getHref());
        assertEquals("I/42", crew.getName());
    }
}