package com.ravineteam.uboatnetparser.parsers.men;

import com.ravineteam.uboatnetparser.data.men.Man;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class MenParserTest {
    
    MenParser menParser;
    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        menParser = new MenParser();
        xmlParser = new XmlParser();
    }

    @Test
    public void shouldParseWilhelmAmbrosius() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/men/commanders/12.html"));
        assertNotNull(doc);
        Man man = menParser.parse(doc);
        assertNotNull(man);
        assertTrue(man.isNotEmpty());
        assertEquals("Wilhelm Ambrosius", man.getName());
        assertNotNull(man.getTonnage());
        assertEquals(8, man.getTonnage().getShipsSunk());
        assertEquals(47030, man.getTonnage().getShipsSunkTonnage());
        assertEquals("Crew 26", man.getCrewName());
        assertEquals("/men/commanders/crews.html?crew=26", man.getCrewHref());
    }

    @Test
    public void shouldParseUlrichAbel() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/men/commanders/1.html"));
        assertNotNull(doc);
        Man man = menParser.parse(doc);
        assertNotNull(man);
        assertTrue(man.isNotEmpty());
        assertEquals("Dr. Ulrich Abel", man.getName());
        assertNull(man.getTonnage());
        assertNull(man.getCrewName());
        assertNull(man.getCrewHref());
    }

    @Test
    public void shouldParseWillenbrock() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/men/commanders/willenbrock.html"));
        assertNotNull(doc);
        Man man = menParser.parse(doc);
        assertNotNull(man);
        assertTrue(man.isNotEmpty());
        assertEquals("Heinrich Lehmann-Willenbrock", man.getName());
        assertNotNull(man.getTonnage());
        assertEquals(24, man.getTonnage().getShipsSunk());
        assertEquals(170237, man.getTonnage().getShipsSunkTonnage());
        assertEquals(2, man.getTonnage().getShipsDamaged());
        assertEquals(15864, man.getTonnage().getShipsDamagedTonnage());
        assertEquals(1, man.getTonnage().getShipsTotalLoss());
        assertEquals(8888, man.getTonnage().getShipsTotalLossTonnage());
        assertEquals("Crew 31", man.getCrewName());
        assertEquals("/men/commanders/crews.html?crew=31", man.getCrewHref());
    }
}