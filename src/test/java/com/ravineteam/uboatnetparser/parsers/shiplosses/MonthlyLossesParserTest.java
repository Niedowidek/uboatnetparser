package com.ravineteam.uboatnetparser.parsers.shiplosses;

import com.ravineteam.uboatnetparser.data.shiplosses.MonthlyShipLosses;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MonthlyLossesParserTest {

    MonthlyLossesParser parser;
    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        parser = new MonthlyLossesParser();
        xmlParser = new XmlParser();
    }

    @Test
    public void shouldReturnMonthlyShipLossesObject() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-09.html"));
        MonthlyShipLosses losses = parser.parse(doc);
        assertTrue(losses.isNotEmpty());
        assertEquals(52 , losses.getNumberOfAllShipsHit());
    }



    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenParsingNull() throws Exception {

        MonthlyShipLosses losses = parser.parse(null);

    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenParsingWrongDocument() throws Exception {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/allies/merchants.html"));
        MonthlyShipLosses losses = parser.parse(doc);


    }
}