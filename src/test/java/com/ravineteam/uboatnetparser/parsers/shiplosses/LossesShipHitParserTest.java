package com.ravineteam.uboatnetparser.parsers.shiplosses;

import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class LossesShipHitParserTest {

    LossesShipHitParser parser;
    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        parser = new LossesShipHitParser();
    }

    @Test
    public void shouldParseSeptember1939() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/shiplosses/losses/1939-09.html"));
        List<ShipHit> shipHitList = parser.parse(doc);

        assertNotNull(shipHitList);
        assertEquals(52, shipHitList.size());
        assertEquals("6 Sep 1939", shipHitList.get(4).getDateLabel());
        assertEquals("Wilhelm Rollmann", shipHitList.get(9).getCommander());
        assertEquals("U-13", shipHitList.get(11).getUboat());
        assertEquals("/boats/u48.htm", shipHitList.get(15).getUboatHref());
        assertEquals("/men/schuhart.htm", shipHitList.get(17).getCommanderHref());
        assertTrue(shipHitList.get(21).isHasPicture());
        assertEquals("Cheyenne", shipHitList.get(24).getShipName());
        assertTrue(shipHitList.get(25).isDamaged());
        assertTrue(shipHitList.get(25).isHitByMine());
        assertEquals("/allies/merchants/ship/27.html", shipHitList.get(27).getShipHref());
        assertEquals(1099, shipHitList.get(29).getTonnage());
        assertEquals("/media/images/flags/small/flag_finland_s.png", shipHitList.get(30).getNationalityHref());
        assertTrue(shipHitList.get(49).isTotalLoss());
        assertEquals("da", shipHitList.get(51).getNationality());

    }
}