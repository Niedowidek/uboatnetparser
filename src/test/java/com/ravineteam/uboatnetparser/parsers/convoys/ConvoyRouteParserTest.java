package com.ravineteam.uboatnetparser.parsers.convoys;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.convoys.ConvoyRoute;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class ConvoyRouteParserTest {

    ConvoyRouteParser parser;
    XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        parser = new ConvoyRouteParser();
    }

    @Test
    public void shouldParseHXRoute() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/convoys/routes/hx.html"));
        ConvoyRoute route = parser.parse(doc);

        assertNotNull(route);
        assertTrue(route.isNotEmpty());
        assertEquals("HX", route.getCode());
        assertEquals("Homeward from Halifax", route.getRouteDescription());
        assertEquals("North Atlantic", route.getArea());
        assertEquals(("From September 1939 to September 1942 from Halifax, then to May 1945 from New York. From July 1940 entry via North Channel, from October 1944 south of Ireland again.\n" +
                "Fast convoys from Halifax (later New York) to the UK, only ships sailing faster than 9 knots.\n" +
                "HX-129 was the first convoy escorted the entire convoy-route.").replace("\n", " "), route.getNotes());
        assertNotNull(route.getRoute());
        assertEquals(17, route.getRoute().length);
        assertEquals(-73.50, route.getRoute()[0].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(40.30,route.getRoute()[1].getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-40.00, route.getRoute()[8].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(55.53,route.getRoute()[12].getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-5.07, route.getRoute()[15].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(53.46,route.getRoute()[16].getLatitude(), Position.ALLOWED_ERROR);
    }

    @Test
    public void shouldReturnEmptyObjectWhenNoRoute() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/convoys/routes/btc.html"));
        ConvoyRoute route = parser.parse(doc);

        assertNotNull(route);
        assertTrue(route.isEmpty());
        assertEquals("BTC", route.getCode());
        assertEquals("UK coastal convoys", route.getRouteDescription());
        assertEquals("UK coastal waters", route.getArea());
        assertEquals("Eastbound convoys through the English Channel.", route.getNotes());
        assertNotNull(route.getRoute());
        assertEquals(0, route.getRoute().length);
    }

    @Test
    public void shouldParsePQRoute() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/convoys/routes/pq.html"));
        ConvoyRoute route = parser.parse(doc);

        assertNotNull(route);
        assertTrue(route.isNotEmpty());
        assertEquals("PQ", route.getCode());
        assertEquals("Iceland - Russia", route.getRouteDescription());
        assertEquals("Arctic Ocean", route.getArea());
        assertEquals(("Convoys with Lend-Lease cargo for the Soviet Union, were replaced by the JW convoys.\n" +
                "From September 1941 to December 1942, then continued as JW.").replace("\n", " "), route.getNotes());
        assertNotNull(route.getRoute());
        assertEquals(17, route.getRoute().length);
        assertEquals(-5.47, route.getRoute()[0].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(58.51,route.getRoute()[1].getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-22.45, route.getRoute()[5].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(75.50,route.getRoute()[10].getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(41.40, route.getRoute()[14].getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(64.59,route.getRoute()[16].getLatitude(), Position.ALLOWED_ERROR);

        Position[] murmansk = route.getRouteVariation(0);
        assertNotNull(route.getRouteVariation(0));
        assertEquals(2, murmansk.length);
        assertEquals(71.00, murmansk[0].getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(34.00, murmansk[1].getLongitude(), Position.ALLOWED_ERROR);
    }
}