package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FinalPositionParserTest {

    private FinalPositionParser parser;
    private XmlParser xmlParser;
    private Document docU48;
    private Document docU96;

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, IOException {
        parser = new FinalPositionParser();
        xmlParser = new XmlParser();
        docU48 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U48_XML));
        docU96 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
    }

    @Test
    public void getFinalPosition() {
        Position pos = parser.getFinalPosition(docU48);
        assertNotNull(pos);
        assertEquals(54.12, pos.getLatitude(), 0.0);
        assertEquals(10.83, pos.getLongitude(), 0.0);

    }

    @Test
    public void getFinalPositionU96() {
        Position pos = parser.getFinalPosition(docU96);
        assertNotNull(pos);
        assertEquals(53.52, pos.getLatitude(), 0.0);
        assertEquals(8.17, pos.getLongitude(), 0.0);

    }
}