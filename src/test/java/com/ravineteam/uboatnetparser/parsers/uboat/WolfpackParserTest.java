package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.uboat.WolfpackService;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WolfpackParserTest {

    private WolfpackParser parser;
    private XmlParser xmlParser;
    private Document doc;


    @Before
    public void setUp() throws Exception {
        parser = new WolfpackParser();
        xmlParser = new XmlParser();
        doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
    }

    @Test
    public void getWolfpacks() {
        List<WolfpackService> wolfpacks = parser.parse(doc);
        assertNotNull(wolfpacks);
        assertEquals(11, wolfpacks.size());
        assertEquals("Kurfürst", wolfpacks.get(2).getName());
        assertEquals("/ops/wolfpacks/31.html", wolfpacks.get(7).getHref());
        assertEquals("1941-08-05", wolfpacks.get(0).getDateFromFormatted());
        assertEquals("1943-01-20", wolfpacks.get(10).getDateToFormatted());
    }
}