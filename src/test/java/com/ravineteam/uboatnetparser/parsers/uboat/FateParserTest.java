package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.uboat.Fate;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FateParserTest {

    private FateParser parser;
    private String u96 = "Decommissioned on 15 February 1945 at Wilhelmshaven.\n" +
            "Sunk on 30 March 1945 in the Hipper Basin at Wilhelmshaven by\n" +
            "bombs during US air raid (8th AF).";
    private String u96dec = "Decommissioned on 15 February 1945 at Wilhelmshaven.\n";
    private String u96sunk = "Sunk on 30 March 1945 in the Hipper Basin at Wilhelmshaven by\n" +
            "bombs during US air raid (8th AF).";
    private String u48 = "Decommissioned on 25 September 1943 and used as instructional\n" +
            "boat.\n" +
            "Scuttled on 3\n" +
            "May 1945 at Neustadt.";
    private String u48dec = "Decommissioned on 25 September 1943 and used as instructional\n" +
            "boat.\n";
    private String u48scut = "Scuttled on 3\n" +
            "May 1945 at Neustadt.";
    private XmlParser xmlParser;
    private Document doc;

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, IOException {
        parser = new FateParser();
        xmlParser = new XmlParser();
        doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U48_XML));
    }

    @Test
    public void getFateLabel() {
        assertEquals("Decommissioned",parser.getLabel(u96dec));
    }

    @Test
    public void getFateWhen() {
        assertEquals("15 February 1945", parser.getWhen(u96dec));
    }

    @Test
    public void getFateLabel_null() {
        assertEquals("",parser.getLabel(null));
    }

    @Test
    public void getFateWhen_null() {
        assertEquals("", parser.getWhen(null));
    }

    @Test
    public void getFateLabel_empty() {
        assertEquals("",parser.getLabel(""));
    }

    @Test
    public void getFateWhen_empty() {
        assertEquals("", parser.getWhen(""));
    }

    @Test
    public void getDescriptions() throws ParserConfigurationException, SAXException, IOException {
        XmlParser xmlParser = new XmlParser();
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        List<String> descriptions = parser.getDescriptions(doc);

        assertNotNull(descriptions);
        assertEquals(2, descriptions.size());
        assertEquals("Decommissioned on 15 February 1945 at Wilhelmshaven.", descriptions.get(0));
        assertEquals("Sunk on 30 March 1945 in the Hipper Basin at Wilhelmshaven by bombs during US air raid (8th AF).", descriptions.get(1));
    }

    @Test
    public void getDescriptionsU48()  {
        List<String> descriptions = parser.getDescriptions(doc);

        assertNotNull(descriptions);
        assertEquals(2, descriptions.size());
        assertEquals("Decommissioned on 25 September 1943 and used as instructional boat.", descriptions.get(0));
        assertEquals("Scuttled on 3 May 1945 at Neustadt.", descriptions.get(1));
    }

    @Test
    public void getDescriptions_docWithoutProperNode() throws ParserConfigurationException, SAXException, IOException {
        XmlParser xmlParser = new XmlParser();
        Document wrongDoc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        List<String> descriptions = parser.getDescriptions(wrongDoc);

        assertNotNull(descriptions);
        assertEquals(0, descriptions.size());
    }

    @Test
    public void getDescriptions_null()  {
        List<String> descriptions = parser.getDescriptions(null);

        assertNotNull(descriptions);
        assertEquals(0, descriptions.size());
    }

    @Test
    public void getFatesU96() {
        List<Fate> fates = parser.getFates(doc);

        assertNotNull(fates);
        assertEquals(2, fates.size());
        assertEquals("Decommissioned on 25 September 1943 and used as instructional boat.", fates.get(0).getDescription());
        assertEquals("Decommissioned",fates.get(0).getLabel());
        assertEquals("25 September 1943", fates.get(0).getWhen());

        assertEquals("Scuttled on 3 May 1945 at Neustadt.", fates.get(1).getDescription());
        assertEquals("Scuttled",fates.get(1).getLabel());
        assertEquals("3 May 1945", fates.get(1).getWhen());
    }
}