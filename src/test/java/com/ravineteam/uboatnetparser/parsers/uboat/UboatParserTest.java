package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.data.uboat.AssignedCommander;
import com.ravineteam.uboatnetparser.data.uboat.Fate;
import com.ravineteam.uboatnetparser.data.uboat.ServedFlotilla;
import com.ravineteam.uboatnetparser.data.uboat.Uboat;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.utils.TagFeeder;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Unit test for simple UboatParser.
 */
public class UboatParserTest
{

    public static final String UBOAT_HREF_PREFIX = "boats" + File.separator;
    private TagFeeder feeder;
    private XmlParser xmlParser;
    private UboatParser uboatParser;
    private DateConverter dateConverter;
    private Document doc;
    private Document wrongDoc;
    private Document docU792;

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, IOException {
        feeder = new TagFeeder();
        xmlParser = new XmlParser();
        uboatParser = new UboatParser();
        dateConverter = new DateConverter();
        doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        wrongDoc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        docU792 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U792_XML));
    }

    @Test
    public void getNumber() throws Exception {
        assertEquals("U-96", uboatParser.getBoatNumber(doc));
    }

    @Test
    public void getTypeValue() {
        String type = uboatParser.getTypeValue(doc);
        assertEquals("VIIC", type);
    }

    @Test
    public void getTypeValue_nullDoc() {
        String type = uboatParser.getTypeValue(null);
        assertEquals("", type);
    }

    @Test
    public void getTypeValue_docWithoutSearchedNode() throws ParserConfigurationException, SAXException, IOException {
        Document doc1 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        String type = uboatParser.getTypeValue(doc1);
        assertEquals("", type);
    }

    @Test
    public void getOrderedValue() {
        String ordered = uboatParser.getOrderedValue(doc);
        assertNotNull(ordered);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH);
        LocalDate ld2 = LocalDate.parse(ordered, dtf);
        assertEquals("30 May 1938", ordered);
        assertEquals("1938-05-30", ld2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH)));
    }

    @Test
    public void getOrderedValue_nullDoc() {
        String ordered = uboatParser.getOrderedValue(null);
        assertEquals("", ordered);
    }

    @Test
    public void getOrderedValue_docWithoutSearchedNode() throws ParserConfigurationException, SAXException, IOException {
        Document doc1 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        String ordered = uboatParser.getOrderedValue(doc1);
        assertEquals("", ordered);
    }

    @Test
    public void getLaidDownWhenValue() {
        String laidDown = uboatParser.getLaidDownWhenValue(doc);
        assertNotNull(laidDown);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH);
        LocalDate ld2 = LocalDate.parse(laidDown, dtf);
        assertEquals("16 Sep 1939", laidDown);
        assertEquals("1939-09-16", ld2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH)));
    }

    @Test
    public void getLaidDownWhenValue_docNull() {
        String laidDown = uboatParser.getLaidDownWhenValue(null);
        assertEquals("", laidDown);
    }

    @Test
    public void getLaidDownWhenValue_docWithoutSearchedNode() throws ParserConfigurationException, SAXException, IOException {
        Document doc1 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML));
        String laidDown = uboatParser.getLaidDownWhenValue(doc1);
        assertEquals("", laidDown);
    }

    @Test
    public void getLaidDownWhere() {
        String laidDownWhere = uboatParser.getLaidDownWhere(doc);
        assertEquals("F. Krupp Germaniawerft AG, Kiel (werk 601)", laidDownWhere);
    }

    @Test
    public void getLaidDownWhere_docNull() {
        String laidDownWhere = uboatParser.getLaidDownWhere(null);
        assertEquals("", laidDownWhere);
    }

    @Test
    public void getLaidDownWhere_docWithoutSearchedNode() throws ParserConfigurationException, SAXException, IOException {
        String laidDownWhere = uboatParser.getLaidDownWhere(xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_TESTFILE_XML)));
        assertEquals("", laidDownWhere);
    }

    @Test
    public void getLaunchedValue() {
        String launched = uboatParser.getLaunchedValue(doc);
        assertNotNull(launched);
        assertEquals("1 Aug 1940", launched);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH);
        LocalDate ld2 = LocalDate.parse(launched, dtf);
        assertEquals("1 Aug 1940", launched);
        assertEquals("1940-08-01", ld2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH)));
    }

    @Test
    public void getLaunchedValue_nullDoc() {
        String launched = uboatParser.getLaunchedValue(null);
        assertEquals("", launched);
    }

    @Test
    public void getLaunchedValue_docWithoutSearchedNode() {
        String launched = uboatParser.getLaunchedValue(wrongDoc);
        assertEquals("", launched);
    }

    @Test
    public void getCommissionedWhenValue() {
        String commissioned = uboatParser.getCommissionedWhenValue(doc);
        assertEquals("14 Sep 1940", commissioned);
    }

    @Test
    public void getCommissionedWhenValue_nullDoc() {
        String commissioned = uboatParser.getCommissionedWhenValue(null);
        assertEquals("", commissioned);
    }

    @Test
    public void getCommissionedWhenValue_docWithoutSearchedNode() {
        String commissioned = uboatParser.getCommissionedWhenValue(wrongDoc);
        assertEquals("", commissioned);
    }

    @Test
    public void getCommissionedCommander() {
        String commander = uboatParser.getCommissionedCommanderValue(doc);
        assertEquals("Kptlt. Heinrich Lehmann-Willenbrock", commander);
    }

    @Test
    public void getCommissionedCommander_nullDoc() {
        String commander = uboatParser.getCommissionedCommanderValue(null);
        assertEquals("", commander);
    }

    @Test
    public void getCommissionedCommander_docWithoutSearchedNode() {
        String commander = uboatParser.getCommissionedCommanderValue(wrongDoc);
        assertEquals("", commander);
    }

    @Test
    public void getCommanders() {
        List<AssignedCommander> commanders = uboatParser.getCommanders(doc);
        assertTrue(commanders != null && commanders.size() == 5);

        assertEquals("willenbrock", commanders.get(0).getCommanderId());
        assertEquals("hellriegel", commanders.get(1).getCommanderId());
        assertEquals("commanders_918", commanders.get(2).getCommanderId());
        assertEquals("commanders_1356", commanders.get(3).getCommanderId());
        assertEquals("commanders_1012", commanders.get(4).getCommanderId());

        assertEquals("14 Sep 1940", commanders.get(0).getDateFrom());
        assertEquals("28 Mar 1942", commanders.get(1).getDateFrom());
        assertEquals("16 Mar 1943", commanders.get(2).getDateFrom());
        assertEquals("Feb, 1944", commanders.get(3).getDateFrom());
        assertEquals("1 Jul 1944", commanders.get(4).getDateFrom());

        assertEquals("1 Apr 1942", commanders.get(0).getDateTo());
        assertEquals("15 Mar 1943", commanders.get(1).getDateTo());
        assertEquals("30 Jun 1944", commanders.get(2).getDateTo());
        assertEquals("Jun, 1944", commanders.get(3).getDateTo());
        assertEquals("15 Feb 1945", commanders.get(4).getDateTo());

        assertEquals("Kptlt.", commanders.get(0).getRank());
        assertEquals("Oblt.", commanders.get(1).getRank());
        assertEquals("Oblt.", commanders.get(2).getRank());
        assertEquals("Oblt.", commanders.get(3).getRank());
        assertEquals("Oblt. (R)", commanders.get(4).getRank());

        assertEquals("8", commanders.get(0).getDecorationIds().get(0));
        assertEquals("8", commanders.get(1).getDecorationIds().get(0));

    }

    @Test
    public void getCommandersU792() throws ParserConfigurationException, SAXException, IOException {
        Document doc1 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U792_XML));
        List<AssignedCommander> commanders = uboatParser.getCommanders(doc1);
        assertTrue(commanders != null && commanders.size() == 2);

        assertEquals("commanders_451", commanders.get(0).getCommanderId());
        assertEquals("commanders_223", commanders.get(1).getCommanderId());

        assertEquals("16 Nov 1943", commanders.get(0).getDateFrom());
        assertEquals("Dec, 1944", commanders.get(1).getDateFrom());

        assertEquals("Dec, 1944", commanders.get(0).getDateTo());
        assertEquals("4 May 1945", commanders.get(1).getDateTo());

        assertEquals("Oblt.", commanders.get(0).getRank());
        assertEquals("Oblt.", commanders.get(1).getRank());
    }

    @Test
    public void getCommanders_docNull() {
        List<AssignedCommander> commanders = uboatParser.getCommanders(null);
        assertEquals(0, commanders.size());
    }

    @Test
    public void getCommanders_docWithoutSearchedNode() {
        List<AssignedCommander> commanders = uboatParser.getCommanders(wrongDoc);
        assertEquals(0, commanders.size());
    }

    @Test
    public void getFlotillas() {
        List<ServedFlotilla> flotillas = uboatParser.getFlotillas(doc);

        assertNotNull(flotillas);
        assertEquals(flotillas.size(), 4);

        assertEquals(flotillas.get(0).getDateFrom(), "14 Sep 1940");
        assertEquals(flotillas.get(0).getDateTo(), "30 Nov 1940");
        assertEquals(flotillas.get(0).getId(), "7flo");
        assertEquals(flotillas.get(0).getTypeOfDuty(), "training");

        assertEquals(flotillas.get(1).getDateFrom(), "1 Dec 1940");
        assertEquals(flotillas.get(1).getDateTo(), "31 Mar 1943");
        assertEquals(flotillas.get(1).getId(), "7flo");
        assertEquals(flotillas.get(1).getTypeOfDuty(), "active service");

        assertEquals(flotillas.get(2).getDateFrom(), "1 Apr 1943");
        assertEquals(flotillas.get(2).getDateTo(), "30 Jun 1944");
        assertEquals(flotillas.get(2).getId(), "24flo");
        assertEquals(flotillas.get(2).getTypeOfDuty(), "training");

        assertEquals(flotillas.get(3).getDateFrom(), "1 Jul 1944");
        assertEquals(flotillas.get(3).getDateTo(), "15 Feb 1945");
        assertEquals(flotillas.get(3).getId(), "22flo");
        assertEquals(flotillas.get(3).getTypeOfDuty(), "school boat");
    }

    @Test
    public void getFlotillas_docNull() {
        List<ServedFlotilla> flotillas = uboatParser.getFlotillas(null);
        assertEquals(0, flotillas.size());
    }

    @Test
    public void getFlotillas_docWithoutSearchedNode() {
        List<ServedFlotilla> flotillas = uboatParser.getFlotillas(wrongDoc);
        assertEquals(0, flotillas.size());
    }

    @Test
    public void getFlotillasU792()  {
        List<ServedFlotilla> flotillas = uboatParser.getFlotillas(docU792);

        assertNotNull(flotillas);
        assertEquals(3, flotillas.size());

        assertEquals(flotillas.get(0).getDateFrom(), "16 Nov 1943");
        assertEquals(flotillas.get(0).getDateTo(), "30 Nov 1943");
        assertEquals(flotillas.get(0).getId(), "5flo2");
        assertEquals(flotillas.get(0).getTypeOfDuty(), "trial boat");

        assertEquals(flotillas.get(1).getDateFrom(), "1 Dec 1943");
        assertEquals(flotillas.get(1).getDateTo(), "15 Feb 1945");
        assertEquals(flotillas.get(1).getId(), "8flo");
        assertEquals(flotillas.get(1).getTypeOfDuty(), "trial boat");

        assertEquals(flotillas.get(2).getDateFrom(), "16 Feb 1945");
        assertEquals(flotillas.get(2).getDateTo(), "4 May 1945");
        assertEquals(flotillas.get(2).getId(), "5flo2");
        assertEquals(flotillas.get(2).getTypeOfDuty(), "trial boat");
    }

    @Test
    public void getSuccesses() {
        Tonnage successes = uboatParser.getSuccesses(doc);

        assertNotNull(successes);
        assertEquals(27, successes.getShipsSunk());
        assertEquals(181206, successes.getShipsSunkTonnage());
        assertEquals(4, successes.getShipsDamaged());
        assertEquals(33043, successes.getShipsDamagedTonnage());
        assertEquals(1, successes.getShipsTotalLoss());
        assertEquals(8888, successes.getShipsTotalLossTonnage());

    }

    @Test
    public void getSuccessesU48() throws ParserConfigurationException, SAXException, IOException {
        Document doc1 = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U48_XML));
        Tonnage successes = uboatParser.getSuccesses(doc1);

        assertNotNull(successes);
        assertEquals(51, successes.getShipsSunk());
        assertEquals(306874, successes.getShipsSunkTonnage());
        assertEquals(3, successes.getShipsDamaged());
        assertEquals(20480, successes.getShipsDamagedTonnage());
        assertEquals(0, successes.getShipsTotalLoss());
        assertEquals(0, successes.getShipsTotalLossTonnage());

        assertEquals(1, successes.getWarshipsSunk());
        assertEquals(1060, successes.getWarshipsSunkTonnage());
        assertEquals(0, successes.getWarshipsDamaged());
        assertEquals(0, successes.getWarshipsDamagedTonnage());
        assertEquals(0, successes.getWarshipsTotalLoss());
        assertEquals(0, successes.getWarshipsTotalLossTonnage());

    }

    @Test
    public void getSuccesses_docNull() {
        Tonnage successes = uboatParser.getSuccesses(null);
        assertNotNull(successes);
        assertEquals(0, successes.getShipsSunk());
        assertEquals(0, successes.getShipsSunkTonnage());
        assertEquals(0, successes.getShipsDamaged());
        assertEquals(0, successes.getShipsDamagedTonnage());
        assertEquals(0, successes.getShipsTotalLoss());
        assertEquals(0, successes.getShipsTotalLossTonnage());

        assertEquals(0, successes.getWarshipsSunk());
        assertEquals(0, successes.getWarshipsSunkTonnage());
        assertEquals(0, successes.getWarshipsDamaged());
        assertEquals(0, successes.getWarshipsDamagedTonnage());
        assertEquals(0, successes.getWarshipsTotalLoss());
        assertEquals(0, successes.getWarshipsTotalLossTonnage());
    }

    @Test
    public void getSuccesses_docWithoutSearchedNode() {
        Tonnage successes = uboatParser.getSuccesses(wrongDoc);
        assertNotNull(successes);
        assertEquals(0, successes.getShipsSunk());
        assertEquals(0, successes.getShipsSunkTonnage());
        assertEquals(0, successes.getShipsDamaged());
        assertEquals(0, successes.getShipsDamagedTonnage());
        assertEquals(0, successes.getShipsTotalLoss());
        assertEquals(0, successes.getShipsTotalLossTonnage());

        assertEquals(0, successes.getWarshipsSunk());
        assertEquals(0, successes.getWarshipsSunkTonnage());
        assertEquals(0, successes.getWarshipsDamaged());
        assertEquals(0, successes.getWarshipsDamagedTonnage());
        assertEquals(0, successes.getWarshipsTotalLoss());
        assertEquals(0, successes.getWarshipsTotalLossTonnage());
    }

    @Test
    public void getFate() {
        List<Fate> fates = uboatParser.getFates(doc);

        assertNotNull(fates);

        assertNotNull(fates.get(0));
        assertEquals("Decommissioned", fates.get(0).getLabel());
        assertEquals("15 February 1945", fates.get(0).getWhen());
        assertEquals("Decommissioned on 15 February 1945 at Wilhelmshaven.", fates.get(0).getDescription());

        assertNotNull(fates.get(1));
        assertEquals("Sunk", fates.get(1).getLabel());
        assertEquals("30 March 1945", fates.get(1).getWhen());
        assertEquals("Sunk on 30 March 1945 in the Hipper Basin at Wilhelmshaven by bombs during US air raid (8th AF).", fates.get(1).getDescription());
    }

    @Test
    public void getFate_null() {
        List<Fate> fates = uboatParser.getFates(null);

        assertNotNull(fates);
        assertEquals(0, fates.size());
    }

    @Test
    public void getFate_docWithoutProperNode() {
        List<Fate> fates = uboatParser.getFates(wrongDoc);

        assertNotNull(fates);
        assertEquals(0, fates.size());
    }

    @Test
    public void getFinalPosition() {
        Position pos = uboatParser.getFinalPosition(doc);

        assertNotNull(pos);
        assertEquals(53.52, pos.getLatitude(), 0.0);
        assertEquals(8.17, pos.getLongitude(), 0.0);
    }

    @Test
    public void getFinalPosition_null() {
        Position pos = uboatParser.getFinalPosition(null);
        assertNull(pos);
    }

    @Test
    public void getFinalPosition_docWithoutProperNode() {
        Position pos = uboatParser.getFinalPosition(wrongDoc);
        assertNull(pos);
    }

    @Test
    public void getU96() throws IOException, SAXException, ParserConfigurationException {
        Uboat uboat = uboatParser.getUboat(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        assertNotNull(uboat);
        assertEquals("boats\\u96.htm", uboat.getHref());
        assertEquals("U-96", uboat.getNumber());
        assertEquals("VIIC", uboat.getType() );
        assertEquals("30 May 1938", uboat.getOrdered() );
        assertEquals("16 Sep 1939", uboat.getLaidDownWhen() );
        assertEquals("F. Krupp Germaniawerft AG, Kiel (werk 601)", uboat.getLaidDownWhere());
        assertEquals("/technical/shipyards/germania.htm", uboat.getLaidDownWhereHref());
        assertEquals("1 Aug 1940", uboat.getLaunched() );
        assertEquals("14 Sep 1940", uboat.getCommisioned() );

        assertNotNull(uboat.getAssignedCommanders());
        assertEquals(5, uboat.getAssignedCommanders().size());

        assertEquals(27, uboat.getSuccesses().getShipsSunk());
        assertEquals(181206, uboat.getSuccesses().getShipsSunkTonnage());
        assertEquals(4, uboat.getSuccesses().getShipsDamaged());
        assertEquals(33043, uboat.getSuccesses().getShipsDamagedTonnage());
        assertEquals(1, uboat.getSuccesses().getShipsTotalLoss());
        assertEquals(8888, uboat.getSuccesses().getShipsTotalLossTonnage());

        assertNotNull(uboat.getFates());
        assertEquals(2, uboat.getFates().size());

        assertNotNull(uboat.getFinalPosition());
        assertEquals(53.52, uboat.getFinalPosition().getLatitude(), 0.0);
        assertEquals(8.17, uboat.getFinalPosition().getLongitude(), 0.0);


    }

    @Test
    public void getU792() throws IOException, SAXException, ParserConfigurationException {
        Uboat uboat = uboatParser.getUboat(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U792_XML));
        assertNotNull(uboat);
        assertEquals(UBOAT_HREF_PREFIX + "u792.htm", uboat.getHref());
        assertEquals("U-792", uboat.getNumber());
        assertEquals("XVIIA", uboat.getType() );
        assertEquals("19 Jun 1942", uboat.getOrdered() );
        assertEquals("1 Dec 1942", uboat.getLaidDownWhen() );
        assertEquals("Blohm & Voss, Hamburg (werk 455)", uboat.getLaidDownWhere());
        assertEquals("/technical/shipyards/blohm.htm", uboat.getLaidDownWhereHref());
        assertEquals("28 Sep 1943", uboat.getLaunched() );
        assertEquals("16 Nov 1943", uboat.getCommisioned() );

        assertNotNull(uboat.getAssignedCommanders());
        assertEquals(2, uboat.getAssignedCommanders().size());

        assertEquals(0, uboat.getSuccesses().getShipsSunk());
        assertEquals(0, uboat.getSuccesses().getShipsSunkTonnage());
        assertEquals(0, uboat.getSuccesses().getShipsDamaged());
        assertEquals(0, uboat.getSuccesses().getShipsDamagedTonnage());
        assertEquals(0, uboat.getSuccesses().getShipsTotalLoss());
        assertEquals(0, uboat.getSuccesses().getShipsTotalLossTonnage());

        assertNotNull(uboat.getFates());
        assertEquals(2, uboat.getFates().size());

        assertNotNull(uboat.getFinalPosition());
        assertEquals(54.32, uboat.getFinalPosition().getLatitude(), 0.0);
        assertEquals(9.72, uboat.getFinalPosition().getLongitude(), 0.0);

    }

    public Uboat getTestUboat() {
        return uboatParser.getUboat(doc);
    }
}
