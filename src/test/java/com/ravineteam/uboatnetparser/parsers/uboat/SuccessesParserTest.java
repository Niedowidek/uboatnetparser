package com.ravineteam.uboatnetparser.parsers.uboat;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SuccessesParserTest {

    private SuccessesParser parser;
    private String value;
    private String valueBr;

    @Before
    public void setUp() throws Exception {
        parser = new SuccessesParser();
        value = "51 ships sunk, total tonnage 306,874 GRT\n1 warship sunk, total tonnage 1,060 tons\n2 warships damaged, total tonnage 2,099 tons\n21 ships damaged, total tonnage 6,874 GRT\n1 ship a total loss, total tonnage 8,448 GRT\n2 warships a total loss, total tonnage 16,888 GRT";
        valueBr = "51 ships sunk, total tonnage 306,874 GRT<br />1 warship sunk, total tonnage 1,060 tons<br />2 warships damaged, total tonnage 2,099 tons<br />21 ships damaged, total tonnage 6,874 GRT<br />1 ship a total loss, total tonnage 8,448 GRT<br />2 warships a total loss, total tonnage 16,888 GRT";
    }

    @Test
    public void getShipsSunk() {
        assertEquals(51, parser.getShipsSunk(value));
    }

    @Test
    public void getShipsSunkTonnage() {
        assertEquals(306874, parser.getShipsSunkTonnage(value));
    }

    // "27 ships sunk, total tonnage 181,206 GRT\n4 ships damaged, total tonnage 33,043 GRT\n1 ship a total loss, total tonnage 8,888 GRT"

    @Test
    public void getShipsSunkTonnage2() {
        assertEquals(181206, parser.getShipsSunkTonnage("27 ships sunk, total tonnage\n" +
                "181,206 GRT\n" +
                "4 ships damaged, total tonnage 33,043 GRT\n" +
                "1 ship a total loss, total tonnage 8,888 GRT"));
    }

    @Test
    public void getShipsDamaged() {
        assertEquals(21, parser.getShipsDamaged(value));
    }

    @Test
    public void getShipsDamagedTonnage() {
        assertEquals(6874, parser.getShipsDamagedTonnage(value));
    }

    @Test
    public void getShipsTotalLoss() {
        assertEquals(1, parser.getShipsTotalLoss(value));
    }

    @Test
    public void getShipsTotalLossTonnage() {
        assertEquals(8448, parser.getShipsTotalLossTonnage(value));
    }

    @Test
    public void getWarshipsSunk() {
        assertEquals(1, parser.getWarshipsSunk(value));
    }

    @Test
    public void getWarshipsSunkTonnage() {
        assertEquals(1060, parser.getWarshipsSunkTonnage(value));
    }

    @Test
    public void getWarshipsDamaged() {
        assertEquals(2, parser.getWarshipsDamaged(value));
    }

    @Test
    public void getWarshipsDamagedTonnage() {
        assertEquals(2099, parser.getWarshipsDamagedTonnage(value));
    }

    @Test
    public void getWarshipsTotalLoss() {
        assertEquals(2, parser.getWarshipsTotalLoss(value));
    }

    @Test
    public void getWarshipsTotalLossTonnage() {
        assertEquals(16888, parser.getWarshipsTotalLossTonnage(value));
    }

    @Test
    public void getShipsSunk_null() {
        assertEquals(0, parser.getShipsSunk(null));
    }

    @Test
    public void getShipsSunkTonnage_null() {
        assertEquals(0, parser.getShipsSunkTonnage(null));
    }

    @Test
    public void getShipsDamaged_null() {
        assertEquals(0, parser.getShipsDamaged(null));
    }

    @Test
    public void getShipsDamagedTonnage_null() {
        assertEquals(0, parser.getShipsDamagedTonnage(null));
    }

    @Test
    public void getShipsTotalLoss_null() {
        assertEquals(0, parser.getShipsTotalLoss(null));
    }

    @Test
    public void getShipsTotalLossTonnage_null() {
        assertEquals(0, parser.getShipsTotalLossTonnage(null));
    }

    @Test
    public void getWarshipsSunk_null() {
        assertEquals(0, parser.getWarshipsSunk(null));
    }

    @Test
    public void getWarshipsSunkTonnage_null() {
        assertEquals(0, parser.getWarshipsSunkTonnage(null));
    }

    @Test
    public void getWarshipsDamaged_null() {
        assertEquals(0, parser.getWarshipsDamaged(null));
    }

    @Test
    public void getWarshipsDamagedTonnage_null() {
        assertEquals(0, parser.getWarshipsDamagedTonnage(null));
    }

    @Test
    public void getWarshipsTotalLoss_null() {
        assertEquals(0, parser.getWarshipsTotalLoss(null));
    }

    @Test
    public void getWarshipsTotalLossTonnage_null() {
        assertEquals(0, parser.getWarshipsTotalLossTonnage(null));
    }

    @Test
    public void getShipsSunk_emptyValue() {
        assertEquals(0, parser.getShipsSunk(""));
    }

    @Test
    public void getShipsSunkTonnage_emptyValue() {
        assertEquals(0, parser.getShipsSunkTonnage(""));
    }

    @Test
    public void getShipsDamaged_emptyValue() {
        assertEquals(0, parser.getShipsDamaged(""));
    }

    @Test
    public void getShipsDamagedTonnage_emptyValue() {
        assertEquals(0, parser.getShipsDamagedTonnage(""));
    }

    @Test
    public void getShipsTotalLoss_emptyValue() {
        assertEquals(0, parser.getShipsTotalLoss(""));
    }

    @Test
    public void getShipsTotalLossTonnage_emptyValue() {
        assertEquals(0, parser.getShipsTotalLossTonnage(""));
    }

    @Test
    public void getWarshipsSunk_emptyValue() {
        assertEquals(0, parser.getWarshipsSunk(""));
    }

    @Test
    public void getWarshipsSunkTonnage_emptyValue() {
        assertEquals(0, parser.getWarshipsSunkTonnage(""));
    }

    @Test
    public void getWarshipsDamaged_emptyValue() {
        assertEquals(0, parser.getWarshipsDamaged(""));
    }

    @Test
    public void getWarshipsDamagedTonnage_emptyValue() {
        assertEquals(0, parser.getWarshipsDamagedTonnage(""));
    }

    @Test
    public void getWarshipsTotalLoss_emptyValue() {
        assertEquals(0, parser.getWarshipsTotalLoss(""));
    }

    @Test
    public void getWarshipsTotalLossTonnage_emptyValue() {
        assertEquals(0, parser.getWarshipsTotalLossTonnage(""));
    }
}