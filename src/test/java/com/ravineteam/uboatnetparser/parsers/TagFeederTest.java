package com.ravineteam.uboatnetparser.parsers;

import com.ravineteam.uboatnetparser.utils.TagFeeder;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import com.ravineteam.uboatnetparser.xml.XmlParserTest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TagFeederTest {

    private TagFeeder feeder;
    private XmlParser xmlParser;
    
    @Before
    public void setUp()  {
        feeder = new TagFeeder();
        xmlParser = new XmlParser();
    }

    @Test
    public void whichTagByNameValueAndOrder() throws Exception {
        String tagName = "h3";
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        assertEquals(2, n);
    }

    @Test
    public void whichTagByNameValueAndOrder_nullTagName() throws Exception {
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, null, tagValue, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void whichTagByNameValueAndOrder_emptyTagName() throws Exception {
        String tagName = "";
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void whichTagByNameValueAndOrder_nullTagValue() throws Exception {
        String tagName = "h3";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, null, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void whichTagByNameValueAndOrder_emptyTagValue() throws Exception {
        String tagName = "h3";
        String tagValue = "";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void whichTagByNameValueAndOrder_order0() throws Exception {
        String tagName = "h3";
        String tagValue = "Type";
        int order = 0;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void whichTagByNameValueAndOrder_orderLessThanZero() throws Exception {
        String tagName = "h3";
        String tagValue = "Type";
        int order = -10;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        assertEquals(n, TagFeeder.NOT_FOUND);
    }

    @Test
    public void getNodeByTagNameAndOrder() throws ParserConfigurationException, SAXException, IOException {
        String tagName = "h3";
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        Node node = feeder.getNode(doc, tagName, n + 1);
        assertNotNull(node);
        assertEquals("VIIC", node.getTextContent());
    }

    // XVIIA

    @Test
    public void getNodeByTagNameAndOrderU792() throws ParserConfigurationException, SAXException, IOException {
        String tagName = "h3";
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U792_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        Node node = feeder.getNode(doc, tagName, n + 1);
        assertNotNull(node);
        assertEquals("XVIIA", node.getTextContent());
    }

    @Test
    public void getNodeByTagNameAndOrderU792_comboOperation() throws ParserConfigurationException, SAXException, IOException {
        String tagName = "h3";
        String tagValue = "Type";
        int order = 1;
        Document doc = xmlParser.parse(new File(XmlParserTest.SRC_TEST_RESOURCES_UBOATNET_U792_XML));
        int n = feeder.getTag(doc, tagName, tagValue, order);
        Node node = feeder.getNode(doc, tagName, n);
        Node node2 = feeder.getNode(doc, tagName, tagValue, order);
        assertNotNull(node);
        assertNotNull(node2);
        assertEquals("Type", node.getTextContent());
        assertEquals("Type", node2.getTextContent());
        assertEquals(node, node2);
    }
}