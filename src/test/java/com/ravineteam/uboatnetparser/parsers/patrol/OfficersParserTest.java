package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Officer;
import com.ravineteam.uboatnetparser.data.util.OfficersUtil;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class OfficersParserTest {

    private OfficersParser officersParser;
    private XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        officersParser = new OfficersParser();
        xmlParser = new XmlParser();
    }

    @Test
    public void patrol4721test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4721.html"));
        assertNotNull(doc);
        
        List<Officer> officers = officersParser.parse(doc);

        OfficersUtil officersUtil = new OfficersUtil();

        assertNotNull(officers);
        assertEquals(7, officers.size());

        Officer willenbrock = officersUtil.getByNamePart(officers, "willenbrock");
        assertNotNull(willenbrock);
        assertTrue(willenbrock.isNotEmpty());
        assertEquals("Heinrich Lehmann-Willenbrock", willenbrock.getName());
        assertEquals("Kptlt.", willenbrock.getRank());
        assertEquals("/men/willenbrock.htm", willenbrock.getHref());

        Officer hamm = officersUtil.getByNamePart(officers, "hamm");
        assertNotNull(hamm);
        assertTrue(hamm.isNotEmpty());
        assertEquals("Horst Hamm", hamm.getName());
        assertEquals("Oblt.", hamm.getRank());
        assertEquals("/men/commanders/404.html", hamm.getHref());
        assertEquals("1WO", hamm.getAssignment());

        Officer hellriegel = officersUtil.getByNamePart(officers, "hellriegel");
        assertNotNull(hellriegel);
        assertTrue(hellriegel.isNotEmpty());
        assertEquals("Hans-Jürgen Hellriegel", hellriegel.getName());
        assertEquals("Oblt.", hellriegel.getRank());
        assertEquals("/men/hellriegel.htm", hellriegel.getHref());
        assertEquals("2WO", hellriegel.getAssignment());

        Officer roithberg = officersUtil.getByNamePart(officers, "roithberg");
        assertNotNull(roithberg);
        assertTrue(roithberg.isNotEmpty());
        assertEquals("2WO", roithberg.getAssignment());

        Officer linder = officersUtil.getByNamePart(officers, "linder");
        assertNotNull(linder);
        assertTrue(linder.isNotEmpty());
        assertEquals("Kptlt.", linder.getRank());

        Officer dommes = officersUtil.getByNamePart(officers, "dommes");
        assertNotNull(dommes);
        assertTrue(dommes.isNotEmpty());
        assertEquals("/men/dommes.htm", dommes.getHref());
        assertEquals("Cdr sea training", dommes.getAssignment());

        Officer radermacher = officersUtil.getByNamePart(officers, "radermacher");
        assertNotNull(radermacher);
        assertTrue(radermacher.isNotEmpty());
        assertEquals("Alfred Radermacher", radermacher.getName());
    }

    @Test
    public void patrol337test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_337.html"));
        assertNotNull(doc);

        List<Officer> officers = officersParser.parse(doc);

        OfficersUtil officersUtil = new OfficersUtil();

        assertNotNull(officers);
        assertEquals(4, officers.size());

        Officer officer = officersUtil.getByNamePart(officers, "bleichrodt");
        assertNotNull(officer);
        assertEquals("Heinrich Bleichrodt", officer.getName());

        officer = officersUtil.getByNamePart(officers, "suhren");
        assertNotNull(officer);
        assertEquals("Oblt.", officer.getRank());

        officer = officersUtil.getByNamePart(officers, "otto ites");
        assertNotNull(officer);
        assertEquals("2WO", officer.getAssignment());

        officer = officersUtil.getByNamePart(officers, "engel");
        assertNotNull(officer);
        assertEquals("/men/commanders/254.html", officer.getHref());

    }

    @Test
    public void patrol1265test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_1265.html"));
        assertNotNull(doc);

        List<Officer> officers = officersParser.parse(doc);

        OfficersUtil officersUtil = new OfficersUtil();

        assertNotNull(officers);
        assertEquals(1, officers.size());

        Officer officer = officersUtil.getByNamePart(officers, "klaus");
        assertNotNull(officer);
        assertEquals("Kptlt.", officer.getRank());
        assertEquals("Klaus Hänert", officer.getName());
    }
}