package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ShipHitListParserTest {

    private XmlParser xmlParser;
    private ShipHitListParser shipHitListParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        shipHitListParser = new ShipHitListParser();
    }

    @Test
    public void patrol4719test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4719.html"));

        List<ShipHit> shipHitList = shipHitListParser.parse(doc);
        assertNotNull(shipHitList);
        assertEquals(7, shipHitList.size());

        assertEquals("1940-12-11", shipHitList.get(0).getDateFormatted());
        assertEquals("U-96", shipHitList.get(1).getUboat());
        assertEquals("Heinrich Lehmann-Willenbrock", shipHitList.get(2).getCommander());
        assertEquals("12 Dec 1940", shipHitList.get(3).getDateLabel());
        assertEquals("/boats/u96.htm", shipHitList.get(4).getUboatHref());
        assertEquals("/men/willenbrock.htm", shipHitList.get(6).getCommanderHref());

        assertEquals("Rotorua", shipHitList.get(0).getShipName());
        assertEquals("/allies/merchants/ship/713.html", shipHitList.get(1).getShipHref());
        assertEquals(4575, shipHitList.get(2).getTonnage());
        assertEquals("be", shipHitList.get(3).getNationality());
        assertEquals("E", shipHitList.get(4).getMapSymbol());
        assertEquals("OB-257", shipHitList.get(5).getConvoy());
        assertEquals("/media/images/flags/small/flag_netherlands_s.png", shipHitList.get(6).getNationalityHref());

        assertTrue(StringUtils.isEmpty(shipHitList.get(4).getConvoy()));

        assertTrue(shipHitList.get(5).isDamaged());
        assertTrue(shipHitList.get(6).isDamaged());

        assertEquals(57.83, shipHitList.get(2).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-8.67, shipHitList.get(2).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("The Swedish motor merchant <a href=\"/allies/merchants/ship/715.html\">Stureholm</a> (4,575 GRT) sunk by <a href=\"/boats/u96.htm\">U-96</a> (Lehmann-Willenbrock) on 12 Dec 1940 at 01h56.", shipHitList.get(2).getTextFromMarkerWithHtml());
    }

    @Test
    public void patrol4755test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4755.html"));

        List<ShipHit> shipHitList = shipHitListParser.parse(doc);
        assertNotNull(shipHitList);
        assertEquals(7, shipHitList.size());

        assertEquals("1940-07-05", shipHitList.get(0).getDateFormatted());
        assertEquals("U-99", shipHitList.get(1).getUboat());
        assertEquals("Otto Kretschmer", shipHitList.get(2).getCommander());
        assertEquals("8 Jul 1940", shipHitList.get(3).getDateLabel());
        assertEquals("/boats/u99.htm", shipHitList.get(4).getUboatHref());
        assertEquals("/men/kretschmer.htm", shipHitList.get(6).getCommanderHref());

        assertEquals("Magog", shipHitList.get(0).getShipName());
        assertEquals("/allies/merchants/ship/412.html", shipHitList.get(1).getShipHref());
        assertEquals(1514, shipHitList.get(2).getTonnage());
        assertEquals("br", shipHitList.get(3).getNationality());
        assertEquals("C", shipHitList.get(2).getMapSymbol());
        assertEquals("HX-53", shipHitList.get(3).getConvoy());
        assertEquals("/ops/convoys/convoys.php?convoy=HX-53", shipHitList.get(3).getConvoyHref());
        assertTrue(StringUtils.isEmpty(shipHitList.get(4).getConvoy()));
        assertTrue(StringUtils.isEmpty(shipHitList.get(5).getConvoy()));
        assertEquals("/media/images/flags/small/flag_united_kingdom_s.png", shipHitList.get(6).getNationalityHref());
        assertTrue(shipHitList.get(5).isCaptured());

        assertEquals(51.00, shipHitList.get(4).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-14.00, shipHitList.get(4).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("The Greek steam merchant <a href=\"/allies/merchants/ship/421.html\">Ia</a> (4,860 GRT) sunk by <a href=\"/boats/u99.htm\">U-99</a> (Kretschmer) on 12 Jul 1940 at 02h06.", shipHitList.get(4).getTextFromMarkerWithHtml());

    }

    @Test
    public void patrol4724test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4724.html"));

        List<ShipHit> shipHitList = shipHitListParser.parse(doc);
        assertTrue(shipHitList.isEmpty());
    }

}