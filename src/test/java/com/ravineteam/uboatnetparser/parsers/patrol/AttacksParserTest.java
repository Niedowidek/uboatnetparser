package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.Attack;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class AttacksParserTest {
    
    private XmlParser xmlParser;
    private AttacksParser attacksParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        attacksParser = new AttacksParser();
    }

    @Test
    public void patrol1265test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_1265.html"));
        assertNotNull(doc);

        List<Attack> attacks = attacksParser.parseAttacks(doc);
        assertNotNull(attacks);
        assertEquals(1, attacks.size());
        assertEquals("1944-02-22", attacks.get(0).getDateFormatted());
        assertEquals("A Catalina aircraft (RCAF Sqdn 162/S, pilot Flying Officer C. C. Cunningham) dropped four depth charges and strafed the boat south of Iceland, killing two men. The boat was lost on the same patrol on 16 April. (Sources: Blair, vol 2, page 560, )", attacks.get(0).getText());
        assertEquals("(Sources: Blair, vol 2, page 560, )", attacks.get(0).getCites().get(0));
        assertTrue(attacks.get(0).getHrefs().isEmpty());
        assertEquals(61.05, attacks.get(0).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-20.17, attacks.get(0).getPosition().getLongitude(), Position.ALLOWED_ERROR);

    }

    @Test
    public void patrol4722test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4722.html"));
        assertNotNull(doc);

        List<Attack> attacks = attacksParser.parseAttacks(doc);
        assertNotNull(attacks);
        assertEquals(1, attacks.size());
        assertEquals("1941-04-28", attacks.get(0).getDateFormatted());
        assertEquals("On 28 Apr 1941 southeast of Iceland, in position 60.04N, 15.45W, the British corvette HMS Gladiolus depth charged a German U-boat. This was for some time thought to have sunk U-65, but the target was actually U-96, which escaped unscathed.", attacks.get(0).getText());
        assertTrue(attacks.get(0).getCites().isEmpty());
        assertEquals(2, attacks.get(0).getHrefs().size());
        assertEquals(60.05, attacks.get(0).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-16.17, attacks.get(0).getPosition().getLongitude(), Position.ALLOWED_ERROR);

    }

    @Test
    public void patrol4725test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4725.html"));
        assertNotNull(doc);

        List<Attack> attacks = attacksParser.parseAttacks(doc);
        assertNotNull(attacks);
        assertEquals(2, attacks.size());

        assertEquals("1941-10-31", attacks.get(0).getDateFormatted());
        assertEquals("While attacking convoy OS 10 on the surface during a full moon, Lehmann-Willenbrock fired into the convoy at long range, sinking one ship. The British escort sloop HMS Lulworth gave chase, and after driving U-96 under with gunfire, dropped 27 depth charges. None fell close, and the boat evaded the attack and continued the patrol. (Sources: Blair, vol 1, page 394)", attacks.get(0).getText());
        assertEquals("(Sources: Blair, vol 1, page 394)", attacks.get(0).getCites().get(0));
        assertEquals(2, attacks.get(0).getHrefs().size());

        assertEquals("1941-11-30", attacks.get(1).getDateFormatted());
        assertEquals("While penetrating the Straits of Gibraltar, U-96 was attacked at 2235hrs by a British Swordfish aircraft. Suffering some damage, the boat dived, surfaced the next morning at 0445hrs, and proceeded to base in France. The much longer and more dramatic stay in the deep described by Buchheim in his novel Das Boot is one of the numerous occasions in this book where the author fictionalized the events he experienced during his time as war correspondent on U-96. (Sources: Blair, vol 1, page 401)", attacks.get(1).getText());
        assertEquals("(Sources: Blair, vol 1, page 401)", attacks.get(1).getCites().get(0));
        assertEquals(1, attacks.get(1).getHrefs().size());

        assertEquals(51.33, attacks.get(0).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-23.67, attacks.get(0).getPosition().getLongitude(), Position.ALLOWED_ERROR);

        assertEquals(36.15, attacks.get(1).getPosition().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-7.30, attacks.get(1).getPosition().getLongitude(), Position.ALLOWED_ERROR);

    }

    @Test
    public void patrol2567test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_2567.html"));
        assertNotNull(doc);
        List<Attack> attacks = attacksParser.parseAttacks(doc);
        assertTrue(attacks.isEmpty());

    }
}