package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.Details;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class DetailsParserTest {

    XmlParser xmlParser;
    DetailsParser parser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        parser = new DetailsParser();
    }

    @Test
    public void shouldParseU9date19431019() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/details/details.php@boat=9_date=1943-10-19.html"));
        Details details = parser.parse(doc);

        assertNotNull(details);
        assertTrue(!details.isEmpty());
        assertNotNull(details.getPositionFromMarker());
        assertEquals("19 Oct 1943", details.getDate());
        assertEquals(44.25, details.getPositionFromMarker().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(39.50, details.getPositionFromMarker().getLongitude(), Position.ALLOWED_ERROR);
        assertNotNull(details.getPositionFromDescription());
        assertEquals(44.15, details.getPositionFromDescription().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(39.30, details.getPositionFromDescription().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("U-9", details.getUboatName());
        assertEquals("/boats/patrols/patrol_38.html", details.getPatrolHref());
        assertEquals("2 Oct 1943", details.getPatrolStartDate());
        assertEquals("23 Oct 1943", details.getPatrolEndDate());

    }

    @Test
    public void shouldParseU28date1939116() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/details/details.php@boat=28_date=1939-11-16.html"));
        Details details = parser.parse(doc);

        assertNotNull(details);
        assertTrue(!details.isEmpty());
        assertNotNull(details.getPositionFromMarker());
        assertEquals("16 Nov 1939", details.getDate());
        assertEquals(58.65, details.getPositionFromMarker().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-9.40, details.getPositionFromMarker().getLongitude(), Position.ALLOWED_ERROR);
        assertNotNull(details.getPositionFromDescription());
        assertEquals(58.39, details.getPositionFromDescription().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-9.24, details.getPositionFromDescription().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("U-28", details.getUboatName());
        assertEquals("/boats/patrols/patrol_197.html", details.getPatrolHref());
        assertEquals("8 Nov 1939", details.getPatrolStartDate());
        assertEquals("18 Dec 1939", details.getPatrolEndDate());

    }

    @Test
    public void shouldParseU85date19420121() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/details/details.php@boat=85_date=1942-01-21.html"));
        Details details = parser.parse(doc);

        assertNotNull(details);
        assertTrue(!details.isEmpty());
        assertNotNull(details.getPositionFromMarker());
        assertEquals("21 Jan 1942", details.getDate());
        assertEquals(43.35, details.getPositionFromMarker().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-31.75, details.getPositionFromMarker().getLongitude(), Position.ALLOWED_ERROR);
        assertNotNull(details.getPositionFromDescription());
        assertEquals(43.21, details.getPositionFromDescription().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-31.45, details.getPositionFromDescription().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("U-85", details.getUboatName());
        assertEquals("/boats/patrols/patrol_3644.html", details.getPatrolHref());
        assertEquals("8 Jan 1942", details.getPatrolStartDate());
        assertEquals("23 Feb 1942", details.getPatrolEndDate());

    }

    @Test
    public void shouldParseU427date19441031() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/details/details.php@boat=427_date=1944-10-31.html"));
        Details details = parser.parse(doc);

        assertNotNull(details);
        assertTrue(!details.isEmpty());
        assertNotNull(details.getPositionFromMarker());
        assertEquals("31 Oct 1944", details.getDate());
        assertEquals(58.65, details.getPositionFromMarker().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(5.00, details.getPositionFromMarker().getLongitude(), Position.ALLOWED_ERROR);
        assertNotNull(details.getPositionFromDescription());
        assertEquals(58.39, details.getPositionFromDescription().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(5.00, details.getPositionFromDescription().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("U-427", details.getUboatName());
        assertEquals("/boats/patrols/patrol_708.html", details.getPatrolHref());
        assertEquals("30 Oct 1944", details.getPatrolStartDate());
        assertEquals("8 Nov 1944", details.getPatrolEndDate());

    }

    @Test
    public void shouldParseU636date19450102() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/details/details.php@boat=636_date=1945-01-02.html"));
        Details details = parser.parse(doc);

        assertNotNull(details);
        assertTrue(!details.isEmpty());
        assertNotNull(details.getPositionFromMarker());
        assertEquals("2 Jan 1945", details.getDate());
        assertEquals(69.45, details.getPositionFromMarker().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(33.50, details.getPositionFromMarker().getLongitude(), Position.ALLOWED_ERROR);
        assertNotNull(details.getPositionFromDescription());
        assertEquals(69.27, details.getPositionFromDescription().getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(33.30, details.getPositionFromDescription().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("U-636", details.getUboatName());
        assertEquals("/boats/patrols/patrol_1887.html", details.getPatrolHref());
        assertEquals("25 Dec 1944", details.getPatrolStartDate());
        assertEquals("30 Jan 1945", details.getPatrolEndDate());

    }


}