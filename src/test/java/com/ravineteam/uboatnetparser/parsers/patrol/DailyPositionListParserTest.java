package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.DailyPosition;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DailyPositionListParserTest {

    private XmlParser xmlParser;
    private DailyPositionListParser dailyPositionListParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        dailyPositionListParser = new DailyPositionListParser();
    }

    @Test
    public void patrol2567test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_2567.html"));
        assertNotNull(doc);

        List<DailyPosition> dailyPositionList = dailyPositionListParser.parse(doc);
        assertNotNull(dailyPositionList);
        assertEquals(22, dailyPositionList.size());
        assertEquals("details.php?boat=862&date=1944-11-18", dailyPositionList.get(0).getHref());
        assertEquals("21 Nov 1944", dailyPositionList.get(3).getLabel());
        assertEquals("1944-12-19", dailyPositionList.get(10).getDate());
        assertEquals("details.php?boat=862&date=1945-02-13", dailyPositionList.get(20).getHref());
        assertEquals("details.php?boat=862&date=1945-02-15", dailyPositionList.get(21).getHref());

    }

    @Test
    public void patrol4755test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4755.html"));
        assertNotNull(doc);

        List<DailyPosition> dailyPositionList = dailyPositionListParser.parse(doc);
        assertNotNull(dailyPositionList);
        assertEquals(25, dailyPositionList.size());
        assertEquals("details.php?boat=99&date=1940-06-27", dailyPositionList.get(0).getHref());
        assertEquals("30 Jun 1940", dailyPositionList.get(3).getLabel());
        assertEquals("1940-07-07", dailyPositionList.get(10).getDate());
        assertEquals("details.php?boat=99&date=1940-07-17", dailyPositionList.get(20).getHref());
        assertEquals("details.php?boat=99&date=1940-07-18", dailyPositionList.get(21).getHref());
    }
}