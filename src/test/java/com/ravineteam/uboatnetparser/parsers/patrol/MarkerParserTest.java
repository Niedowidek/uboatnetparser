package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.Attack;
import com.ravineteam.uboatnetparser.data.patrol.Event;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class MarkerParserTest {
    
    private XmlParser xmlParser;
    private MarkerParser markerParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        markerParser = new MarkerParser();
    }

    @Test
    public void patrol1265AttackPositionsTest() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_1265.html"));
        assertNotNull(doc);
        AttacksParser attacksParser = new AttacksParser();
        List<Attack> attackList = attacksParser.parseAttacks(doc);
        Attack attack = attackList.get(0);
//        Marker marker = markerParser.getAttackMarker(doc, attackList.get(0));
        Marker marker = markerParser.getMarker(doc, "pink_Marker", attack.getDate(), attack.getText());
        Position attackPosition = marker.getPosition();
        assertTrue(attackPosition.isNotEmpty());

        assertEquals(61.05, attackPosition.getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-20.17, attackPosition.getLongitude(), Position.ALLOWED_ERROR);
    }

    @Test
    public void patrol2567test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_2567.html"));
        assertNotNull(doc);

        List<Event> eventList = new EventParser().parseEvents(doc);
        Position eventPosition = markerParser.getEventPosition(doc, eventList.get(0));
        assertTrue(eventPosition.isEmpty());

//        assertEquals(999, eventPosition.getLatitude(), Position.ALLOWED_ERROR);
//        assertEquals(999, eventPosition.getLongitude(), Position.ALLOWED_ERROR);
    }

    @Test
    public void patrol4755AttackPositionsTest() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4755.html"));
        assertNotNull(doc);
        AttacksParser attacksParser = new AttacksParser();
        List<Attack> attackList = attacksParser.parseAttacks(doc);

        int count = 0;
        Attack attack = attackList.get(count);
//        Marker marker = markerParser.getAttackMarker(doc, attackList.get(count));
        Marker marker = markerParser.getMarker(doc, "pink_Marker", attack.getDate(), attack.getText());
        Position attackPosition = marker.getPosition();
        assertNotNull(attackPosition);
        assertTrue(attackPosition.isNotEmpty());
        assertEquals(57.75, attackPosition.getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(1.40, attackPosition.getLongitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLatitude(), attackList.get(count).getPosition().getLatitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLongitude(), attackList.get(count).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("<strong>U-99</strong> on <strong>29 Jun 1940</strong>. In diving to escape an attack by a German aircraft which dropped three bombs, the boat suffered minor damage on striking the seabed. It continued the patrol after repairing the damage while settled on the bottom. Position (57.45, 1.24).", marker.getTextContentWithHtmlTags());
        assertEquals("U-99 on 29 Jun 1940. In diving to escape an attack by a German aircraft which dropped three bombs, the boat suffered minor damage on striking the seabed. It continued the patrol after repairing the damage while settled on the bottom. Position (57.45, 1.24).", marker.getTextContent());

        count++;
        attack = attackList.get(count);
//        marker = markerParser.getAttackMarker(doc, attackList.get(count));
        marker = markerParser.getMarker(doc, "pink_Marker", attack.getDate(), attack.getText());
        attackPosition = marker.getPosition();
        assertNotNull(attackPosition);
        assertTrue(attackPosition.isNotEmpty());
        assertEquals(50.10, attackPosition.getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-10.38, attackPosition.getLongitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLatitude(), attackList.get(count).getPosition().getLatitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLongitude(), attackList.get(count).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("<strong>U-99</strong> on <strong>7 Jul 1940</strong>. At 14.14 hours the boat tried to stop the armed merchant <a href=\"/allies/merchants/ships/775.html\">Manistee</a> with gunfire after missing her with a G7e torpedo at 14.01 hours. No hits were scored in the gun duel, but the Germans broke off the attack when shots fell within 100-200m (109-118 yds) of U-99. Position (50.06, -10.23).", marker.getTextContentWithHtmlTags());
        assertEquals("U-99 on 7 Jul 1940. At 14.14 hours the boat tried to stop the armed merchant Manistee with gunfire after missing her with a G7e torpedo at 14.01 hours. No hits were scored in the gun duel, but the Germans broke off the attack when shots fell within 100-200m (109-118 yds) of U-99. Position (50.06, -10.23).", marker.getTextContent());

        count++;
        attack = attackList.get(count);
//        marker = markerParser.getAttackMarker(doc, attackList.get(count));
        marker = markerParser.getMarker(doc, "pink_Marker", attack.getDate(), attack.getText());
        attackPosition = marker.getPosition();
        assertNotNull(attackPosition);
        assertTrue(attackPosition.isNotEmpty());
        assertEquals(50.60, attackPosition.getLatitude(), Position.ALLOWED_ERROR);
        assertEquals(-9.40, attackPosition.getLongitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLatitude(), attackList.get(count).getPosition().getLatitude(), Position.ALLOWED_ERROR);
//        assertEquals(attackPosition.getLongitude(), attackList.get(count).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals("<strong>U-99</strong> on <strong>8 Jul 1940</strong>. After a successful attack on convoy <a href=\"/ops/convoys/convoys.php?convoy=HX-53\">HX 53</a> south of Fastnet, escorts dropped a total of 107 depth charges over 14 hours, but the boat escaped unscathed. Position (50.36, -9.24).", marker.getTextContentWithHtmlTags());
        assertEquals("U-99 on 8 Jul 1940. After a successful attack on convoy HX 53 south of Fastnet, escorts dropped a total of 107 depth charges over 14 hours, but the boat escaped unscathed. Position (50.36, -9.24).", marker.getTextContent());
    }
}