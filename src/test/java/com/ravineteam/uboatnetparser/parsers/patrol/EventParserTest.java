package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Event;
import com.ravineteam.uboatnetparser.data.patrol.Href;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class EventParserTest {

    private XmlParser xmlParser;
    private EventParser eventParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        eventParser = new EventParser();
    }

    @Test
    public void patrol337test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_337.html"));
        assertNotNull(doc);

        List<Event> eventList = eventParser.parseEvents(doc);
        assertNotNull(eventList);
        assertEquals(1, eventList.size());

        assertEquals("1940-09-23", eventList.get(0).getDateFormatted());
        assertEquals("U-48 and U-99 searched for survivors from a crashed Heinkel He 111 aircraft in naval grid BF 17.", eventList.get(0).getText());

        assertNotNull(eventList.get(0).getHrefs());
        Href u48 = eventList.get(0).getHrefs().get(0);
        assertNotNull(u48);
        assertEquals("U-48", u48.getText());
        assertEquals("/boats/u48.htm", u48.getHref());
        Href u99 = eventList.get(0).getHrefs().get(1);
        assertNotNull(u99);
        assertEquals("U-99", u99.getText());
        assertEquals("/boats/u99.htm", u99.getHref());
        assertTrue(eventList.get(0).getPosition().isEmpty());

//        assertEquals(0.0, eventList.get(0).getPosition().getLongitude(), Position.ALLOWED_ERROR);
//        assertEquals(0.0, eventList.get(0).getPosition().getLatitude(), Position.ALLOWED_ERROR);
    }

    @Test
    public void patrol2567test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_2567.html"));
        assertNotNull(doc);

        List<Event> eventList = eventParser.parseEvents(doc);
        assertNotNull(eventList);
        assertEquals(1, eventList.size());

        assertEquals("1944-12-09", eventList.get(0).getDateFormatted());
        assertEquals("U-862 had a gun duel with the Greek steam merchant Ilissos (4724 tons) off Tasmania at 37°11S/139°35E (grid VC 8275). U-862 fired three shots which missed, and choppy seas and accurate defensive gunfire from the vessel forced the U-boat to dive and leave the area before firing more.", eventList.get(0).getText());

        assertNotNull(eventList.get(0).getHrefs());
        Href href1 = eventList.get(0).getHrefs().get(0);
        assertNotNull(href1);
        assertEquals("U-862", href1.getText());
        assertEquals("/boats/u862.htm", href1.getHref());
        Href href2 = eventList.get(0).getHrefs().get(1);
        assertNotNull(href2);
        assertEquals("U-862", href2.getText());
        assertEquals("/boats/u862.htm", href2.getHref());
    }

    @Test
    public void patrol4755test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4755.html"));
        assertNotNull(doc);

        List<Event> eventList = eventParser.parseEvents(doc);
        assertNotNull(eventList);
        assertEquals(2, eventList.size());

        assertEquals("1940-06-29", eventList.get(0).getDateFormatted());
        assertEquals("A German aircraft dropped 3 bombs on U-99 in the North Sea. The boat suffered minor damage.", eventList.get(0).getText());

        Href u99 = eventList.get(0).getHrefs().get(0);
        assertNotNull(u99);
        assertEquals("U-99", u99.getText());
        assertEquals("/boats/u99.htm", u99.getHref());

        assertEquals("1940-07-12", eventList.get(1).getDateFormatted());
        assertEquals("On 12 July, 1940, the Estonian steamer Merisaar was ordered by U-99 to sail to Bordeaux, France (the port was then already under German control). Her captain complied but on the way there she was sunk (on July 15th) by bombs from a German aircraft south of Queenstown.", eventList.get(1).getText());

        u99 = eventList.get(1).getHrefs().get(0);
        assertNotNull(u99);
        assertEquals("U-99", u99.getText());
        assertEquals("/boats/u99.htm", u99.getHref());

        Href bordeaux = eventList.get(1).getHrefs().get(1);
        assertNotNull(bordeaux);
        assertEquals("Bordeaux", bordeaux.getText());
        assertEquals("/flotillas/bases/bordeaux.htm", bordeaux.getHref());

        eventList.get(0).getPosition().isEmpty();
        eventList.get(1).getPosition().isEmpty();

        /*assertEquals(0.0, eventList.get(0).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(0.0, eventList.get(0).getPosition().getLatitude(), Position.ALLOWED_ERROR);

        assertEquals(0.0, eventList.get(1).getPosition().getLongitude(), Position.ALLOWED_ERROR);
        assertEquals(0.0, eventList.get(1).getPosition().getLatitude(), Position.ALLOWED_ERROR);*/

    }

    @Test
    public void patrol1265test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_1265.html"));
        assertNotNull(doc);

        List<Event> eventList = eventParser.parseEvents(doc);
        assertTrue(eventList.isEmpty());
    }

}