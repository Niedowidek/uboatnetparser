package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TonnageParserTest {

    private XmlParser xmlParser;
    private TonnageParser tonnageParser;

    @Before
    public void setUp() throws Exception {
        xmlParser = new XmlParser();
        tonnageParser = new TonnageParser();
    }

    @Test
    public void patrol4719test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4719.html"));
        assertNotNull(doc);

        Tonnage tonnage = tonnageParser.parse(doc);
        assertNotNull(tonnage);
        assertEquals(5, tonnage.getShipsSunk());
        assertEquals(37037, tonnage.getShipsSunkTonnage());
        assertEquals(2, tonnage.getShipsDamaged());
        assertEquals(15864, tonnage.getShipsDamagedTonnage());

        assertEquals(0, tonnage.getWarshipsDamagedTonnage());
    }

    @Test
    public void patrol4348test() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4348.html"));
        assertNotNull(doc);

        Tonnage tonnage = tonnageParser.parse(doc);
        assertNotNull(tonnage);
        assertEquals(0, tonnage.getShipsSunk());
        assertEquals(0, tonnage.getShipsSunkTonnage());
        assertEquals(0, tonnage.getShipsDamaged());
        assertEquals(0, tonnage.getShipsDamagedTonnage());

    }


}