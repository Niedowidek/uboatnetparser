package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Patrol;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class PatrolParserTest {

    private PatrolParser patrolParser;
    private XmlParser xmlParser;

    @Before
    public void setUp() throws Exception {
        patrolParser = new PatrolParser();
        xmlParser = new XmlParser();
    }

    @Test
    public void patrol4721startEndtest() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_4721.html"));
        Patrol patrol = patrolParser.parse(doc);

        assertNotNull(patrol);

        assertEquals("1941-01-30", patrol.getStartDate());
        assertEquals("Lorient", patrol.getStartPlace());
        assertEquals("1941-02-28", patrol.getEndDate());
        assertEquals("St. Nazaire", patrol.getEndPlace());
        assertEquals("30 days", patrol.getDuration());
        assertNull(patrol.getFate());

    }

    @Test
    public void patrol1265startEndtest() throws ParserConfigurationException, SAXException, IOException {
        Document doc = xmlParser.parse(new File("src/test/resources/uboatnet/patrols/patrol_1265.html"));
        Patrol patrol = patrolParser.parse(doc);

        assertNotNull(patrol);

        assertEquals("1944-02-06", patrol.getStartDate());
        assertEquals("Kiel", patrol.getStartPlace());
        assertEquals("1944-04-16", patrol.getEndDate());
        assertNull(patrol.getEndPlace());
        assertEquals("71 days", patrol.getDuration());
        assertEquals("Lost", patrol.getFate());

    }


}