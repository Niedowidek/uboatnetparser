package com.ravineteam.uboatnetparser.xml;


import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class XmlParserTest {

    public static final String SRC_TEST_RESOURCES_UBOATNET_U96_XML = "src/test/resources/uboatnet/boats/u96.htm";
    public static final String SRC_TEST_RESOURCES_UBOATNET_U792_XML = "src/test/resources/uboatnet/boats/u792.htm";
    public static final String SRC_TEST_RESOURCES_TESTFILE_XML = "src/test/resources/testfile.html";
    public static final String SRC_TEST_RESOURCES_UBOATNET_U48_XML = "src/test/resources/uboatnet/boats/u48.htm";
    private File testfile;
    private XmlParser parser;

    @Before
    public void setUp() {
        testfile = new File(SRC_TEST_RESOURCES_TESTFILE_XML);
        boolean exists = testfile.exists();
        parser  = new XmlParser();
    }

    @Test
    public void parsujPlik() throws ParserConfigurationException, SAXException, IOException {
        Document doc = parser.parse(testfile);
        assertNotNull(doc);
        NodeList list = doc.getElementsByTagName("node");
        assertTrue(list != null && list.getLength() == 1 && "text inside node".equals(list.item(0).getTextContent()));
    }

    @Test
    public void parsujInputStream() throws IOException, SAXException, ParserConfigurationException {
        Document doc = parser.parse(new FileInputStream(testfile));
        assertNotNull(doc);
        NodeList list = doc.getElementsByTagName("node");
        assertTrue(list != null && list.getLength() == 1 && "text inside node".equals(list.item(0).getTextContent()));
    }

    @Test
    public void parsujU96Html() throws IOException, SAXException, ParserConfigurationException {
        Document doc = parser.parse(new FileInputStream(SRC_TEST_RESOURCES_UBOATNET_U96_XML));
        assertNotNull(doc);
        NodeList list = doc.getElementsByTagName("title");
        assertTrue(list != null && list.getLength() == 1 && "The Type VIIC U-boat U-96 - German U-boats of WWII -\nuboat.net".equals(list.item(0).getTextContent()));
    }
}