package com.ravineteam.uboatnetparser.export;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.data.shiplosses.AllShipLosses;
import com.ravineteam.uboatnetparser.data.shiplosses.MonthlyShipLosses;
import com.ravineteam.uboatnetparser.parsers.shiplosses.MonthlyLossesParser;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class AllShipLossesExporter {

    private String HEADER_ALL_DATA = "WKT,month,ship name,convoy,nationality,date,tonnage,id,sunk,damaged,hit by mine,captured,total loss\n";
    private String HEADER_WITH_DATE = "WKT,date\n";

    private short FORMAT_POINT_ONLY = 0;
    private short FORMAT_WITH_DATE = 1;
    private short FORMAT_ALL_DATA = 2;

    public static void main(String[] args) {
        new AllShipLossesExporter().generate();
    }

    private void generate() {
        String filename = "src/main/resources/output/allShipLosses"+ LocalDateTime.now().toString().replace(":","")+".json";
        try (OutputStream os = new FileOutputStream(new File(filename));
             OutputStream osAllData = new FileOutputStream(new File(filename.replace("allShipLosses", "allShipLosses_date_")));
             OutputStream osWithDate = new FileOutputStream(new File(filename.replace("allShipLosses", "allShipLosses_allData_")))) {
            AllShipLossesExporter allShipLosses = new AllShipLossesExporter();
            AllShipLosses losses = allShipLosses.getAllLosses();
            if (losses != null) {
                losses.enumerate();
            }
            allShipLosses.generateWKT(os, losses, FORMAT_POINT_ONLY);
            allShipLosses.generateWKT(osWithDate, losses, FORMAT_WITH_DATE);
            allShipLosses.generateWKT(osAllData, losses, FORMAT_ALL_DATA);
        } catch (FileNotFoundException e) {
            System.err.println("No file to save parsed ship losses data. " + e.getCause().getMessage());
        } catch (IOException e) {
            System.err.println("Cannot save parsed ship losses data. " + e.getCause().getMessage());
        }
    }

    void generateWKT(OutputStream os, AllShipLosses losses, short format) {
        List<Position> positions = new LinkedList<>();
        DateConverter dateConverter = new DateConverter();
        try {
            if (format == FORMAT_ALL_DATA) {
                os.write(HEADER_ALL_DATA.getBytes());
            } else if (format == FORMAT_WITH_DATE) {
                os.write(HEADER_WITH_DATE.getBytes());
            }
        } catch (IOException e) {
            System.err.println("Error writing headers of ship losses: " + e.getCause().getMessage());
        }
        for (MonthlyShipLosses m : losses.getMonthlyShipLossesList()) {
            for (ShipHit s : m.getShipsHit()) {
                Position p = s.getPosition();
                if (p != null && p.isNotEmpty()) {
                    LocalDate hitDate = dateConverter.getLocalDateFrom_d_MMM_YYYY(s.getDateLabel());
                    String dateString = "";
                    if (hitDate != null)  dateString = hitDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
                    String line = "";
                    if (format == FORMAT_ALL_DATA) {
                        line = getLineAllData(m, s, p, dateString);
                    } else if (format == FORMAT_WITH_DATE) {
                        line = "\"POINT (" + p.getLongitude() + " " + p.getLatitude() + ")\", " + dateString;
                    } else {
                        line = "POINT (" + p.getLongitude() + " " + p.getLatitude() + ")";
                    }
                    try {
                        os.write(line.getBytes());
                    } catch (IOException e) {
                        System.err.println("Error writing POINT: " + e.getCause().getMessage());
                    }
                }
            }
        }
    }

    private String getLineAllData(MonthlyShipLosses m, ShipHit s, Position p, String dateString) {
        return "\"POINT (" + p.getLongitude() + " " + p.getLatitude() + ")\", "+ m.getStartDate().format(DateTimeFormatter.ISO_LOCAL_DATE) + ", " + s.getShipName() + ", " + s.getConvoy() + ", " + s.getNationality() + ", " + dateString + ", " + s.getTonnage() + ", " + s.getId() + ", " + s.isSunk() + ", " + s.isDamaged() + ", " + s.isHitByMine() + ", " + s.isCaptured() + ", " + s.isTotalLoss() + "\n";
    }

    void generateJson(OutputStream os, Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            os.write(gson.toJson(object).getBytes());
        } catch (IOException e) {
            System.err.println(e.getCause().getMessage());
        }
    }

    AllShipLosses getAllLosses() {
        MonthlyLossesParser parser = new MonthlyLossesParser();
        XmlParser xmlParser = new XmlParser();
        AllShipLosses losses = new AllShipLosses();
        for (int year = 1939; year <= 1945; year++) {
            for (int month = 1; month <= 12; month++) {
                if (year == 1939 && month == 1) month = 9;
                String filename = year + "-" + (month > 9 ? "" : "0") + month + ".html";
                File file = new File("src/test/resources/uboatnet/shiplosses/losses/" + filename);
                Document doc = null;
                try {
                    doc = xmlParser.parse(file);
                } catch (IOException | SAXException | ParserConfigurationException e) {
                    System.err.println(e.getCause().getMessage());
                }
                MonthlyShipLosses monthlyShipLosses = null;
                try {
                    monthlyShipLosses = parser.parse(doc);
                } catch (Exception e) {
                    System.err.println("Error in MontlyShipLossesParser");
                }
                losses.addMonthlyShipLossess(monthlyShipLosses);
                if (year == 1945 && month == 5) {
                    break;
                }
            }
        }
        return losses;
    }
}
