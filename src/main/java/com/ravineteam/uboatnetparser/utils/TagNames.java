package com.ravineteam.uboatnetparser.utils;

public class TagNames {

    public static final String TD = "td";
    public static final String TR = "tr";
    public static final String H1 = "h1";
    public static final String H3 = "h3";

    public static final String P = "p" ;
    public static final String A = "a";
    public static final String STRONG = "strong";
}
