package com.ravineteam.uboatnetparser.utils;


import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

public class NodeUtil {

    private TagFeeder tagFeeder;

    public NodeUtil() {
        tagFeeder = new TagFeeder();
    }

    public String getAttributeValue(Node item, String attributeName) {
        if (item != null && attributeName != null) {
            NamedNodeMap attributes = item.getAttributes();
            for (int i = 0; i < attributes.getLength(); i++) {
                Node att = attributes.item(i);
                if (attributeName.equals(att.getNodeName())) {
                    return att.getNodeValue();
                }
            }
        }
        return "";
    }

    public Collection<Node> getChildren(Node parentNode, String elementName) {
        Collection<Node> nodes = new LinkedHashSet<>();
        if (parentNode != null && elementName != null) {
            NodeList children = parentNode.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child != null && elementName.equals(child.getNodeName())) {
                    nodes.add(child);
                }
            }
        }
        return nodes;
    }

    public String getValue(Document doc, String tagName, String tagValue, int order, int valueOrder) {
        int i = tagFeeder.getTag(doc, tagName, tagValue, order);
        Node node = tagFeeder.getNode(doc, tagName, i + valueOrder);
        if (node != null) {
            return node.getTextContent();
        }
        return "";
    }

    public Node getNode(Document doc, String tagName, String tagValue, int order, int valueOrder) {
        int i = tagFeeder.getTag(doc, tagName, tagValue, order);
        return tagFeeder.getNode(doc, tagName, i + valueOrder);
    }

    public Node getNode(Document doc, String tagName, int order, String... tagValue) {
        int i = tagFeeder.getTag(doc, tagName, order, tagValue);
        return tagFeeder.getNode(doc, tagName, i);
    }

    public Node getNode(Document doc, String tagName, String valuePrefix) {
        NodeList nodeList = doc.getElementsByTagName(tagName);
        Node node = null;
        for (int i = 0; i < nodeList.getLength(); i++) {
            node = nodeList.item(i);
            if (StringUtils.startsWithIgnoreCase(node.getTextContent().replace("\n", " "), valuePrefix)) {
                break;
            }
        }
        return node;
    }

    public String getTextInBraces(Node node) {
        String value = node.getTextContent();
        int start = value.indexOf("(");
        int end = value.indexOf(")");
        if (start >= 0 && end > 0 && end > start) {
            return value.substring(start + 1, end);
        } else {
            return "";
        }
    }

    public List<Node> getChildrenNodesByOwnerNodeName(Node parentNode, String name) {
        List<Node> retVal = new LinkedList<>();
        if (StringUtils.isNotEmpty(name) && parentNode != null) {
            NodeList children = parentNode.getChildNodes();
            for(int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (name.equals(child.getParentNode().getNodeName())) {
                    retVal.add(child);
                }
                NodeList nextLevelChildren = child.getChildNodes();
                for(int j = 0; j < nextLevelChildren.getLength(); j++) {
                    List<Node> nextLevelRetVal = getChildrenNodesByOwnerNodeName(nextLevelChildren.item(j), name);
                    if (nextLevelRetVal != null && !nextLevelRetVal.isEmpty()) {
                        retVal.addAll(nextLevelRetVal);
                    }
                }
            }
        }
        return retVal;
    }

    public List<Node> getChildrenNodesByNodeName(Node parentNode, String name) {
        List<Node> retVal = new LinkedList<>();
        if (StringUtils.isNotEmpty(name) && parentNode != null) {
            NodeList children = parentNode.getChildNodes();
            for(int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (name.equals(child.getNodeName())) {
                    retVal.add(child);
                }
                NodeList nextLevelChildren = child.getChildNodes();
                for(int j = 0; j < nextLevelChildren.getLength(); j++) {
                    List<Node> nextLevelRetVal = getChildrenNodesByNodeName(nextLevelChildren.item(j), name);
                    if (nextLevelRetVal != null && !nextLevelRetVal.isEmpty()) {
                        retVal.addAll(nextLevelRetVal);
                    }
                }
            }
        }
        return retVal;
    }

    public Node getSiblingTable(Node startNode) {
        if (startNode != null) {
            Node sibling = startNode.getNextSibling();
            while (sibling != null) {
                if ("table".equals(sibling.getNodeName())) {
                    return sibling;
                }
                sibling = sibling.getNextSibling();
            }
        }
        return null;
    }

    /**
     * Retrieves chosen TD node from table
     * @param tableNode table node to be searched
     * @param row   index of row, counted from 0
     * @param column    index of column, counted from 0
     * @return  TD node from chosen row and column or null if found nothing
     */
    public Node getTd(Node tableNode, int row, int column) {
        if (tableNode != null) {
            List<Node> trNodes = getChildrenNodesByNodeName(tableNode, TagNames.TR);
            if (trNodes != null && trNodes.size() > row) {
                Node tr = trNodes.get(row);
                if (tr != null) {
                    List<Node> tdNodes = getChildrenNodesByNodeName(tr, TagNames.TD);
                    if (tdNodes != null && tdNodes.size() > column) {
                        return tdNodes.get(column);
                    }
                }
            }
        }
        return null;
    }
}
