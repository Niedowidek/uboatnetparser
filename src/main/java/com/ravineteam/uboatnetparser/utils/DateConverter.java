package com.ravineteam.uboatnetparser.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

public class DateConverter {

    public static final String MMM_YYYY = "MMM yyyy";
    public static final String MMMM_YYYY = "MMMM yyyy";
    public static final String D_MMM_YY = "d MMM yy";
    public static final String D_MMM_YYYY = "d MMM yyyy";
    public static final String D_MMMM_YYYY = "d MMMM yyyy";
    public static final String MMM_D_YYYY = "MMM d yyyy";
    public static final String MMMM_D_YYYY = "MMMM d yyyy";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public String to_yyyyMMdd(String date) {
        return convert(date, D_MMM_YYYY, YYYY_MM_DD);
        /*if (date != null) {
            LocalDate ld;
            try {
                ld = LocalDate.parse(date, DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH));
            } catch (java.time.format.DateTimeParseException e) {
                return "";
            }
            if (ld != null) {
                return ld.format(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH));
            }
        }
        return "";*/
    }

    public String to_d_MMM_yyyy(String date) {
        return convert(date, YYYY_MM_DD, D_MMM_YYYY);
    }

    public String convert (String date, String patternFrom, String patternTo) {
        if (StringUtils.isNoneEmpty(date, patternFrom, patternTo)) {
            LocalDate ld;
            try {
                ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(patternFrom, Locale.ENGLISH));
            } catch (java.time.format.DateTimeParseException e) {
                return "";
            }
            if (ld != null) {
                return ld.format(DateTimeFormatter.ofPattern(patternTo, Locale.ENGLISH));
            }
        }
        return "";
    }

    public LocalDate getLocalDateFrom_d_MMM_YYYY (String date) {
        LocalDate ld = null;
        if (StringUtils.isNotEmpty(date)) {
            date = date.replace(", ", " ").replace(".", "").trim();
            if (",".equals(date.substring(date.length()-1))) {
                date = date.substring(0, date.length() - 1);
            }
            try {
                ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(D_MMM_YYYY, Locale.ENGLISH));
            } catch (java.time.format.DateTimeParseException e) {
                try {
                    ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(D_MMMM_YYYY, Locale.ENGLISH));
                } catch (DateTimeParseException e2) {
                    try {
                        ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(MMM_YYYY, Locale.ENGLISH));
                    } catch (DateTimeParseException e3) {
                        try {
                            ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(MMMM_YYYY, Locale.ENGLISH));
                        } catch (DateTimeParseException e4) {
                            try {
                                ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(D_MMM_YY, Locale.ENGLISH));
                            } catch (DateTimeParseException e5) {
                                try {
                                    ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(MMMM_D_YYYY, Locale.ENGLISH));
                                } catch (DateTimeParseException e6) {
                                    try {
                                        ld = LocalDate.parse(date, DateTimeFormatter.ofPattern(MMM_D_YYYY, Locale.ENGLISH));
                                    } catch (DateTimeParseException e7) {
//                                        System.err.println("Cannot parse date: " + date);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return ld;
    }
}
