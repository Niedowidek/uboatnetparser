package com.ravineteam.uboatnetparser.utils;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TagFeeder {

    public static final int NOT_FOUND = -1;

    public int getTag(Document doc, String tagName, String tagValue, int order) {
        return getTag(doc, tagName, tagValue, order, true);
    }

    private int getTag(Document doc, String tagName, String tagValue, int order, boolean exactMatch) {
        return getTag(doc, tagName, order, exactMatch, tagValue);
        /*if (tagName != null && doc != null) {
            NodeList nodeList = doc.getElementsByTagName(tagName);
            if (nodeList != null) {
                int counter = 0;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (matches(node, tagValue, exactMatch)) {
                        counter++;
                        if (counter == order)
                            return i + 1;
                    }
                }
            }
        }
        return NOT_FOUND;*/
    }

    public int getTag(Document doc, String tagName, int order, String... tagValue) {
        return getTag(doc, tagName, order, false, tagValue);
    }

    private int getTag(Document doc, String tagName, int order, boolean exactMatch, String... tagValue) {
        if (tagName != null && doc != null) {
            NodeList nodeList = doc.getElementsByTagName(tagName);
            if (nodeList != null) {
                int counter = 0;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    if (matches(node, exactMatch, tagValue)) {
                        counter++;
                        if (counter == order)
                            return i + 1;
                    }
                }
            }
        }
        return NOT_FOUND;
    }

    private boolean matches(Node node, boolean exactMatch, String... tagValueArray) {
        if (tagValueArray.length == 1) {
            String tagValue = tagValueArray[0];
            return (exactMatch && node.getTextContent().equals(tagValue)) || (!exactMatch && node.getTextContent().contains(tagValue));
        } else {
            boolean matches = true;
            for (String s : tagValueArray) {
                if (!StringUtils.contains(node.getTextContent(), s)) {
                    matches = false;
                }
            }
            return matches;
        }
    }

    public Node getNode(Document doc, String tagName, int i) {
        if (doc != null) {
            NodeList nodeList = doc.getElementsByTagName(tagName);
            if (nodeList != null  && i > 0 && nodeList.getLength() >= (i-1)) {
                return nodeList.item(i-1);
            }
        }
        return null;
    }

    public Node getNode(Document doc, String tagName, String tagValue, int order) {
        return getNode(doc, tagName, tagValue, order, true);
    }

    public Node getNode(Document doc, String tagName, String tagValue, int order, boolean exactMatch) {
        int n = getTag(doc, tagName, tagValue, order, exactMatch);
        return getNode(doc, tagName, n);
    }
}
