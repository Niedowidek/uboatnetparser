package com.ravineteam.uboatnetparser.utils;

import org.w3c.dom.Node;

import java.util.Collection;

public class HrefUtil {

    NodeUtil nodeUtil;

    public HrefUtil() {
        this.nodeUtil = new NodeUtil();
    }

    public String getHrefFilenameWithoutExtension(Node node) {
        if(node != null) {
            String href = getHref(node);
            String id = getFilenameWithoutExtension(href);
            String folder = getFolder(href);
            if ("commanders".equals(folder)) {
                return "commanders_" + id;
            } else {
                return id;
            }
        }
        return "";
    }

    private String getFolder(String href) {
        String s = "";
        if (href != null) {
            int i = href.lastIndexOf("/");
            if (i >0) {
                s = href.substring(0, i);
                i = s.lastIndexOf("/");
                s = s.substring(i + 1);
            } else {
                System.err.println("No folders for href: " + href);
            }
        }
        return s;
    }

    private String getFilenameWithoutExtension(String href) {
        int i = href.lastIndexOf("/");
        return href.substring(i+1).replace(".html", "").replace(".htm", "");
    }

    public String getHref(Node node) {
        if (node != null) {
            Collection<Node> aElements = nodeUtil.getChildren(node, "a");
            if (aElements != null && aElements.iterator().hasNext()) {
                Node idNode = aElements.iterator().next();
                return nodeUtil.getAttributeValue(idNode, "href");
            }
        }
        return "";
    }

    public String getANodeTextContent(Node node) {
        if (node != null) {
            Collection<Node> aElements = nodeUtil.getChildren(node, "a");
            if (aElements != null && aElements.iterator().hasNext()) {
                Node idNode = aElements.iterator().next();
                return idNode.getTextContent();
            }
        }
        return "";
    }
}
