package com.ravineteam.uboatnetparser.parsers.convoys;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.convoys.ConvoyRoute;
import com.ravineteam.uboatnetparser.parsers.patrol.MarkerParser;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class ConvoyRouteParser {

    private final String CODE_START = "Convoy route";

    private final String ROUTE_SECTION_START = "Print out the convoy route";
    private final String ROUTE_START = "= [";
    private final String ROUTE_END = "]";
    private final String POSITION_START = "google.maps.LatLng(";
    private final String POSITION_END = ")";

    public ConvoyRoute parse (Document doc) {
        ConvoyRoute route = new ConvoyRoute();

        Node h1Node = getConvoyCode(doc, route);
        getConvoyInfo(route, h1Node);
        List<String> routesContentList = getRouteDescriptions(doc);
        addPositionsToRoute(route, routesContentList);
        return route;
    }

    private Node getConvoyCode(Document doc, ConvoyRoute route) {
        String tagName = TagNames.H1;
        String valuePrefix = CODE_START;
        NodeList h1Elements = doc.getElementsByTagName(tagName);
        Node h1Node = null;
        for (int i = 0; i < h1Elements.getLength(); i++) {
            h1Node = h1Elements.item(i);
            if (StringUtils.startsWithIgnoreCase(h1Node.getTextContent(), valuePrefix)) {
                route.setCode(h1Node.getTextContent().replace(valuePrefix, "").trim());
                break;
            }
        }
        return h1Node;
    }

    private void getConvoyInfo(ConvoyRoute route, Node h1Node) {
        NodeUtil nodeUtil = new NodeUtil();

        Node table = null;
        if (h1Node != null) {
            table = nodeUtil.getSiblingTable(h1Node);
        }

        if (table != null) {
            Node routeDescriptionNode = nodeUtil.getTd(table, 1,1);
            String routeDescription = routeDescriptionNode != null ? routeDescriptionNode.getTextContent() : "";
            route.setRouteDescription(routeDescription);

            Node areaNode = nodeUtil.getTd(table, 2,1);
            String area = areaNode != null ? areaNode.getTextContent() : "";
            route.setArea(area);

            Node notesNode = nodeUtil.getTd(table, 3, 1);
            String notes = notesNode != null ? notesNode.getTextContent().replace("\n", " ").trim() : "";
            route.setNotes(notes);
        }
    }

    private List<String> getRouteDescriptions(Document doc) {
        MarkerParser markerParser = new MarkerParser();
        String scriptContent = markerParser.getScriptsContent(doc);
        List<String> routesContentList = new LinkedList<>();
        if (StringUtils.isNotEmpty(scriptContent)) {
            int routeSectionStartIndex = scriptContent.lastIndexOf(ROUTE_SECTION_START);
            String routeSectionContent = "";
            if (routeSectionStartIndex >= 0) {
                routeSectionContent = scriptContent.substring(routeSectionStartIndex);
            }
            int routeStartIndex = routeSectionContent.indexOf(ROUTE_START);
            while (routeStartIndex >= 0 ) {
                int routeEndIndex = routeSectionContent.indexOf(ROUTE_END, routeStartIndex);
                if (routeEndIndex > 0) {
                    String routeContent = routeSectionContent.substring(routeStartIndex + ROUTE_START.length(), routeEndIndex).trim();
                    if (StringUtils.isNotEmpty(routeContent)) {
                        routesContentList.add(routeContent);
                    }
                }
                routeStartIndex = routeSectionContent.indexOf(ROUTE_START, routeEndIndex);
            }
        }
        return routesContentList;
    }

    private void addPositionsToRoute(ConvoyRoute route, List<String> routesContentList) {
        if (routesContentList.size() > 0) {
            int counter = 0;
            for (String routeContent : routesContentList) {
                List<Position> positionList = new LinkedList<>();
                int positionStartIndex = routeContent.indexOf(POSITION_START);
                do {
                    int positionEndIndex = routeContent.indexOf(POSITION_END, positionStartIndex);
                    String positionContent = routeContent.substring(positionStartIndex + POSITION_START.length(), positionEndIndex).trim();
                    String[] coords = positionContent.split(",");
                    if (coords != null && coords.length == 2) {
                        Position position = new Position(coords[0], coords[1]);
                        if (position.isNotEmpty()) {
                            if (counter == 0) {
                                route.addNextRoutePosition(position);
                            } else if (counter > 0) {
                                route.addNextPositionToRouteVariation(counter - 1, position);
                            }
                        }
                    }
                    positionStartIndex = routeContent.indexOf(POSITION_START, positionEndIndex);
                } while (positionStartIndex >= 0);
                counter++;
            }
        }
    }

}
