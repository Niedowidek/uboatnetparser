package com.ravineteam.uboatnetparser.parsers.men;

import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.data.men.Man;
import com.ravineteam.uboatnetparser.utils.HrefUtil;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MenParser {

    public static final String SUCCESSES = "Successes";
    public static final String TOTAL_TONNAGE = "total tonnage";

    public Man parse(Document doc) {
        Man man = new Man();
        Node table = null;
        NodeUtil nodeUtil = new NodeUtil();
        NodeList h1Nodes = doc.getElementsByTagName(TagNames.H1);
        if (h1Nodes != null && h1Nodes.getLength() == 2) {
            man.setName(h1Nodes.item(1).getTextContent());
            table = nodeUtil.getSiblingTable(h1Nodes.item(1));
        }
        NodeList h3Nodes = doc.getElementsByTagName(TagNames.H3);
        HrefUtil hrefUtil = new HrefUtil();
        if (h3Nodes != null && h3Nodes.getLength() > 0) {
            Node h3node = h3Nodes.item(0);
            String href = hrefUtil.getHref(h3node);
            if (StringUtils.containsIgnoreCase(href, "crew")) {
                man.setCrewHref(href);
            }
            String aContent = hrefUtil.getANodeTextContent(h3node);
            if (StringUtils.isNotEmpty(aContent)) {
                man.setCrewName(aContent);
            }
        }

        if (table != null) {
            String content = table.getTextContent();
            if (StringUtils.containsIgnoreCase(content, SUCCESSES)) {
                content = content.replace(SUCCESSES, "").trim();
                String[] lines = content.split("\n");
                if (lines != null && lines.length > 0) {
                    Tonnage tonnage = new Tonnage();
                    for (String line : lines) {
                        if (StringUtils.containsIgnoreCase(line, "warship sunk") || StringUtils.containsIgnoreCase(line, "warships sunk")) {
                            tonnage.setWarshipsSunk(getNumber(line));
                            tonnage.setWarshipsSunkTonnage(getTonnage(line));
                        } else if (StringUtils.containsIgnoreCase(line, "warship damaged") || StringUtils.containsIgnoreCase(line, "warships damaged")) {
                            tonnage.setWarshipsDamaged(getNumber(line));
                            tonnage.setWarshipsDamagedTonnage(getTonnage(line));
                        } else if (StringUtils.containsIgnoreCase(line, "warship") && StringUtils.containsIgnoreCase(line, "total loss")) {
                            tonnage.setWarshipsTotalLoss(getNumber(line));
                            tonnage.setWarshipsTotalLossTonnage(getTonnage(line));
                        } else if (StringUtils.containsIgnoreCase(line, "ship sunk") || StringUtils.containsIgnoreCase(line, "ships sunk")) {
                            tonnage.setShipsSunk(getNumber(line));
                            tonnage.setShipsSunkTonnage(getTonnage(line));
                        } else if (StringUtils.containsIgnoreCase(line, "ship damaged") || StringUtils.containsIgnoreCase(line, "ships damaged")) {
                            tonnage.setShipsDamaged(getNumber(line));
                            tonnage.setShipsDamagedTonnage(getTonnage(line));
                        } else if (StringUtils.containsIgnoreCase(line, "ship") && StringUtils.containsIgnoreCase(line, "total loss")) {
                            tonnage.setShipsTotalLoss(getNumber(line));
                            tonnage.setShipsTotalLossTonnage(getTonnage(line));
                        }
                    }
                    man.setTonnage(tonnage);
                }
            }
        }

        return man;
    }

    private int getTonnage(String line) {
        int tonnage = 0;
        if (StringUtils.isNotEmpty(line)) {
            int index = line.indexOf(TOTAL_TONNAGE);
            if (index >= 0) {
                line = line.substring(index + TOTAL_TONNAGE.length()).replace("GRT", "").trim();
                if (StringUtils.isNotEmpty(line)) {
                    line = line.replace(",","");
                    try {
                        tonnage = Integer.parseInt(line);
                    } catch (NumberFormatException e) {
                        System.err.println("Cannot parse tonnage from commander info");
                    }
                }
            }
        }
        return tonnage;
    }

    private int getNumber(String line) {
        int number = 0;
        if (StringUtils.isNotEmpty(line)) {
            int firstSpace = line.indexOf(" ");
            if (firstSpace > 0) {
                line = line.substring(0, firstSpace).trim();
            }
            if (StringUtils.isNotEmpty(line)) {
                try {
                    number = Integer.parseInt(line);
                } catch (NumberFormatException e) {
                    System.err.println("Cannot parse ships sunk from commander info");
                }
            }
        }
        return number;
    }
}
