package com.ravineteam.uboatnetparser.parsers.shiplosses;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.parsers.patrol.ShipHitListParser;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class LossesShipHitParser extends ShipHitListParser {

    public LossesShipHitParser() {
        numberOfColumns = 9;
    }

    @Override
    public List<ShipHit> parse (Document doc) {
        List<ShipHit> shipHitList = new LinkedList<>();
        String typeSunk = markerParser.determineProperMarker(doc, new String[] {"Ship sunk"});
        String typeDamaged = markerParser.determineProperMarker(doc, new String[] {"Ship damaged"});
        Node node = nodeUtil.getNode(doc, TagNames.P, "This is the full listing of all ships hit during");
        Node divNode = getDivNode(node);
        List<Node> rowNodes = getRowNodes(divNode);
        if (!rowNodes.isEmpty()) {
            for (Node row : rowNodes) {
                List<Node> tdNodes = nodeUtil.getChildrenNodesByNodeName(row, TagNames.TD);
                if (tdNodes != null && !tdNodes.isEmpty() && tdNodes.size() == numberOfColumns) {
                    addShip(doc, shipHitList, tdNodes, null);
                } else if (tdNodes != null && tdNodes.size() != numberOfColumns) {
                    System.err.println("Incorrect number of columns in a table with ship losses");
                }
            }
        }
        return shipHitList;
    }

    @Override
    protected String[] getTitleParts() {
        return new String[] {"Ship losses by month"};
    }

    @Override
    protected void addShip(Document doc, List<ShipHit> shipHitList, List<Node> tdNodes, String type) {
        ShipHit shipHit = new ShipHit();
        for (int i = 0; i < numberOfColumns; i++) {
            Node td = tdNodes.get(i);
            assignData(shipHit, i, td);
        }
        String searchedText = shipHit.getShipName();
        Marker marker = markerParser.getMarker(doc, searchedText);
        shipHit.setPosition(marker.getPosition());
        shipHit.setTextFromMarker(marker.getTextContent());
        shipHit.setTextFromMarkerWithHtml(marker.getTextContentWithHtmlTags());
        shipHitList.add(shipHit);
    }
}
