package com.ravineteam.uboatnetparser.parsers.shiplosses;

import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.data.shiplosses.MonthlyShipLosses;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.LocalDate;
import java.util.List;

public class MonthlyLossesParser  {

    private LossesShipHitParser parser;

    public MonthlyLossesParser() {
        this.parser = new LossesShipHitParser();
    }

    public MonthlyShipLosses parse(Document doc) throws Exception {
        if (doc == null) {
            throw new Exception("Document is null");
        }
        MonthlyShipLosses losses = new MonthlyShipLosses();

        setDateFromTitle(doc, losses);
        List<ShipHit> shipHitList = parser.parse(doc);
        losses.setShipHitList(shipHitList);

        return losses;
    }

    private void setDateFromTitle(Document doc, MonthlyShipLosses losses) throws Exception {
        NodeList titleList = doc.getElementsByTagName("title");
        if (titleList != null && titleList.getLength() > 0) {
            Node titleNode = titleList.item(0);
            String title = titleNode.getTextContent();
            if (!StringUtils.startsWithIgnoreCase(title, "Ship losses by month")) {
                throw new Exception("Wrong html file");
            }
            if (StringUtils.isNotEmpty(title)) {
                title = title.replace("Ship losses by month -", "").replace("- uboat.net", "").trim();
                String[] parts = title.split(" ");
                if (parts != null && parts.length == 2) {
                    String yearParsed = parts[1];
                    String monthParsed = "";
                    switch(parts[0]) {
                        case "January" : monthParsed = "01"; break;
                        case "February" : monthParsed = "02"; break;
                        case "March" : monthParsed = "03"; break;
                        case "April" : monthParsed = "04"; break;
                        case "May" : monthParsed = "05"; break;
                        case "June" : monthParsed = "06"; break;
                        case "July" : monthParsed = "07"; break;
                        case "August" : monthParsed = "08"; break;
                        case "September" : monthParsed = "09"; break;
                        case "October" : monthParsed = "10"; break;
                        case "November" : monthParsed = "11"; break;
                        case "December" : monthParsed = "12"; break;
                    }
                    if (StringUtils.isNoneEmpty(yearParsed, monthParsed)) {
                        if (monthParsed.startsWith("0")) monthParsed = monthParsed.substring(1);
                        try {
                            int month = Integer.parseInt(monthParsed);
                            int year = Integer.parseInt(yearParsed);
                            LocalDate date = LocalDate.of(year, month, 1);
                            losses.setStartDate(date);
                        } catch (NumberFormatException e) {
                            System.err.println("Cannot parse date of monthly losses page");
                        }
                    }
                }
            }
        }
    }
}
