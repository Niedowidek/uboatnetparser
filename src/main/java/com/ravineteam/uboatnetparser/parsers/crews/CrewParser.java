package com.ravineteam.uboatnetparser.parsers.crews;

import com.ravineteam.uboatnetparser.data.crews.Command;
import com.ravineteam.uboatnetparser.data.crews.Crew;
import com.ravineteam.uboatnetparser.data.crews.TrainedCommander;
import com.ravineteam.uboatnetparser.utils.HrefUtil;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;


public class CrewParser {

    private String searchedTitleFragment = "Kriegsmarine Crews - Crew";

    public Crew parse(Document doc) {
        Crew crew = new Crew();

        NodeList titleList = doc.getElementsByTagName("title");
        parseTitle(crew, titleList);

        NodeList tables = doc.getElementsByTagName("table");
        NodeUtil nodeUtil = new NodeUtil();
        List<Node> trFound = findRowsWithData(tables, nodeUtil);
        parseTrainedCommanders(crew, nodeUtil, trFound);
        return crew;
    }

    private void parseTitle(Crew crew, NodeList titleList) {
        if (titleList != null && titleList.getLength() == 1) {
            Node titleNode = titleList.item(0);
            String fullTitle = titleNode.getTextContent();
            int index = StringUtils.indexOf(fullTitle, searchedTitleFragment);
            if (index > 0) {
                crew.setName(fullTitle.substring(index + searchedTitleFragment.length()).trim());
            }
        }
    }

    private List<Node> findRowsWithData(NodeList tables, NodeUtil nodeUtil) {
        List<Node> trFound = null;
        for (int i = 0; i < tables.getLength(); i++) {
            Node table = tables.item(i);
            List<Node> trNodes = nodeUtil.getChildrenNodesByNodeName(table, TagNames.TR);
            if (trNodes != null && trNodes.size() > 0) {
                String textContent = trNodes.get(0).getTextContent();
                if (StringUtils.containsIgnoreCase(textContent, "Rank")
                && StringUtils.containsIgnoreCase(textContent, "Commander")
                && StringUtils.containsIgnoreCase(textContent, "Commands")) {
                    trFound = trNodes;
                    break;
                }
            }
        }
        return trFound;
    }

    private void parseTrainedCommanders(Crew crew, NodeUtil nodeUtil, List<Node> trFound) {
        if (trFound != null && trFound.size() > 0) {
            HrefUtil hrefUtil = new HrefUtil();
            for (int i = 1; i < trFound.size(); i++) {
                Node tr = trFound.get(i);
                if(tr != null) {
                    List<Node> tdNodes = nodeUtil.getChildrenNodesByNodeName(tr, TagNames.TD);
                    if (tdNodes != null && tdNodes.size() == 4 ) {
                        TrainedCommander trainedCommander = new TrainedCommander();
                        trainedCommander.setRank(tdNodes.get(1).getTextContent());
                        trainedCommander.setName(hrefUtil.getANodeTextContent(tdNodes.get(2)));
                        trainedCommander.setHref(hrefUtil.getHref(tdNodes.get(2)));
                        List<Node> commandNodes = nodeUtil.getChildrenNodesByNodeName(tdNodes.get(3), TagNames.A);
                        parseCommands(nodeUtil, trainedCommander, commandNodes);
                        crew.getTrainedCommanderList().add(trainedCommander);
                    }
                }
            }
        }
    }

    private void parseCommands(NodeUtil nodeUtil, TrainedCommander trainedCommander, List<Node> commandNodes) {
        if (commandNodes != null && commandNodes.size() > 0) {
            List<Command> commandList = new LinkedList<>();
            for (Node node : commandNodes) {
                Command command = new Command();
                command.setName(node.getTextContent());
                command.setHref(nodeUtil.getAttributeValue(node, "href"));
                trainedCommander.getCommandList().add(command);
            }
        }
    }
}
