package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.data.uboat.*;
import com.ravineteam.uboatnetparser.utils.HrefUtil;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagFeeder;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.ravineteam.uboatnetparser.utils.TagNames.H3;
import static com.ravineteam.uboatnetparser.utils.TagNames.TD;

public class UboatParser
{


    private final TagFeeder tagFeeder;
    private final NodeUtil nodeUtil;

    public UboatParser() {
        this.tagFeeder = new TagFeeder();
        nodeUtil = new NodeUtil();
    }

    private String getValue(Document doc, String tagName, String tagValue, int order, int valueOrder) {
        return new NodeUtil().getValue(doc, tagName, tagValue, order, valueOrder);
    }

    public String getTypeValue(Document doc) {
        return getValue(doc, H3, "Type", 1, 1);
    }

    public String getOrderedValue(Document doc) {
        return getValue(doc, TD, "Ordered", 1, 1);
    }

    public String getLaidDownWhenValue(Document doc) {
        return getValue(doc, TD, "Laid down", 1, 1);
    }

    public String getLaidDownWhere(Document doc) {
        return getValue(doc, TD, "Laid down", 1, 2).replace("\n", " ");
    }

    public String getLaidDownWhereHref(Document doc) {
        Node node = new NodeUtil().getNode(doc, TD, "Laid down", 1, 2);
        return new HrefUtil().getHref(node);
    }

    public String getLaunchedValue(Document doc) {
        return getValue(doc, TD, "Launched", 1, 1);
    }

    public String getCommissionedWhenValue(Document doc) {
        return getValue(doc, TD, "Commissioned", 1, 1);
    }

    public String getCommissionedCommanderValue(Document doc) {
        return getValue(doc, TD, "Commissioned", 1, 2);
    }

    public List<AssignedCommander> getCommanders(Document doc) {
        List<AssignedCommander> commanders = new LinkedList<>();
        if (doc != null) {
            String tagValue = "Commanders";
            Node node = tagFeeder.getNode(doc, TD, tagValue, 1);
            if (node != null) {
                Node table = node.getNextSibling().getNextSibling().getFirstChild().getNextSibling();
                Collection<Node> trElements = nodeUtil.getChildren(table, "tr");
                for (Node trChildNode : trElements) {
                    Collection<Node> tdElements = nodeUtil.getChildren(trChildNode, TD);
                    List<Node> tdElementsList = new LinkedList<>(tdElements);
                    String dateFrom = tdElementsList.get(0).getTextContent();
                    String dateTo = tdElementsList.get(2).getTextContent();
                    Node commander = tdElementsList.get(4);
                    String href = new HrefUtil().getHref(commander);
                    String commanderId = new HrefUtil().getHrefFilenameWithoutExtension(commander);
                    String commanderRank = getCommanderRank(commander);
                    List<String> decorationIds = getDecorationIds(commander);
                    AssignedCommander assignedCommander = new AssignedCommander(commanderId, commanderRank, decorationIds, dateFrom, dateTo);
                    assignedCommander.setHref(href);
                    commanders.add(assignedCommander);
                }
            }
        }
        return commanders;
    }

    public List<ServedFlotilla> getFlotillas(Document doc) {
        List<ServedFlotilla> flotillas = new LinkedList<>();
        if (doc != null) {
            Node node = tagFeeder.getNode(doc, TD, "Career", 1, false);
            if (node != null) {
                Node table = node.getNextSibling().getNextSibling().getFirstChild().getNextSibling();
                Collection<Node> trElements = nodeUtil.getChildren(table, "tr");
                for (Node trChildNode : trElements) {
                    Collection<Node> tdElements = nodeUtil.getChildren(trChildNode, TD);
                    List<Node> tdElementsList = new LinkedList<>(tdElements);
                    String dateFrom = tdElementsList.get(0).getTextContent();
                    String dateTo = tdElementsList.get(2).getTextContent();
                    Node flotillaNode = tdElementsList.get(3);
//                    String href = new HrefUtil().getHref(flotillaNode);
                    String flotillaId = new HrefUtil().getHrefFilenameWithoutExtension(flotillaNode);
                    String typeOfDuty = new NodeUtil().getTextInBraces(flotillaNode);
                    ServedFlotilla sf = new ServedFlotilla(dateFrom, dateTo, flotillaId, typeOfDuty);
                    flotillas.add(sf);
                }
            }
        }
        return flotillas;
    }

    public Tonnage getSuccesses(Document doc) {
        String s = getValue(doc, TD, "Successes", 1, 1);
        Tonnage successes = new Tonnage();
        if (s != null && !s.isEmpty()) {
            SuccessesParser parser = new SuccessesParser();

            successes.setShipsSunk(parser.getShipsSunk(s));
            successes.setShipsSunkTonnage(parser.getShipsSunkTonnage(s));
            successes.setShipsDamaged(parser.getShipsDamaged(s));
            successes.setShipsDamagedTonnage(parser.getShipsDamagedTonnage(s));
            successes.setShipsTotalLoss(parser.getShipsTotalLoss(s));
            successes.setShipsTotalLossTonnage(parser.getShipsTotalLossTonnage(s));

            successes.setWarshipsSunk(parser.getWarshipsSunk(s));
            successes.setWarshipsSunkTonnage(parser.getWarshipsSunkTonnage(s));
            successes.setWarshipsDamaged(parser.getWarshipsDamaged(s));
            successes.setWarshipsDamagedTonnage(parser.getWarshipsDamagedTonnage(s));
            successes.setWarshipsTotalLoss(parser.getWarshipsTotalLoss(s));
            successes.setWarshipsTotalLossTonnage(parser.getWarshipsTotalLossTonnage(s));
        }
        return successes;
    }

    public List<Fate> getFates(Document doc) {
        return new FateParser().getFates(doc);
    }

    public Position getFinalPosition(Document doc) {
        return new FinalPositionParser().getFinalPosition(doc);
    }

    public List<WolfpackService> getWolfpacks(Document doc) { return new WolfpackParser().parse(doc);}

    public Uboat getUboat(File file) throws ParserConfigurationException, SAXException, IOException {
        Document doc = new XmlParser().parse(file);
        if (doc != null) {
            String path = file.getAbsolutePath();
            int cutIndex = 0;
            if (StringUtils.containsIgnoreCase(path, "boats")) {
                cutIndex = path.lastIndexOf("boats");
            } else {
                cutIndex = path.lastIndexOf(File.separator) + 1 ;
            }
            if (cutIndex > 0) {
                path = path.substring(cutIndex);
            }
            if (path.contains("xml")) {
                path = path.replace("xml", "htm");
            }
            return getUboat(doc, path);
        }
        return new Uboat();
    }

    public Uboat getUboat(Document doc) {
        return getUboat(doc, "");
    }

    public Uboat getUboat(Document doc, String href) {
        Uboat uboat = new Uboat();
        uboat.setHref(href);
        String nr = getBoatNumber(doc);
        uboat.setNumber(nr);
        System.out.println("Parsing uboat " + nr + " , href: " + href);
        uboat.setType(getTypeValue(doc));
        uboat.setLaunched(getLaunchedValue(doc));
        uboat.setOrdered(getOrderedValue(doc));
        uboat.setLaidDownWhen(getLaidDownWhenValue(doc));
        uboat.setLaidDownWhere(getLaidDownWhere(doc));
        uboat.setLaidDownWhereHref(getLaidDownWhereHref(doc));
        uboat.setCommisioned(getCommissionedWhenValue(doc));
        uboat.setCommisionedCommander(getCommissionedCommanderValue(doc));
        uboat.setAssignedCommanders(getCommanders(doc));
        uboat.setServedFlotillas(getFlotillas(doc));
        uboat.setSuccesses(getSuccesses(doc));
        uboat.setFates(getFates(doc));
        uboat.setFinalPosition(getFinalPosition(doc));
        uboat.setWolfpacks(getWolfpacks(doc));
        return uboat;
    }



    private List<String> getDecorationIds(Node commander) {
        List<String> decorations = new LinkedList<>();
        if (commander != null) {
            Collection<Node> aElements = nodeUtil.getChildren(commander, "a");
            Iterator<Node> iterator = aElements.iterator();
            iterator.next(); // omit <a> element with link to commander page
            while (iterator.hasNext()) {
                Node node = iterator.next();
                if (node != null) {
                    String href = nodeUtil.getAttributeValue(node, "href");
                    int i = href.lastIndexOf("/");
                    decorations.add(href.substring(i+1).replace(".html", "").replace(".htm", ""));
                }
            }
        }
        return decorations;
    }

    private String getCommanderRank(Node commander) {
        if (commander != null) {
            String content = commander.getTextContent();
            if (content != null) {
                String parts[] = content.split("\\.");
                String rank = parts[0];
                if (parts.length == 1) {
                    System.err.println("Cannot split rank content by .  - whole commander node text content: " + content.replace("\n", " "));
                } else if (parts.length > 1 && parts[1] != null && parts[1].contains("(R)")) {
                    return rank.trim() + ". (R)";
                } else {
                    return rank.trim() + ".";
                }
            }
        }
        return "";
    }

    public String getBoatNumber(Document doc) {
        Node node = tagFeeder.getNode(doc, "h1", 2);
        return node != null ? node.getTextContent() : "";
    }
}
