package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.uboat.WolfpackService;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagFeeder;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class WolfpackParser {

    private static final String H3 = "h3";
    private final TagFeeder tagFeeder;
    private final NodeUtil nodeUtil;

    public WolfpackParser() {
        this.tagFeeder = new TagFeeder();
        this.nodeUtil = new NodeUtil();
    }

    public List<WolfpackService> parse(Document doc) {
        List<WolfpackService> wolfpacks = new LinkedList<>();
        if (doc != null) {
            String tagValue = "Wolfpack operations";
            Node node = tagFeeder.getNode(doc, H3, tagValue, 1);
            if (node != null) {
                Node firstWolfpack = getFirstWolfpack(node);
                List<Node> wolfpackNodes = new LinkedList<>();
                if (firstWolfpack != null) {
                    getWolfpackNodes(firstWolfpack, wolfpackNodes);
                }
                if (wolfpackNodes.size() > 0) {
                    processWolfpackNodes(wolfpacks, wolfpackNodes);
                }
            }
        }
        return  wolfpacks;
    }

    private void getWolfpackNodes(Node firstWolfpack, List<Node> wolfpackNodes) {
        wolfpackNodes.add(firstWolfpack);
        Node temp = firstWolfpack.getNextSibling();
        while (temp!= null && !StringUtils.equalsAny(temp.getNodeName(), "h3")) {
            while (temp!= null && !StringUtils.equalsAny(temp.getNodeName(), "a")) {
                temp = temp.getNextSibling();
            }
            if (temp == null) break;
            String hrefValue = nodeUtil.getAttributeValue(temp, "href");
            if (StringUtils.contains(hrefValue, "wolfpack")) {
                wolfpackNodes.add(temp);
            }
            temp = temp.getNextSibling();
        }
    }

    private void processWolfpackNodes(List<WolfpackService> wolfpacks, List<Node> wolfpackNodes) {
        for (Node wolfpack : wolfpackNodes) {
            String hrefValue = nodeUtil.getAttributeValue(wolfpack, "href");
            String name = wolfpack.getFirstChild().getTextContent();
            String dates = wolfpack.getNextSibling().getTextContent().replace("\n"," ");
            String from = dates.replace("(","").substring(0, dates.indexOf("-") - 1).trim();
            String to = dates.replace(")","").substring(dates.indexOf("-") + 1).trim();
            DateConverter dateConverter = new DateConverter();
            String dateFrom = dateConverter.to_yyyyMMdd(from);
            String dateTo = dateConverter.to_yyyyMMdd(to);
            System.out.println();
            WolfpackService wolfpackService = new WolfpackService();
            wolfpackService.setHref(hrefValue);
            wolfpackService.setName(name);
            wolfpackService.setDateFromFormatted(dateFrom);
            wolfpackService.setDateToFormatted(dateTo);
            wolfpacks.add(wolfpackService);
        }
    }

    private Node getFirstWolfpack(Node node) {
        Node wolfpack = null;
        Node temp = node.getNextSibling().getNextSibling().getFirstChild();
        while (!StringUtils.equalsAny(temp.getNodeName(), "a", "h3")) {
            temp = temp.getNextSibling();
        }
        if ("a".equals(temp.getNodeName())) {
            String hrefValue = nodeUtil.getAttributeValue(temp, "href");
            if (StringUtils.contains(hrefValue, "wolfpack")) {
                wolfpack = temp;
            }
        }
        return wolfpack;
    }

}
