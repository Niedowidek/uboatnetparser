package com.ravineteam.uboatnetparser.parsers.uboat;

public class SuccessesParser {
    public int getShipsSunk(String s) {
        int i = getNumberBeforeWords(s, " ships sunk");
        int j = getNumberBeforeWords(s, " ship sunk");
        return i > 0 ? i : j;
    }

    public int getShipsSunkTonnage(String s) {
        int i = getTonnage(s, " ships sunk");
        int j = getTonnage(s, " ship sunk");
        return i > 0 ? i : j;
    }

    public int getShipsDamaged(String s) {
        int i = getNumberBeforeWords(s, " ships damaged");
        int j = getNumberBeforeWords(s, " ship damaged");
        return i > 0 ? i : j;
    }

    public int getShipsDamagedTonnage(String s) {
        int i = getTonnage(s, " ships damaged");
        int j = getTonnage(s, " ship damaged");
        return i > 0 ? i : j;
    }

    public int getShipsTotalLoss(String s) {
        int i = getNumberBeforeWords(s, " ships a total loss");
        int j = getNumberBeforeWords(s, " ship a total loss");
        return i > 0 ? i : j;
    }

    public int getShipsTotalLossTonnage(String s) {
        int i = getTonnage(s, " ships a total loss");
        int j = getTonnage(s, " ship a total loss");
        return i > 0 ? i : j;
    }

    public int getWarshipsSunk(String s) {
        int i = getNumberBeforeWords(s, " warships sunk");
        int j = getNumberBeforeWords(s, " warship sunk");
        return i > 0 ? i : j;
    }

    public int getWarshipsSunkTonnage(String s) {
        int i = getTonnage(s, " warships sunk");
        int j = getTonnage(s, " warship sunk");
        return i > 0 ? i : j;
    }

    public int getWarshipsDamaged(String s) {
        int i = getNumberBeforeWords(s, " warships damaged");
        int j = getNumberBeforeWords(s, " warship damaged");
        return i > 0 ? i : j;
    }

    public int getWarshipsDamagedTonnage(String s) {
        int i = getTonnage(s, " warships damaged");
        int j = getTonnage(s, " warship damaged");
        return i > 0 ? i : j;
    }

    public int getWarshipsTotalLoss(String s) {
        int i = getNumberBeforeWords(s, " warships a total loss");
        int j = getNumberBeforeWords(s, " warship a total loss");
        return i > 0 ? i : j;
    }

    public int getWarshipsTotalLossTonnage(String s) {
        int i = getTonnage(s, " warships a total loss");
        int j = getTonnage(s, " warship a total loss");
        return i > 0 ? i : j;
    }

    private int getNumberBeforeWords(String stringToParse, String words) {
        if (stringToParse != null) {
            int i = stringToParse.indexOf(words);
            if (i >= 0) {
                stringToParse = stringToParse.substring(0, i);
                i = stringToParse.lastIndexOf("\n");
                stringToParse = stringToParse.substring(i+1).trim();
                try {
                    return Integer.parseInt(stringToParse);
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
        }
        return 0;
    }

    private int getTonnage(String stringToParse, String words) {
        if (stringToParse != null) {
            int i = stringToParse.indexOf(words);
            if (i >= 0) {
                stringToParse = stringToParse.substring(i);
                i = stringToParse.indexOf("total tonnage");
                stringToParse = stringToParse.substring(i + "total tonnage".length() + 1);
                i = stringToParse.indexOf("\n");
                if (i == -1) i = stringToParse.length();
                stringToParse = stringToParse.substring(0, i).replace(words,"").replace("tons", "").replace("GRT", "").replace("total tonnage", "").replace(",","").replace(" ", "").trim();
                try {
                    return Integer.parseInt(stringToParse);
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
        }
        return 0;
    }
}
