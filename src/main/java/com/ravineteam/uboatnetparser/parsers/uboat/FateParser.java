package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.uboat.Fate;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagFeeder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class FateParser {

    private static final String TD = "td";


    public String getLabel(String value) {
        if (value != null && !value.isEmpty()) {
            String s = value.split(" ")[0];
            if (s != null && s.length() > 0) {
                return s;
            }
        }
        return "";
    }

    public String getWhen(String value) {
        return getWordsAfter(value, " on ", 3);
    }

    /*public String getWhere(String value) {
        return getWordsAfter(value, " at ", 3);
    }*/

    public String getWordsAfter(String value, String after, int howManyWords) {
        if (value != null && !value.isEmpty()) {
            int i = value.indexOf(after);
            if (i > 0) {
                value = value.substring(i + after.length());
                String[] words = value.split(" ");
                StringBuilder sb = new StringBuilder();
                if (words.length >= howManyWords) {
                    for (int j = 0; j < howManyWords; j++) {
                        sb.append(words[j]).append(" ");
                    }
                }
                return sb.toString().trim();
            }
        }
        return "";
    }

    public List<String> getDescriptions(Document doc) {
        List<String> descriptions = new LinkedList<>();
        TagFeeder feeder = new TagFeeder();
        Node node = feeder.getNode(doc, TD, "Fate", 1);
        if (node != null) {
            NodeUtil util = new NodeUtil();
            Collection<Node> pNodes = util.getChildren(node.getNextSibling().getNextSibling(), "p");
            for (Node p : pNodes) {
                descriptions.add(p.getTextContent().replace("\n", " "));
            }
        }
        return descriptions;
    }


    public List<Fate> getFates(Document doc) {
        List<Fate> fates = new LinkedList<>();
        List<String> descriptions = getDescriptions(doc);
        if (descriptions != null && descriptions.size() > 0) {
            for (String d : descriptions) {
                if (d != null && d.length() > 0) {
                    String label = getLabel(d);
                    String when = getWhen(d);
                    Fate f = new Fate();
                    f.setLabel(label);
                    f.setWhen(when);
                    f.setDescription(d);
                    fates.add(f);
                }
            }
        }
        return fates;
    }
}
