package com.ravineteam.uboatnetparser.parsers.uboat;

import com.ravineteam.uboatnetparser.data.Position;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class FinalPositionParser {

    private final String positionMarker = "position: new google.maps.LatLng";

    public Position getFinalPosition(Document doc) {
        if (doc != null) {
            NodeList scripts = doc.getElementsByTagName("script");
            List<String> contents = new LinkedList<>();
            for (int i = 0; i < scripts.getLength(); i++) {
                Node s = scripts.item(i);
                if (s != null) {
                    String c = s.getTextContent();
                    if (c != null && c.contains(positionMarker)) {
                        int j = c.indexOf(positionMarker);
                        if (j >= 0) {
                            c = c.substring(j + positionMarker.length() );
                            j = c.indexOf("(");
                            c = c.substring(j + 1);
                            j = c.indexOf(")");
                            c = c.substring(0,j);
                            String[] parts = c.split(",");
                            if (parts.length == 2) {
                                try {
                                    double latitude = Double.parseDouble(parts[0]);
                                    double longitude = Double.parseDouble(parts[1]);
                                    return new Position(latitude, longitude);
                                } catch (NumberFormatException e) {
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
