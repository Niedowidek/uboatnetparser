package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Fate;
import com.ravineteam.uboatnetparser.data.patrol.Patrol;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import org.w3c.dom.Document;

import static com.ravineteam.uboatnetparser.utils.TagNames.TD;

public class PatrolParser {

    NodeUtil nodeUtil;
    DateConverter dateConverter;

    public PatrolParser() {
        this.nodeUtil = new NodeUtil();
        dateConverter = new DateConverter();
    }

    private String getValue (Document doc, String tagName, String tagValue, int order, int valueOrder) {
        return nodeUtil.getValue(doc, tagName, tagValue, order, valueOrder);
    }

    public Patrol parse(Document doc) {
        Patrol patrol = new Patrol();
        setStartDate(doc, patrol);
        setStartPlace(doc, patrol);
        setEndDate(doc, patrol);
        setEndPlaceFate(doc, patrol);
        setDuration(doc, patrol);
        return patrol;
    }

    private void setDuration(Document doc, Patrol patrol) {
        String duration = getValue(doc, TD, "Departure", 1, 9);
        patrol.setDuration(duration);
    }

    private void setEndPlaceFate(Document doc, Patrol patrol) {
        String fateArrivalPlace = getValue(doc, TD, "Departure", 1, 8);
        Fate fate = Fate.getByName(fateArrivalPlace);
        if (fate != Fate.EMPTY) {
            patrol.setFate(fate.getFate());
        } else {
            patrol.setEndPlace(fateArrivalPlace);
        }
    }

    private void setEndDate(Document doc, Patrol patrol) {
        String arrivalDateString = getValue(doc, TD, "Departure", 1, 7);
        String arrivalDate = dateConverter.to_yyyyMMdd(arrivalDateString);
        patrol.setEndDate(arrivalDate);
    }

    private void setStartPlace(Document doc, Patrol patrol) {
        String departurePlace = getValue(doc, TD, "Departure", 1, 6);
        patrol.setStartPlace(departurePlace);
    }

    private void setStartDate(Document doc, Patrol patrol) {
        String departureDateString = getValue(doc, TD, "Departure", 1, 5);
        String departureDate = dateConverter.to_yyyyMMdd(departureDateString);
        patrol.setStartDate(departureDate);
    }

}
