package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.patrol.Event;
import com.ravineteam.uboatnetparser.data.patrol.Href;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class EventParser {

    protected NodeUtil nodeUtil = new NodeUtil();

    private List<Event> eventList;

    public EventParser() {
        eventList = new LinkedList<>();
    }

    protected String[] getTitleParts() {
        return new String[] {"General Events during this patrol"};
    }

    protected String[] getMarkerLegendText() {
        return new String[] {""};
    }

    protected String getEndingText() {
        return "Add more events!";
    }

    public List<Event> parseEvents(Document doc) {
        parse(doc);
        return eventList;
    }

    protected Event getNextEventToFill() {
        Event attack = new Event();
        eventList.add(attack);
        return attack;
    }

    public void parse(Document doc) {
        Node node = getNode(doc);
        Node div = getDivNode(node);
        if (div != null) {
            getEvents(doc, div);
        }
    }

    protected Node getNode(Document doc) {
        return nodeUtil.getNode(doc, TagNames.H3, 1,  getTitleParts());
    }

    private Node getDivNode(Node node) {
        Node div = node.getNextSibling().getNextSibling();
        String divClass = nodeUtil.getAttributeValue(div, "class");
        if ("content_box".equals(divClass)) {
            return div;
        }
        return  null;
    }

    private void getEvents(Document doc, Node div) {
        List<Node> nodeList = nodeUtil.getChildrenNodesByNodeName(div, "p");
        String dateSaved = "";
        for (Node p : nodeList) {
            String dateFromDivString = getDateString(p);
            if (StringUtils.isEmpty(dateFromDivString) && StringUtils.isNotEmpty(dateSaved)) {
                dateFromDivString = dateSaved;
            }
            String textCheck = getTextFromNode(p, dateFromDivString, false);
            if (StringUtils.equals(dateFromDivString, textCheck) && StringUtils.isNotEmpty(dateFromDivString)) {
                dateSaved = dateFromDivString;
                continue;
            }
            String date = new DateConverter().to_yyyyMMdd(dateFromDivString);
            String text = getTextFromNode(p, dateFromDivString, StringUtils.isEmpty(dateSaved));
            dateSaved = "";
            List<Node> citesFound = getCiteNodes(p);
            List<Node> hrefsFound = getHrefNodes(p);

            if (StringUtils.isNoneEmpty(date, text)) {
                setEventData(doc, dateFromDivString, text, citesFound, hrefsFound);
            }
        }
    }

    private void setEventData(Document doc, String dateFromDivString, String text, List<Node> citesFound, List<Node> hrefsFound) {
        Event event = getNextEventToFill();
        event.setDateFormatted(new DateConverter().to_yyyyMMdd(dateFromDivString));
        event.setText(text);
        setCites(citesFound, event);
        setHrefs(hrefsFound, event);
        setDataFromMarker(doc, event);
    }

    private void setCites(List<Node> citesFound, Event event) {
        if (citesFound != null && !citesFound.isEmpty()) {
            for (Node cite : citesFound) {
                String citeText = cite.getFirstChild().getTextContent().replace("\n", " ").trim();
                if (StringUtils.isNoneEmpty(citeText)) {
                    event.getCites().add(citeText);
                }
            }
        }
    }

    private void setHrefs(List<Node> hrefsFound, Event attack) {
        if (hrefsFound != null && !hrefsFound.isEmpty()) {
            for (Node hrefNode : hrefsFound) {
                String hrefText = hrefNode.getFirstChild().getTextContent().replace("\n", " ").trim();
//                Node parent = hrefNode.getParentNode();
                String hrefLink = nodeUtil.getAttributeValue(hrefNode, "href");
                if (StringUtils.isNoneEmpty(hrefText, hrefLink)) {
                    Href href =  new Href();
                    href.setHref(hrefLink);
                    href.setText(hrefText);
                    attack.getHrefs().add(href);
                }
            }
        }
    }

    private String getDateString(Node div) {
        List<Node> nodeList = nodeUtil.getChildrenNodesByNodeName(div, "strong");
        if (nodeList != null && nodeList.size() > 0) {
            Node dateFromDiv = nodeList.get(0).getFirstChild()/*.getNextSibling().getFirstChild().getFirstChild()*/;
            if (dateFromDiv != null) {
                return dateFromDiv.getTextContent();

            }
        }
        return "";
    }

    private String getTextFromNode(Node node, String dateFromDivString) {
        return getTextFromNode(node, dateFromDivString, true);
    }

    private String getTextFromNode(Node node, String dateFromDivString, boolean eraseDate) {
        String textFromDiv = node.getTextContent();
        int cutIndex = textFromDiv.indexOf(getEndingText());
        textFromDiv = textFromDiv.substring(0, cutIndex > 0 ? cutIndex : textFromDiv.length());
        if (eraseDate) {
            textFromDiv = textFromDiv.replaceFirst(dateFromDivString, "");
        }
        if (textFromDiv.startsWith(". ")) {
            textFromDiv = textFromDiv.replaceFirst(". ", "");
        }
        textFromDiv = textFromDiv.replace("\n", " ").trim();
        return textFromDiv;
    }

    private List<Node> getCiteNodes (Node node) {
        return nodeUtil.getChildrenNodesByNodeName(node, "cite");
    }

    private List<Node> getHrefNodes(Node node) {
        List<Node> hrefsFound = nodeUtil.getChildrenNodesByNodeName(node, "a");
        List<Node> toRemove = new LinkedList<>();
        removeUnnecessaryHrefNodes(hrefsFound, toRemove);
        hrefsFound.removeAll(toRemove);
        return  hrefsFound;
    }

    protected void removeUnnecessaryHrefNodes(List<Node> hrefsFound, List<Node> toRemove) {
        for (Node a : hrefsFound) {
            if ("please let us know".equals(a.getTextContent().replace("\n", " ").trim())) {
                toRemove.add(a);
            }
        }
    }

    public boolean setDataFromMarker(Document doc, Event attack) {
        MarkerParser markerParser = new MarkerParser();
        String type = markerParser.determineProperMarker(doc, getMarkerLegendText());
        String date = attack.getDate();
        String textContent = attack.getText();
        Marker marker = markerParser.getMarker(doc, type, date, textContent);
        if (marker != null && marker.isNotEmpty()) {
            attack.setPosition(marker.getPosition());
            attack.setMarkerText(marker.getTextContent());
            attack.setMarkerTextWithHtmlTags(marker.getTextContentWithHtmlTags());
            return true;
        }
        return false;
    }

}
