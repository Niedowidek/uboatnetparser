package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.Details;
import com.ravineteam.uboatnetparser.utils.HrefUtil;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DetailsParser {

    public static final String LEGEND_DESCRIPTION = "Daily position of the U-boat";
    public static final String POSITION_ON_THIS_DAY = "Position on this day:";
    private final String NAME_START = "Patrol info for";
    private final String DATE_START = "Date lookup for";

    public Details parse(Document doc) {
        Details details = new Details();
        NodeUtil nodeUtil = new NodeUtil();

        String uboatName = setUboatName(doc, details, nodeUtil);

        Node dateNode = nodeUtil.getNode(doc, TagNames.H3, DATE_START);
        String date = setDate(details, dateNode);

        setPatrolDates(details, nodeUtil, dateNode);

        Marker marker = setPositionFromMarker(doc, details, uboatName, date);

        setPositionFromDescription(details, marker);

        setPatrolHref(doc, details, nodeUtil);

        return details;
    }

    private String setDate(Details details, Node dateNode) {
        String date = dateNode != null ? dateNode.getTextContent() : "";
        if (StringUtils.isNotEmpty(date)) {
            date = date.replace(DATE_START, "").trim();
            details.setDate(date);
        }
        return date;
    }

    private String setUboatName(Document doc, Details details, NodeUtil nodeUtil) {
        Node uboatNameNode = nodeUtil.getNode(doc, TagNames.H1, NAME_START);
        String uboatName = uboatNameNode != null ? uboatNameNode.getTextContent() : "";
        if (StringUtils.isNotEmpty(uboatName)) {
            uboatName = uboatName.replace(NAME_START, "").trim();
            details.setUboatName(uboatName);
        }
        return uboatName;
    }

    private void setPatrolDates(Details details, NodeUtil nodeUtil, Node dateNode) {
        Node table = nodeUtil.getSiblingTable(dateNode);
        if (table != null) {
            Node startDateNode = nodeUtil.getTd(table, 1, 0);
            String startDate = startDateNode != null ? startDateNode.getTextContent() : "";
            details.setPatrolStartDate(startDate);

            Node endDateNode = nodeUtil.getTd(table, 1, 2);
            String endDate = endDateNode != null ? endDateNode.getTextContent() : "";
            details.setPatrolEndDate(endDate);
        }
    }

    private Marker setPositionFromMarker(Document doc, Details details, String uboatName, String date) {
        MarkerParser markerParser = new MarkerParser();
        String markerType = markerParser.determineProperMarker(doc, new String[] {LEGEND_DESCRIPTION});
        Marker marker = markerParser.getMarker(doc, markerType, date, uboatName);
        if (marker.getPosition().isNotEmpty()) {
            details.setPositionFromMarker(marker.getPosition());
        }
        return marker;
    }

    private void setPositionFromDescription(Details details, Marker marker) {
        String description = marker.getTextContentWithHtmlTags();
        if (StringUtils.containsIgnoreCase(description, POSITION_ON_THIS_DAY)) {
            int positionsStartIndex = description.indexOf(POSITION_ON_THIS_DAY);
            int positionsEndIndex = description.indexOf("<");
            if (positionsEndIndex <= 0 ) positionsEndIndex = description.length();
            String positionsFragment = description.substring(positionsStartIndex + POSITION_ON_THIS_DAY.length(), positionsEndIndex);
            if (StringUtils.isNotEmpty(positionsFragment)) {
                Position position = Position.getInstance(",", positionsFragment);
                if (position.isNotEmpty()) {
                    details.setPositionFromDescription(position);
                }
            }
        }
    }

    private void setPatrolHref(Document doc, Details details, NodeUtil nodeUtil) {
        Node patrolHrefNode = nodeUtil.getNode(doc, TagNames.P, "Return to see the full info on this patrol");
        HrefUtil hrefUtil = new HrefUtil();
        String hrefPatrol = hrefUtil.getHref(patrolHrefNode);
        if (StringUtils.isNotEmpty(hrefPatrol)) details.setPatrolHref(hrefPatrol);
    }

}
