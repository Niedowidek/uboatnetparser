package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Tonnage;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class TonnageParser {

    Tonnage parse(Document doc) {
        Tonnage tonnage = new Tonnage();

        Node p = null;
        NodeList plist = doc.getElementsByTagName("p");
        for (int i = 0; i < plist.getLength(); i++) {
            Node node = plist.item(i);
            if (node != null) {
                String textContent = node.getTextContent();
                if (StringUtils.contains(textContent, "tons")
                        && StringUtils.containsAny(textContent, "sunk", "damaged")) {
                    p = node;
                    break;
                }
            }
        }
        if (p != null) {
            String text = p.getTextContent();
            if (StringUtils.isNotEmpty(text)) {
                tonnage.setShipsSunk(getNumber(text, "ships sunk"));
                tonnage.setShipsSunkTonnage(getTonnage(text,"ships sunk"));
                tonnage.setShipsDamaged(getNumber(text, "ships damaged"));
                tonnage.setShipsDamagedTonnage(getTonnage(text, "ships damaged"));
                tonnage.setWarshipsSunk(getNumber(text, "warships sunk"));
                tonnage.setWarshipsSunkTonnage(getTonnage(text, "warships sunk"));
                tonnage.setWarshipsDamaged(getNumber(text, "warships damaged"));
                tonnage.setWarshipsDamagedTonnage(getTonnage(text, "warships damaged"));
            }
        }

        return tonnage;
    }

    private int getNumber(String text, String searched) {
        if (StringUtils.isNoneEmpty(text, searched)) {
            int start = text.indexOf(searched);
            if (start == -1) {
                searched = searched.replace("ships", "ship");
                start = text.indexOf(searched);
            }
            if (start >= 0) {
                String newText = text.substring(0, start).trim();
                int lastSpace = newText.lastIndexOf(" ");
                if (lastSpace == -1) lastSpace = 0;
                newText = newText.substring(lastSpace).trim();
                try {
                    return Integer.parseInt(newText);
                } catch (NumberFormatException e) {
                    System.err.println("Cannot parse (war)ships");
                }
            }
        }
        return 0;
    }

    private int getTonnage(String text, String searched) {
        if (StringUtils.isNoneEmpty(text, searched)) {
            int start = text.indexOf(searched);
            if (start == -1) {
                searched = searched.replace("ships", "ship");
                start = text.indexOf(searched);
            }
            if (start >= 0) {
                start = start + searched.length();
                String newText = text.substring(start);
                int bracesStart = newText.indexOf("(");
                int bracesEnd = newText.indexOf(")");
                if (bracesStart >= 0 && bracesEnd > 0) {
                    newText = newText.substring(bracesStart + 1, bracesEnd).trim();
                    newText = newText.replace("tons", "").replace(",", "").trim();
                    try {
                        return Integer.parseInt(newText);
                    } catch (NumberFormatException e) {
                        System.err.println("Cannot parse tonnage");
                    }
                }
            }
        }
        return 0;
    }
}
