package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Attack;
import com.ravineteam.uboatnetparser.data.patrol.Event;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class AttacksParser extends EventParser {

    List<Attack> attackList;

    public AttacksParser() {
        attackList = new LinkedList<>();
    }

    @Override
    protected String[] getTitleParts() {
        return new String[] { "Attacks on", "during this patrol" };
    }

    @Override
    protected String[] getMarkerLegendText() {
        return new String[] {"indicates", "attack on the boat"};
    }

    @Override
    protected String getEndingText() {
        return "About this data";
    }

    public List<Attack> parseAttacks(Document doc) {
        parse(doc);
        return attackList;
    }

    @Override
    protected Event getNextEventToFill() {
        Attack attack = new Attack();
        attackList.add(attack);
        return attack;
    }

    /*public void parseEvents(Document doc) {
//        List<Event> eventList = new LinkedList<>();
        Node attacksNode = getEventsNode(doc);
        Node div = getDivNode(attacksNode);
        if (div != null) {
            getEvents(div);
        }
//        return eventList;
    }

    private void getEvents( Node div) {
        String dateFromDivString = getDateString(div);
        String date = new DateConverter().to_yyyyMMdd(dateFromDivString);
        String textFromDiv = getTextFromDiv(div, dateFromDivString);
        List<Node> citesFound = getCiteNodes(div);
        List<Node> hrefsFound = getHrefNodes(div);

        if (StringUtils.isNoneEmpty(date, textFromDiv)) {
            Event event = getNextEventToFill(); //new Event();
            event.setDateFormatted(new DateConverter().to_yyyyMMdd(dateFromDivString));
            event.setText(textFromDiv);
            setCites(citesFound, event);
            setHrefs(hrefsFound, event);
//            eventList.add(event);
        }
    }

    private void setCites(List<Node> citesFound, Event event) {
        if (citesFound != null && !citesFound.isEmpty()) {
            for (Node cite : citesFound) {
                String citeText = cite.getTextContent();
                if (StringUtils.isNoneEmpty(citeText)) {
                    event.getCites().add(citeText);
                }
            }
        }
    }

    private void setHrefs(List<Node> hrefsFound, Event attack) {
        if (hrefsFound != null && !hrefsFound.isEmpty()) {
            for (Node hrefNode : hrefsFound) {
                String hrefText = hrefNode.getTextContent().replace("\n", " ").trim();
                Node parent = hrefNode.getParentNode();
                String hrefLink = nodeUtil.getAttributeValue(parent, "href");
                if (StringUtils.isNoneEmpty(hrefText, hrefLink)) {
                    Href href =  new Href();
                    href.setHref(hrefLink);
                    href.setText(hrefText);
                    attack.getHrefs().add(href);
                }
            }
        }
    }

    private Node getEventsNode(Document doc) {
        //"Attacks on", "during this patrol"
        return nodeUtil.getNode(doc, TagNames.H3, 1,  titleParts);
    }

    private String getDateString(Node div) {
        Node dateFromDiv = div.getFirstChild().getNextSibling().getFirstChild().getFirstChild();
        return dateFromDiv.getTextContent();
    }

    private Node getDivNode(Node attacksNode) {
        Node div = attacksNode.getNextSibling().getNextSibling();
        String divClass = nodeUtil.getAttributeValue(div, "class");
        if ("content_box".equals(divClass)) {
            return div;
        }
        return  null;
    }

    private String getTextFromDiv (Node div, String dateFromDivString) {
        //"About this data"
        String textFromDiv = div.getTextContent();
        textFromDiv = textFromDiv.substring(0, textFromDiv.indexOf(endingText));
        textFromDiv = textFromDiv.replaceFirst(dateFromDivString, "");
        textFromDiv = textFromDiv.replace("\n", " ").trim();
        return textFromDiv;
    }

    private List<Node> getCiteNodes (Node div) {
        return nodeUtil.getChildrenNodesByOwnerNodeName(div, "cite");
    }

    private List<Node> getHrefNodes(Node div) {
        List<Node> hrefsFound = nodeUtil.getChildrenNodesByOwnerNodeName(div, "a");
        List<Node> toRemove = new LinkedList<>();
        removeUnnecessaryHrefNodes(hrefsFound, toRemove);
        hrefsFound.removeAll(toRemove);
        return  hrefsFound;
    }*/

    protected void removeUnnecessaryHrefNodes(List<Node> hrefsFound, List<Node> toRemove) {
        for (Node a : hrefsFound) {
            if ("please let us know".equals(a.getTextContent().replace("\n", " ").trim())) {
                toRemove.add(a);
            }
        }
    }

}
