package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.DailyPosition;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class DailyPositionListParser {

    private String[] titleParts = new String[] {"We have", "daily positions"};

    public List<DailyPosition> parse(Document doc) {
        List<DailyPosition> dailyPositions = new LinkedList<>();
        NodeUtil nodeUtil = new NodeUtil();

        Node node  = nodeUtil.getNode(doc, TagNames.P, 1, this.titleParts );
        Node sibling = getDivNode(node);
        List<Node> aNodes = getANodes(nodeUtil, sibling);
        processANodes(dailyPositions, nodeUtil, aNodes);

        return dailyPositions;
    }

    private Node getDivNode(Node node) {
        Node sibling = node.getParentNode().getNextSibling();
        while(sibling != null && !StringUtils.equals("div", sibling.getNodeName())) {
            sibling = sibling.getNextSibling();
        }
        return sibling;
    }

    private List<Node> getANodes(NodeUtil nodeUtil, Node sibling) {
        List<Node> aNodes = null;
        if (sibling != null && StringUtils.equals("div", sibling.getNodeName()) && StringUtils.equals("content_box", nodeUtil.getAttributeValue(sibling, "class"))) {
            List<Node> pNodes = nodeUtil.getChildrenNodesByNodeName(sibling, "p");
            if (pNodes != null && pNodes.size() > 0) {
                for (Node p : pNodes) {
                    aNodes = nodeUtil.getChildrenNodesByNodeName(p, "a");
                    if (aNodes != null && aNodes.size() > 0 && StringUtils.startsWith(nodeUtil.getAttributeValue(aNodes.get(0), "href"), "details.php")) {
                        break;
                    }
                }
            }
        }
        return aNodes;
    }

    private void processANodes(List<DailyPosition> dailyPositions, NodeUtil nodeUtil, List<Node> aNodes) {
        if (aNodes != null && aNodes.size() > 0) {
            DateConverter dateConverter = new DateConverter();
            for (Node a : aNodes) {
                String href = nodeUtil.getAttributeValue(a, "href");
                String label = a.getTextContent().replace("\n", " ").trim();
                String date = dateConverter.to_yyyyMMdd(label);
                if (StringUtils.isNotEmpty(href)) {
                    addDailyPosition(dailyPositions, href, label, date);
                }

            }
        }
    }

    private void addDailyPosition(List<DailyPosition> dailyPositions, String href, String label, String date) {
        DailyPosition dailyPosition = new DailyPosition();
        dailyPosition.setHref(href);
        dailyPosition.setLabel(label);
        dailyPosition.setDate(date);
        dailyPositions.add(dailyPosition);
    }


}
