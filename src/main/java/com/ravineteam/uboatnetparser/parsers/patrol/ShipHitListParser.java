package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.patrol.ShipHit;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.LinkedList;
import java.util.List;

public class ShipHitListParser {

    protected NodeUtil nodeUtil = new NodeUtil();
    protected MarkerParser markerParser = new MarkerParser();

    private String captureIndicator = "(c.)";
    private String damageIndicator = "(d.)";
    private String totalLossIndicator = "(t.)";
    private String mineIndicator = "[Mine]";


    protected int numberOfColumns = 10;

    public static final String HREF = "href";
    protected final int DATE = 0;
    protected final int UBOAT_NUMBER = 1;
    protected final int UBOAT_COMMANDER = 2;
    protected final int PICTURE = 3;
    protected final int SHIP_NAME = 4;
    protected final int TONNAGE = 5;
    protected final int FLAG = 6;
    protected final int NATIONALITY = 7;
    protected final int CONVOY = 8;
    protected final int MARKER = 9;

    protected String[] getTitleParts() {
        return new String[] {"Ships hit","during this patrol"};
    }

    public List<ShipHit> parse (Document doc) {
        List<ShipHit> shipHitList = new LinkedList<>();
        String type = markerParser.determineProperMarker(doc, new String[] {"means a ship hit"});
        Node node = nodeUtil.getNode(doc, TagNames.H3, 1, this.getTitleParts());
        Node divNode = getDivNode(node);
        List<Node> rowNodes = getRowNodes(divNode);
        if (!rowNodes.isEmpty()) {
            for (Node row : rowNodes) {
                List<Node> tdNodes = nodeUtil.getChildrenNodesByNodeName(row, TagNames.TD);
                if (tdNodes != null && !tdNodes.isEmpty() && tdNodes.size() == numberOfColumns) {
                    addShip(doc, shipHitList, tdNodes, type);
                } else if (tdNodes != null && tdNodes.size() != numberOfColumns) {
                    System.err.println("Incorrect number of columns in a table with ships hit");
                }
            }
        }
        return shipHitList;
    }



    protected Node getDivNode(Node node) {
        Node sibling = node.getNextSibling();
        do {
            sibling = sibling.getNextSibling();
        } while (sibling != null && !sibling.getNodeName().equals("div"));
        return sibling;
    }

    protected List<Node> getRowNodes(Node sibling) {
        List<Node> rowNodes = new LinkedList<>();
        if (sibling != null) {
            rowNodes = nodeUtil.getChildrenNodesByNodeName(sibling, TagNames.TR );
        }
        return rowNodes;
    }

    protected void addShip(Document doc, List<ShipHit> shipHitList, List<Node> tdNodes, String type) {
        ShipHit shipHit = new ShipHit();
        for (int i = 0; i < numberOfColumns; i++) {
            Node td = tdNodes.get(i);
            assignData(shipHit, i, td);
        }
        String searchedText = type + shipHit.getMapSymbol();
        Marker marker = markerParser.getMarker(doc, searchedText);
        shipHit.setPosition(marker.getPosition());
        shipHit.setTextFromMarker(marker.getTextContent());
        shipHit.setTextFromMarkerWithHtml(marker.getTextContentWithHtmlTags());
        shipHitList.add(shipHit);
    }

    protected void assignData(ShipHit shipHit, int i, Node td) {
        String value;
        Node childNode;
        switch(i) {
            case DATE:
                value = getTextContent(td);
                shipHit.setDateLabel(value);
                break;
            case UBOAT_NUMBER:
                value = getTextContent(td);
                shipHit.setUboat(value);
                childNode = getChildNode(td, "a");
                shipHit.setUboatHref(nodeUtil.getAttributeValue(childNode, HREF));
                break;
            case UBOAT_COMMANDER:
                value = getTextContent(td);
                shipHit.setCommander(value);
                childNode = getChildNode(td, "a");
                shipHit.setCommanderHref(nodeUtil.getAttributeValue(childNode, HREF));
                break;
            case PICTURE:
                childNode = getChildNode(td, "img");
                value = nodeUtil.getAttributeValue(childNode, "src");
                shipHit.setHasPicture(StringUtils.isNotEmpty(value));
                break;
            case SHIP_NAME:
                value = getTextContent(td);
                if (StringUtils.isNotEmpty(value)) {
                    if (value.contains(captureIndicator)) {
                        shipHit.setCaptured(true);
                        value = value.replace(captureIndicator, "");
//                        value = value.substring(0, value.length() - captureIndicator.length());
                    }
                    if (value.contains(damageIndicator)) {
                        shipHit.setDamaged(true);
                        value = value.replace(damageIndicator, "");
//                        value = value.substring(0, value.length() - damageIndicator.length());
                    }
                    if (value.contains(totalLossIndicator)) {
                        shipHit.setTotalLoss(true);
                        value = value.replace(totalLossIndicator, "");
                    }
                    if (value.contains(mineIndicator)) {
                        shipHit.setHitByMine(true);
                        value = value.replace(mineIndicator, "");
                    }
                }
                shipHit.setShipName(value.trim());
                childNode = getChildNode(td, "a");
                shipHit.setShipHref(nodeUtil.getAttributeValue(childNode, HREF));
                break;
            case TONNAGE:
                String tonnageString = td.getTextContent();
                tonnageString = tonnageString.replace(",", "");
                try {
                    int tonnage = Integer.parseInt(tonnageString);
                    shipHit.setTonnage(tonnage);
                } catch (NumberFormatException e) {
                    // log error
                }
                break;
            case FLAG:
                childNode = getChildNode(td, "img");
                value = nodeUtil.getAttributeValue(childNode, "src");
                shipHit.setNationalityHref(value);
                break;
            case NATIONALITY:
                value = getTextContent(td);
                shipHit.setNationality(value);
                break;
            case CONVOY:
                childNode = getChildNode(td, "a");
                shipHit.setConvoyHref(nodeUtil.getAttributeValue(childNode, HREF));
                value = getTextContent(td);
                shipHit.setConvoy(value);
                break;
            case MARKER:
                shipHit.setMapSymbol(td.getTextContent());
                break;
        }
    }

    private String getTextContent(Node td) {
        return td.getTextContent().replace("\n", " ").trim();
    }

    private Node getChildNode(Node parentNode, String childName) {
        List<Node> nodes = nodeUtil.getChildrenNodesByNodeName(parentNode, childName);
        if (nodes != null && nodes.size() == 1) {
            return nodes.get(0);
        } else {
            return null;
        }
    }

}
