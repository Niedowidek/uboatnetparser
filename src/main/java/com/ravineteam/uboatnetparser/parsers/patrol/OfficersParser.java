package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.patrol.Officer;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class OfficersParser {

    public List<Officer> parse (Document doc) {
        List<Officer> officerList = new LinkedList<>();
        if (doc != null) {
            Node h3Node = null;
            NodeList list = doc.getElementsByTagName(TagNames.H3);
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node != null) {
                    String content = node.getTextContent();
                    if (StringUtils.containsAny(content, "Commander", "Officers")) {
                        h3Node = node;
                        break;
                    }
                }
            }
        if (h3Node != null) {
           Node parent = h3Node.getParentNode();
           while (parent != null && !"table".equals(parent.getNodeName())) {
               parent = parent.getParentNode();
           }
           if (parent != null) {
               NodeUtil nodeUtil = new NodeUtil();
               List<Node> trNodes = nodeUtil.getChildrenNodesByNodeName(parent, TagNames.TR);
               if (trNodes != null && trNodes.size() == 2) {
                   List<Node> tdNodes = nodeUtil.getChildrenNodesByNodeName(trNodes.get(1), TagNames.TD);
                   if (tdNodes != null && tdNodes.size() > 0) {
                       for (Node td : tdNodes) {
                           Officer officer = new Officer();
                           List<Node> aNodes = nodeUtil.getChildrenNodesByNodeName(td, TagNames.A);
                           List<Node> strongNodes = nodeUtil.getChildrenNodesByNodeName(td, TagNames.STRONG);
                           if (aNodes != null && aNodes.size() == 1 && aNodes.get(0).getTextContent() != null) {
                                officer.setName(aNodes.get(0).getTextContent().replace("\n", " ").trim());
                                officer.setHref(nodeUtil.getAttributeValue(aNodes.get(0), "href"));
                                if (strongNodes != null && strongNodes.size() == 1 && strongNodes.get(0).getTextContent() != null) {
                                    officer.setAssignment(strongNodes.get(0).getTextContent().replace("\n", " ").trim());
                                }
                                String rank = td.getTextContent();
                                if (StringUtils.isNotEmpty(rank)) {
                                    rank = rank.replace("\n", " ").replace(officer.getName(), "").replace(officer.getAssignment() != null ? officer.getAssignment() : "", "").trim();
                                }
                                officer.setRank(rank);
                                officerList.add(officer);
                           }
                       }
                   }
               }
           }
        }

        }


        return officerList;
    }

}
