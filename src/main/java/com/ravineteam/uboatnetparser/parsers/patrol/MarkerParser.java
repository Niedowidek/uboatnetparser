package com.ravineteam.uboatnetparser.parsers.patrol;

import com.ravineteam.uboatnetparser.data.Marker;
import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.patrol.Event;
import com.ravineteam.uboatnetparser.utils.NodeUtil;
import com.ravineteam.uboatnetparser.utils.TagNames;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class MarkerParser {

    public static final String MARKERS_SEARCH_STRING = "markers[";
    public static final String GOOGLE_MAPS_LAT_LNG = "google.maps.LatLng(";
    private final String POSITION_MARKER = "position: new google.maps.LatLng";
    private NodeUtil nodeUtil;

    public MarkerParser() {
        nodeUtil = new NodeUtil();
    }

    public Marker getMarker(Document doc, String type, String date, String textContent) {
        Marker marker = new Marker();
        String scriptsContent = getScriptsContent(doc);
        List<Marker> markers = getMarkersByDate(scriptsContent, date);
        markers = filterMarkersByType(markers, type);
        if (markers != null && markers.size() > 1) {
            markers = filterMarkersByTextContent(markers, textContent);
        }
        if (markers != null && markers.size() == 1) {
            marker = markers.get(0);
        }
        return marker;
    }

    private List<Marker> getMarkersByDate(String scriptsContent, String date) {
        List<Marker> markers = new LinkedList<>();
        if (StringUtils.isNoneEmpty(scriptsContent, date)) {
            int searchStartIndex = 0;
            int indexFound = -1;
            if (StringUtils.isNoneEmpty(scriptsContent, date)) {
                do {
                    indexFound = getMarkerNumber(scriptsContent.substring(searchStartIndex), date);
                    if (indexFound >= 0 ){
                        searchStartIndex = scriptsContent.indexOf(getMarkerString(indexFound + 1));
                        addMarker(scriptsContent, date, markers, indexFound);
                    }
                } while (indexFound >= 0 && searchStartIndex >= 0);
                for (Marker marker : markers) {
                    setType(scriptsContent, marker);
                    setPosition(scriptsContent, marker);
                    setTextContent(scriptsContent, marker);
                }
            }

        }
        return markers;
    }

    private String getMarkerString(int indexFound) {
        return "markers[" + indexFound + "]";
    }

    private void addMarker(String scriptsContent, String date, List<Marker> markers, int indexFound) {
        if (indexFound >= 0) {
            int startingIndex = scriptsContent.indexOf(getMarkerString(indexFound));
            Marker marker = new Marker();
            marker.setNumber(indexFound);
            marker.setStartingIndex(startingIndex);
            marker.setDate(date);
            markers.add(marker);
        }
    }

    private void setType(String scriptsContent, Marker marker) {
        String markerText = scriptsContent.substring(marker.getStartingIndex());
        int iconIndex = markerText.indexOf("icon:");
        String iconText = markerText.substring(iconIndex);
        if (StringUtils.isNotEmpty(iconText)) {
            int pathStartIndex = iconText.indexOf("/");
            int pathLength = iconText.substring(pathStartIndex + 1).indexOf("'");
            String pathText = iconText.substring(pathStartIndex, pathStartIndex + pathLength + 1);
            if (StringUtils.isNotEmpty(pathText)) {
                int filenameStartIndex = pathText.lastIndexOf("/");
                int filenameEndLength = pathText.substring(filenameStartIndex).lastIndexOf(".");
                String markerName = pathText.substring(filenameStartIndex + 1, filenameStartIndex + filenameEndLength);
                if (StringUtils.isNotEmpty(markerName)) {
                    marker.setType(markerName);
                }
            }
        }
    }

    private void setPosition(String scriptsContent, Marker marker) {
        String markerText = scriptsContent.substring(marker.getStartingIndex());
        int geoTextStart = markerText.indexOf(GOOGLE_MAPS_LAT_LNG);
        if (geoTextStart >= 0) {
            geoTextStart = geoTextStart + GOOGLE_MAPS_LAT_LNG.length();
            int geoTextLength = markerText.substring(geoTextStart).indexOf(")");
            if (geoTextLength > 0) {
                String geoText = markerText.substring(geoTextStart, geoTextStart + geoTextLength);
                String[] latLong = geoText.split(",");
                if (latLong != null && latLong.length == 2) {
                    try {
                        double lat = Double.parseDouble(latLong[0]);
                        double lng = Double.parseDouble(latLong[1]);
                        if (lat >= -90.0 && lat <= 90 && lng >= -180.0 && lng <= 180) {
                            marker.setPosition(new Position(lat, lng));
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void setTextContent(String scriptsContent, Marker marker) {
        String markerText = scriptsContent.substring(marker.getStartingIndex());
        int infoStart = markerText.indexOf(getMarkerString(marker.getNumber()) + ".infoWindow");
        String infoText = markerText.substring(infoStart);
        if (StringUtils.isNotEmpty(infoText)) {
            int textStart = infoText.indexOf("style=");
            if (textStart >= 0) {
                textStart = infoText.substring(textStart).indexOf(">") + 1 + textStart;
                if (textStart >= 0) {
                    String contentText = infoText.substring(textStart);
                    if (StringUtils.isNotEmpty(contentText)) {
                        int contentEnd = contentText.indexOf("<\\/div>");
                        if (contentEnd > 0) {
                            contentText = contentText.substring(0,contentEnd);
                            if (StringUtils.isNotEmpty(contentText)) {
                                String correctedText = contentText.replace("<\\/", "</");
//                                String htmlString = "<div>" + correctedText + "</div>";
//                                String htmlString = correctedText;
                                String cleanText = Jsoup.parse(correctedText).text();
                                marker.setTextContent(cleanText);
                                marker.setTextContentWithHtmlTags(correctedText);
                            }
                        }
                    }
                }
            }
        }
    }

    public String determineProperMarker(Document doc, String[] searchedText) {
        String markerName = "";
        Node legendNode = nodeUtil.getNode(doc, TagNames.P, 1, searchedText);
        List<Node> imgNodes = nodeUtil.getChildrenNodesByNodeName(legendNode, "img");
        Node properImg = null;
        for (Node img : imgNodes) {
            Node textNode = img.getNextSibling();
            short s = textNode.getNodeType();
            boolean searchedTextPartsPresent = true;
            for (String searched : searchedText) {
                if (!StringUtils.containsIgnoreCase(textNode.getTextContent(), searched)) {
                    searchedTextPartsPresent = false;
                    break;
                }
            }
            if (textNode.getNodeType() == Node.TEXT_NODE && searchedTextPartsPresent) {
                properImg = img;
            }
        }
        if (properImg != null) {
            String src = nodeUtil.getAttributeValue(properImg, "src");
            if (src != null) {
                int index = src.lastIndexOf("/");
                if (index >= 0) {
                    markerName = src.substring(index);
                    int j = markerName.lastIndexOf(".");
                    markerName = markerName.substring(1, j);
                    System.out.println();
                }
            }
        }
        return markerName;
    }

    private List<Marker> filterMarkersByType(List<Marker> markers, String type) {
        List<Marker> markerFiltered = new LinkedList<>();
        for (Marker m : markers) {
            if (StringUtils.equalsIgnoreCase(m.getType(), type)) {
                markerFiltered.add(m);
            }
        }
        return markerFiltered;
    }

    private List<Marker> filterMarkersByTextContent(List<Marker> markers, String textContent) {
        List<Marker> matchingMarkers = new LinkedList<>();
        for (Marker m : markers) {
            String searchedString = textContent != null ? textContent.substring(0, 10) : null;
            if (searchedString != null && StringUtils.containsIgnoreCase(m.getTextContent(), searchedString)) {
                matchingMarkers.add(m);
            }
        }
        return matchingMarkers;
    }

    public Position getEventPosition(Document doc, Event event) {
        return getEventPosition(doc, event, true);
    }

    public Position getEventPosition(Document doc, Event event, boolean savePositionToEvent) {
        return Position.getEmptyPosition();
    }

    public String getScriptsContent(Document doc) {
        if (doc != null) {
            NodeList scripts = doc.getElementsByTagName("script");
            List<String> contents = new LinkedList<>();
            for (int i = 0; i < scripts.getLength(); i++) {
                Node s = scripts.item(i);
                if (s != null) {
                    String c = s.getTextContent();
                    if (c != null && c.contains(POSITION_MARKER)) {
                        return c;
                    }
                }
            }
        }
        return "";
    }

    private int getMarkerNumber (String scriptContents, String searchedText) {
        if (StringUtils.isNoneEmpty(scriptContents, searchedText)) {
            int index = scriptContents.indexOf(searchedText);
            if (index >= 0) {
                int markerIndex = scriptContents.substring(0, index).lastIndexOf(MARKERS_SEARCH_STRING);
                int markerLength = scriptContents.substring(markerIndex, index).indexOf("=");
                String markerText = scriptContents.substring(markerIndex, markerIndex+markerLength).trim();
                String number = markerText.substring(markerText.indexOf("[")+1, markerText.indexOf("]"));
                try {
                    return Integer.parseInt(number);
                } catch (NumberFormatException e) {
                    System.err.println("Cannot find number of marker in " + markerText);
                }
            }
        }
        return -1;
    }

    public Marker getMarker(Document doc, String markerName) {
        Marker marker = new Marker();
        if (doc != null && StringUtils.isNotEmpty(markerName)) {
            String scriptContents = getScriptsContent(doc);
            if (StringUtils.isNotEmpty(scriptContents)) {
                int markerNumber = getMarkerNumber(scriptContents, markerName);
                if (markerNumber >= 0) {
                    List<Marker> markerList = new LinkedList<>();
                    addMarker(scriptContents, null, markerList, markerNumber);
                    if (markerList.size() == 1) {
                        marker = markerList.get(0);
                        setPosition(scriptContents, marker);
                        setTextContent(scriptContents, marker);
                    }
                }
            }
        }
        return marker;
    }
}
