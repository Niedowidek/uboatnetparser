package com.ravineteam.uboatnetparser.xml;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class XmlParser {

    private static DocumentBuilderFactory dbf;

    public XmlParser() {
        if (dbf == null)
            dbf = DocumentBuilderFactory.newInstance();
    }

    public Document parse (File file ) throws IOException, SAXException, ParserConfigurationException {
        /*InputSource is= new InputSource(new FileReader(file));
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(is);*/
        return parse (new FileInputStream(file));
    }

    public Document parse(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(is);
    }


}
