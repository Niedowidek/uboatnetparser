package com.ravineteam.uboatnetparser.data;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

public class Marker {

    private Position position;
    private int number;
    private String type;
    private String date;
    private String textContent;
    private String textContentWithHtmlTags;
    private int startingIndex;
    private int endingIndex;

    public Position getPosition() {
        if (position == null) position = Position.getEmptyPosition();
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        if (StringUtils.isEmpty(this.textContent)) this.textContent = this.textContentWithHtmlTags != null ? Jsoup.parse(this.textContentWithHtmlTags).text() : "";
        this.textContent = textContent;
    }

    public String getTextContentWithHtmlTags() {
        return textContentWithHtmlTags;
    }

    public void setTextContentWithHtmlTags(String textContentWithHtmlTags) {
        this.textContentWithHtmlTags = textContentWithHtmlTags;
    }

    public int getStartingIndex() {
        return startingIndex;
    }

    public void setStartingIndex(int startingIndex) {
        this.startingIndex = startingIndex;
    }

    public int getEndingIndex() {
        return endingIndex;
    }

    public void setEndingIndex(int endingIndex) {
        this.endingIndex = endingIndex;
    }

    public boolean isNotEmpty() {
        return this.getPosition().isNotEmpty();
    }
}
