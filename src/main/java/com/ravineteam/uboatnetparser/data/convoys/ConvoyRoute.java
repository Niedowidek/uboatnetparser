package com.ravineteam.uboatnetparser.data.convoys;

import com.ravineteam.uboatnetparser.data.Position;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class ConvoyRoute {


    private String code;
    private String routeDescription;
    private String area;
    private String notes;

    private List<List<Position>> routeVariations;

    private List<Position> route;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRouteDescription() {
        return routeDescription;
    }

    public void setRouteDescription(String routeDescription) {
        this.routeDescription = routeDescription;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    private List<Position> getRouteList() {
        if (route == null) route = new LinkedList<>();
        return route;
    }

    public Position[] getRoute() {
        return this.getRouteList().toArray(new Position[0]);
    }

    public boolean addNextRoutePosition(Position position) {
        return position != null && position.isNotEmpty() && this.getRouteList().add(position);
    }

    private List<Position> getRouteVariationList (int index) {
        if (routeVariations == null) routeVariations = new LinkedList<>();
        if (routeVariations.size() == index) routeVariations.add(new LinkedList<>());
        if (routeVariations.size() > index) {
            return routeVariations.get(index);
        } else {
            return null;
        }
    }

    public Position[] getRouteVariation(int index) {
        List<Position> routeVariationList = getRouteVariationList(index);
        if (routeVariationList != null && routeVariationList.size() > index) {
            return routeVariationList.toArray(new Position[0]);
        } else return new Position[0];
    }

    public boolean addNextPositionToRouteVariation (int index, Position position) {
        List<Position> routeVariationList = getRouteVariationList(index);
        return routeVariationList != null && position != null && position.isNotEmpty() && routeVariationList.add(position);
    }

    public boolean isNotEmpty() {
        return getRoute().length > 0 && StringUtils.isNotEmpty(getCode());
    }

    public boolean isEmpty() {
        return !isNotEmpty();
    }


}
