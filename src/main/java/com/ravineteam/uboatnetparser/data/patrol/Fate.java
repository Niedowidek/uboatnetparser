package com.ravineteam.uboatnetparser.data.patrol;

import org.apache.commons.lang3.StringUtils;

public enum Fate {

    LOST("Lost"),
    EMPTY("");

    private final String fate;

    Fate(String fate) {
        this.fate = fate;
    }

    public String getFate() {
        return fate;
    }

    public static Fate getByName(String name) {
        if (StringUtils.isNotEmpty(name)) {
            for (Fate fate : Fate.values()) {
                if (fate.getFate().equals(name)) {
                    return fate;
                }
            }
        }
        return EMPTY;
    }
}
