package com.ravineteam.uboatnetparser.data.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.hasId;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import java.util.LinkedList;
import java.util.List;

public class Event implements hasId {

    private String id ;
    private String dateFormatted;
    private String date;
    private String text;
    private List<Href> hrefs;
    private List<String> cites;
    private Position position;
    private String markerTextWithHtmlTags;
    private String markerText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateFormatted() {
        if (StringUtils.isEmpty(dateFormatted)) {
            dateFormatted = new DateConverter().to_yyyyMMdd(date);
        }
        return dateFormatted;
    }

    public void setDateFormatted(String dateFormatted) {
        this.dateFormatted = dateFormatted;
    }

    public String getDate() {
        if (StringUtils.isEmpty(date)) {
            date = new DateConverter().to_d_MMM_yyyy(dateFormatted);
        }
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Href> getHrefs() {
        if (hrefs == null) hrefs = new LinkedList<>();
        return hrefs;
    }

    public void setHrefs(List<Href> hrefs) {
        this.hrefs = hrefs;
    }

    public List<String> getCites() {
        if (cites == null) cites = new LinkedList<>();
        return cites;
    }

    public void setCites(List<String> cites) {
        this.cites = cites;
    }

    public Position getPosition() {
        if (position == null) position = Position.getEmptyPosition();
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getMarkerTextWithHtmlTags() {
        return markerTextWithHtmlTags;
    }

    public void setMarkerTextWithHtmlTags(String markerTextWithHtmlTags) {
        this.markerTextWithHtmlTags = markerTextWithHtmlTags;
    }

    public String getMarkerText() {
        if(StringUtils.isEmpty(markerText)) markerText = markerTextWithHtmlTags != null ? Jsoup.parse(markerTextWithHtmlTags).text() : "";
        return markerText;
    }

    public void setMarkerText(String markerText) {
        this.markerText = markerText;
    }
}
