package com.ravineteam.uboatnetparser.data.patrol;

import com.ravineteam.uboatnetparser.data.hasId;
import org.apache.commons.lang3.StringUtils;

public class Officer implements hasId {

    private String id;
    private String name;
    private String rank;
    private String assignment;
    private String href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isEmpty() {
        return StringUtils.isAnyEmpty(getName(), getHref());
    }

    public boolean isNotEmpty() {
        return StringUtils.isNoneEmpty(getName(), getHref());
    }
}
