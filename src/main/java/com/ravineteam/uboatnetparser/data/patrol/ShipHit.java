package com.ravineteam.uboatnetparser.data.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.hasId;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import org.apache.commons.lang3.StringUtils;

public class ShipHit implements hasId {

    private String id;
    private String dateFormatted;
    private String dateLabel;
    private String uboat;
    private String uboatHref;
    private String commander;
    private String commanderHref;
    private String shipName;
    private String shipHref;
    private boolean damaged;
    private boolean captured;
    private boolean totalLoss;
    private boolean hitByMine;
    private int tonnage;
    private String nationalityHref;
    private String nationality;
    private String convoy;
    private String convoyHref;
    private String mapSymbol;
    private boolean hasPicture;

    private Position position;
    private String textFromMarker;
    private String textFromMarkerWithHtml;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateFormatted() {
        if (StringUtils.isEmpty(this.dateFormatted)) this.dateFormatted = new DateConverter().to_yyyyMMdd(this.dateLabel);
        return dateFormatted;
    }

    public void setDateFormatted(String dateFormatted) {
        this.dateFormatted = dateFormatted;
    }

    public String getDateLabel() {
        return dateLabel;
    }

    public void setDateLabel(String dateLabel) {
        this.dateLabel = dateLabel;
    }

    public String getUboat() {
        return uboat;
    }

    public void setUboat(String uboat) {
        this.uboat = uboat;
    }

    public String getUboatHref() {
        return uboatHref;
    }

    public void setUboatHref(String uboatHref) {
        this.uboatHref = uboatHref;
    }

    public String getCommander() {
        return commander;
    }

    public void setCommander(String commander) {
        this.commander = commander;
    }

    public String getCommanderHref() {
        return commanderHref;
    }

    public void setCommanderHref(String commanderHref) {
        this.commanderHref = commanderHref;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipHref() {
        return shipHref;
    }

    public void setShipHref(String shipHref) {
        this.shipHref = shipHref;
    }

    public boolean isDamaged() {
        return damaged;
    }

    public void setDamaged(boolean damaged) {
        this.damaged = damaged;
    }

    public boolean isCaptured() {
        return captured;
    }

    public void setCaptured(boolean captured) {
        this.captured = captured;
    }

    public int getTonnage() {
        return tonnage;
    }

    public boolean isTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(boolean totalLoss) {
        this.totalLoss = totalLoss;
    }

    public boolean isHitByMine() {
        return hitByMine;
    }

    public void setHitByMine(boolean hitByMine) {
        this.hitByMine = hitByMine;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = tonnage;
    }

    public String getNationalityHref() {
        return nationalityHref;
    }

    public void setNationalityHref(String nationalityHref) {
        this.nationalityHref = nationalityHref;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getConvoy() {
        return convoy;
    }

    public void setConvoy(String convoy) {
        this.convoy = convoy;
    }

    public String getConvoyHref() {
        return convoyHref;
    }

    public void setConvoyHref(String convoyHref) {
        this.convoyHref = convoyHref;
    }

    public String getMapSymbol() {
        return mapSymbol;
    }

    public void setMapSymbol(String mapSymbol) {
        this.mapSymbol = mapSymbol;
    }

    public boolean isHasPicture() {
        return hasPicture;
    }

    public void setHasPicture(boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getTextFromMarker() {
        return textFromMarker;
    }

    public void setTextFromMarker(String textFromMarker) {
        this.textFromMarker = textFromMarker;
    }

    public String getTextFromMarkerWithHtml() {
        return textFromMarkerWithHtml;
    }

    public void setTextFromMarkerWithHtml(String textFromMarkerWithHtml) {
        this.textFromMarkerWithHtml = textFromMarkerWithHtml;
    }

    public boolean isSunk() {
        return !this.isDamaged() && !this.isDamaged();
    }
}
