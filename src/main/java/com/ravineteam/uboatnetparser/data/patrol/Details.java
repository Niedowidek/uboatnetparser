package com.ravineteam.uboatnetparser.data.patrol;

import com.ravineteam.uboatnetparser.data.Position;
import org.apache.commons.lang3.StringUtils;

public class Details {

    private String date;
    private String uboatName;
    private Position positionFromMarker;
    private Position positionFromDescription;
    private String patrolStartDate;
    private String patrolEndDate;
    private String patrolHref;

    public boolean hasPosition() {
        return (positionFromMarker != null && positionFromMarker.isNotEmpty())
                || (positionFromDescription != null && positionFromDescription.isNotEmpty());
    }

    public boolean isNotEmpty() {
        return StringUtils.isNoneEmpty(uboatName, date) && hasPosition();
    }

    public boolean isEmpty() {
        return !isNotEmpty();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Position getPositionFromMarker() {
        return positionFromMarker;
    }

    public void setPositionFromMarker(Position positionFromMarker) {
        this.positionFromMarker = positionFromMarker;
    }

    public Position getPositionFromDescription() {
        return positionFromDescription;
    }

    public void setPositionFromDescription(Position positionFromDescription) {
        this.positionFromDescription = positionFromDescription;
    }

    public String getPatrolStartDate() {
        return patrolStartDate;
    }

    public void setPatrolStartDate(String patrolStartDate) {
        this.patrolStartDate = patrolStartDate;
    }

    public String getPatrolEndDate() {
        return patrolEndDate;
    }

    public void setPatrolEndDate(String patrolEndDate) {
        this.patrolEndDate = patrolEndDate;
    }

    public String getUboatName() {
        return uboatName;
    }

    public void setUboatName(String uboatName) {
        this.uboatName = uboatName;
    }

    public String getPatrolHref() {
        return patrolHref;
    }

    public void setPatrolHref(String patrolHref) {
        this.patrolHref = patrolHref;
    }
}
