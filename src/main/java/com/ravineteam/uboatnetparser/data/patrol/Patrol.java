package com.ravineteam.uboatnetparser.data.patrol;

import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.data.hasId;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class Patrol implements hasId {

    private String id;
    private String href;
    private String patrolStartDate;
    private String patrolStartPlace;
    private String endDate;
    private String endPlace;
    private String fate;
    private String duration;

    private List<Officer> officers;
    private List<DailyPosition> dailyPositions;
    private List<ShipHit> shipHitList;

    private Tonnage tonnage;
    private List<Event> events;
    private List<Attack> attacks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getStartDate() {
        return patrolStartDate;
    }

    public void setStartDate(String startDate) {
        this.patrolStartDate = startDate;
    }

    public String getStartPlace() {
        return patrolStartPlace;
    }

    public void setStartPlace(String startPlace) {
        this.patrolStartPlace = startPlace;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public String getFate() {
        return fate;
    }

    public void setFate(String fate) {
        this.fate = fate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getDurationInDays() {
        if (StringUtils.isNotEmpty(duration)) {
            try {
                String durationTemp = duration.replace(" days","");
                return Integer.parseInt(duration);
            } catch (NumberFormatException e) {
                System.err.println("Duration not expressed in days. Error while parsing.");
            }
        }
        return -1;
    }

    public List<Officer> getOfficers() {
        return officers;
    }

    public void setOfficers(List<Officer> officers) {
        this.officers = officers;
    }

    public List<DailyPosition> getDailyPositions() {
        return dailyPositions;
    }

    public void setDailyPositions(List<DailyPosition> dailyPositions) {
        this.dailyPositions = dailyPositions;
    }

    public List<ShipHit> getShipHitList() {
        return shipHitList;
    }

    public void setShipHitList(List<ShipHit> shipHitList) {
        this.shipHitList = shipHitList;
    }

    public Tonnage getTonnage() {
        return tonnage;
    }

    public void setTonnage(Tonnage tonnage) {
        this.tonnage = tonnage;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Attack> getAttacks() {
        return attacks;
    }

    public void setAttacks(List<Attack> attacks) {
        this.attacks = attacks;
    }
}
