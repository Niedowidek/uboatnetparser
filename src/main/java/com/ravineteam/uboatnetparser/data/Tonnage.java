package com.ravineteam.uboatnetparser.data;

public class Tonnage {

    private int shipsSunk;
    private int shipsSunkTonnage;
    private int shipsDamaged;
    private int shipsDamagedTonnage;
    private int shipsTotalLoss;
    private int shipsTotalLossTonnage;

    private int warshipsSunk;
    private int warshipsSunkTonnage;
    private int warshipsDamaged;
    private int warshipsDamagedTonnage;
    private int warshipsTotalLoss;
    private int warshipsTotalLossTonnage;

    public int getShipsSunk() {
        return shipsSunk;
    }

    public void setShipsSunk(int shipsSunk) {
        this.shipsSunk = shipsSunk;
    }

    public int getShipsSunkTonnage() {
        return shipsSunkTonnage;
    }

    public void setShipsSunkTonnage(int shipsSunkTonnage) {
        this.shipsSunkTonnage = shipsSunkTonnage;
    }

    public int getShipsDamaged() {
        return shipsDamaged;
    }

    public void setShipsDamaged(int shipsDamaged) {
        this.shipsDamaged = shipsDamaged;
    }

    public int getShipsDamagedTonnage() {
        return shipsDamagedTonnage;
    }

    public void setShipsDamagedTonnage(int shipsDamagedTonnage) {
        this.shipsDamagedTonnage = shipsDamagedTonnage;
    }

    public int getShipsTotalLoss() {
        return shipsTotalLoss;
    }

    public void setShipsTotalLoss(int shipsTotalLoss) {
        this.shipsTotalLoss = shipsTotalLoss;
    }

    public int getShipsTotalLossTonnage() {
        return shipsTotalLossTonnage;
    }

    public void setShipsTotalLossTonnage(int shipsTotalLossTonnage) {
        this.shipsTotalLossTonnage = shipsTotalLossTonnage;
    }

    public int getWarshipsSunk() {
        return warshipsSunk;
    }

    public void setWarshipsSunk(int warshipsSunk) {
        this.warshipsSunk = warshipsSunk;
    }

    public int getWarshipsSunkTonnage() {
        return warshipsSunkTonnage;
    }

    public void setWarshipsSunkTonnage(int warshipsSunkTonnage) {
        this.warshipsSunkTonnage = warshipsSunkTonnage;
    }

    public int getWarshipsDamaged() {
        return warshipsDamaged;
    }

    public void setWarshipsDamaged(int warshipsDamaged) {
        this.warshipsDamaged = warshipsDamaged;
    }

    public int getWarshipsDamagedTonnage() {
        return warshipsDamagedTonnage;
    }

    public void setWarshipsDamagedTonnage(int warshipsDamagedTonnage) {
        this.warshipsDamagedTonnage = warshipsDamagedTonnage;
    }

    public int getWarshipsTotalLoss() {
        return warshipsTotalLoss;
    }

    public void setWarshipsTotalLoss(int warshipsTotalLoss) {
        this.warshipsTotalLoss = warshipsTotalLoss;
    }

    public int getWarshipsTotalLossTonnage() {
        return warshipsTotalLossTonnage;
    }

    public void setWarshipsTotalLossTonnage(int warshipsTotalLossTonnage) {
        this.warshipsTotalLossTonnage = warshipsTotalLossTonnage;
    }
}
