package com.ravineteam.uboatnetparser.data.crews;

import java.util.LinkedList;
import java.util.List;

public class TrainedCommander {

    private String href;
    private String name;
    private String rank;
    private List<Command> commandList;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<Command> getCommandList() {
        if (commandList == null) commandList = new LinkedList<>();
        return commandList;
    }

    public void setCommandList(List<Command> commandList) {
        this.commandList = commandList;
    }
}
