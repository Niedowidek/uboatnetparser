package com.ravineteam.uboatnetparser.data.crews;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Crew {

    private String name;
    private String href;
    private LocalDate year;
    private List<TrainedCommander> trainedCommanderList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public LocalDate getYear() {
        return year;
    }

    public void setYear(LocalDate year) {
        this.year = year;
    }

    public List<TrainedCommander> getTrainedCommanderList() {
        if (trainedCommanderList == null) trainedCommanderList = new LinkedList<>();
        return trainedCommanderList;
    }

    public void setTrainedCommanderList(List<TrainedCommander> trainedCommanderList) {
        this.trainedCommanderList = trainedCommanderList;
    }

    public boolean isNotEmpty() {
        return StringUtils.isNoneEmpty( getName())
                && getTrainedCommanderList() != null
                && getTrainedCommanderList().size() > 0;
    }
}
