package com.ravineteam.uboatnetparser.data.men;

import com.ravineteam.uboatnetparser.data.Tonnage;
import org.apache.commons.lang3.StringUtils;

public class Man {

    private String id;
    private String href;
    private String name;
    private Tonnage tonnage;
    private String crewName;
    private String crewHref;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tonnage getTonnage() {
        return tonnage;
    }

    public void setTonnage(Tonnage tonnage) {
        this.tonnage = tonnage;
    }

    public String getCrewName() {
        return crewName;
    }

    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    public String getCrewHref() {
        return crewHref;
    }

    public void setCrewHref(String crewHref) {
        this.crewHref = crewHref;
    }

    public boolean isNotEmpty() {
        return StringUtils.isNoneEmpty(name);
    }
}
