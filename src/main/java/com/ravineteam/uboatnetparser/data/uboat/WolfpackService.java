package com.ravineteam.uboatnetparser.data.uboat;

import com.ravineteam.uboatnetparser.utils.DateConverter;
import org.apache.commons.lang3.StringUtils;

public class WolfpackService {

    private String href;
    private String name;
    private String dateFromFormatted;
    private String dateToFormatted;
    private String dateFrom;
    private String dateTo;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateFromFormatted() {
        if (StringUtils.isEmpty(dateFromFormatted)) {
            dateFromFormatted = new DateConverter().to_yyyyMMdd(dateFrom);
        }
        return dateFromFormatted;
    }

    public void setDateFromFormatted(String dateFromFormatted) {
        this.dateFromFormatted = dateFromFormatted;
    }

    public String getDateToFormatted() {
        if (StringUtils.isEmpty(dateToFormatted)) {
            dateToFormatted = new DateConverter().to_yyyyMMdd(dateTo);
        }
        return dateToFormatted;
    }

    public void setDateToFormatted(String dateToFormatted) {
        this.dateToFormatted = dateToFormatted;
    }

    public String getDateFrom() {
        if (StringUtils.isEmpty(dateFrom)) {
            dateFrom = new DateConverter().to_d_MMM_yyyy(dateFromFormatted);
        }
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        if (StringUtils.isEmpty(dateTo)) {
            dateTo = new DateConverter().to_d_MMM_yyyy(dateToFormatted);
        }
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
}
