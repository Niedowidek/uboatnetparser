package com.ravineteam.uboatnetparser.data.uboat;

public class Fate {

    private String fateLabel;
    private String when;
    private String description;

    public String getLabel() {
        return fateLabel;
    }

    public void setLabel(String label) {
        this.fateLabel = label;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
