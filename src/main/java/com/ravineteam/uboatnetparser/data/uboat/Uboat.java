package com.ravineteam.uboatnetparser.data.uboat;

import com.ravineteam.uboatnetparser.data.Position;
import com.ravineteam.uboatnetparser.data.Tonnage;
import com.ravineteam.uboatnetparser.data.hasId;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Uboat implements hasId {

    private String id;

    private String href;

    private String number;
    private String type;
    private String ordered;
    private String laidDownWhen;
    private String laidDownWhere;
    private String laidDownWhereHref;
    private String launched;
    private String commisioned;
    private String commisionedCommander;
    private Collection<AssignedCommander> assignedCommanders;
    private Collection<ServedFlotilla> servedFlotillas;
    private Tonnage successes;
    private Collection<Fate> fates;
    private Position finalPosition;
    private List<WolfpackService> wolfpacks;

    public boolean isNotEmpty() {
        return StringUtils.isNoneEmpty(number, type);
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrdered() {
        return ordered;
    }

    public void setOrdered(String ordered) {
        this.ordered = ordered;
    }

    public String getLaidDownWhen() {
        return laidDownWhen;
    }

    public void setLaidDownWhen(String laidDownWhen) {
        this.laidDownWhen = laidDownWhen;
    }

    public String getLaidDownWhere() {
        return laidDownWhere;
    }

    public void setLaidDownWhere(String laidDownWhere) {
        this.laidDownWhere = laidDownWhere;
    }

    public String getLaidDownWhereHref() {
        return laidDownWhereHref;
    }

    public void setLaidDownWhereHref(String laidDownWhereHref) {
        this.laidDownWhereHref = laidDownWhereHref;
    }

    public String getLaunched() {
        return launched;
    }

    public void setLaunched(String launched) {
        this.launched = launched;
    }

    public String getCommisioned() {
        return commisioned;
    }

    public void setCommisioned(String commisioned) {
        this.commisioned = commisioned;
    }

    public String getCommisionedCommander() {
        return commisionedCommander;
    }

    public void setCommisionedCommander(String commisionedCommander) {
        this.commisionedCommander = commisionedCommander;
    }

    public Collection<AssignedCommander> getAssignedCommanders() {
        if (assignedCommanders == null) {
            assignedCommanders = new LinkedList<>();
        }
        return assignedCommanders;
    }

    public void setAssignedCommanders(Collection<AssignedCommander> assignedCommanders) {
        this.assignedCommanders = assignedCommanders;
    }

    public Collection<ServedFlotilla> getServedFlotillas() {
        if (servedFlotillas == null) {
            servedFlotillas = new LinkedList<>();
        }
        return servedFlotillas;
    }

    public void setServedFlotillas(Collection<ServedFlotilla> servedFlotillas) {
        this.servedFlotillas = servedFlotillas;
    }

    public Tonnage getSuccesses() {
        return successes;
    }

    public void setSuccesses(Tonnage successes) {
        this.successes = successes;
    }

    public Collection<Fate> getFates() {
        if (fates == null) {
            fates = new LinkedList<>();
        }
        return fates;
    }

    public void setFates(Collection<Fate> fates) {
        this.fates = fates;
    }

    public Position getFinalPosition() {
        return finalPosition;
    }

    public void setFinalPosition(Position finalPosition) {
        this.finalPosition = finalPosition;
    }

    public List<WolfpackService> getWolfpacks() {
        return wolfpacks;
    }

    public void setWolfpacks(List<WolfpackService> wolfpacks) {
        this.wolfpacks = wolfpacks;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
