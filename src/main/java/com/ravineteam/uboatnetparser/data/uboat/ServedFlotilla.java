package com.ravineteam.uboatnetparser.data.uboat;

public class ServedFlotilla {

    private String href;
    private String dateFrom;
    private String dateTo;
    private String id;
    private String typeOfDuty;

    public ServedFlotilla(String dateFrom, String dateTo, String id, String typeOfDuty) {
        this(dateFrom, dateTo, id, typeOfDuty, "");
    }

    public ServedFlotilla(String dateFrom, String dateTo, String id, String typeOfDuty, String href) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.id = id;
        this.typeOfDuty = typeOfDuty;
        this.href  = href;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeOfDuty() {
        return typeOfDuty;
    }

    public void setTypeOfDuty(String typeOfDuty) {
        this.typeOfDuty = typeOfDuty;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
