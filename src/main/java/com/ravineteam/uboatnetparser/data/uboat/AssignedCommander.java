package com.ravineteam.uboatnetparser.data.uboat;


import java.util.List;

public class AssignedCommander {

    private String href;
    private String commanderId;
    private String rank;
    private List<String> decorationIds;
    private String dateFrom;
    private String dateTo;

    public AssignedCommander(String commanderId, String rank, List<String> decorationIds, String dateFrom, String dateTo) {
        this.commanderId = commanderId;
        this.rank = rank;
        this.decorationIds = decorationIds;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getCommanderId() {
        return commanderId;
    }

    public void setCommanderId(String commanderId) {
        this.commanderId = commanderId;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<String> getDecorationIds() {
        return decorationIds;
    }

    public void setDecorationIds(List<String> decorationIds) {
        this.decorationIds = decorationIds;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
}
