package com.ravineteam.uboatnetparser.data;

import org.apache.commons.lang3.StringUtils;

public class Position {

    private double  latitude;
    private double longitude;

    public static final double ALLOWED_ERROR = 0.0099;
    public static final double EMPTY = 999.0;

    public static Position getInstance(String separator, String positionsFragment) {
        if (StringUtils.contains(positionsFragment, separator)) {
            String[] coords = positionsFragment.split(String.valueOf(separator));
            if (coords != null && coords.length == 2) {
                return new Position(coords[0], coords[1]);
            }
        }
        return getEmptyPosition();
    }

    public Position(String latitude, String longitude) {
        try {
            this.latitude = Double.parseDouble(latitude);
            this.longitude = Double.parseDouble(longitude);
        } catch (NumberFormatException e) {
            this.latitude = EMPTY;
            this.longitude = EMPTY;
        }
    }

    public Position(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static Position getEmptyPosition() {
        return new Position(Position.EMPTY, Position.EMPTY);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isEmpty() {
        return (latitude < -90 || latitude > 90)
                || (longitude < -180 || longitude > 180);
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }
}
