package com.ravineteam.uboatnetparser.data.shiplosses;

import com.ravineteam.uboatnetparser.data.patrol.ShipHit;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class MonthlyShipLosses {

    private LocalDate startDate;
    private List<ShipHit> shipHitList;

    public boolean isNotEmpty() {
        return startDate != null && getShipsHit() != null && getShipsHit().size() > 0;
    }

    public boolean isEmpty() {
        return !isNotEmpty();
    }

    public int getNumberOfAllShipsHit() {
        return getShipsHit().size();
    }

    public List<ShipHit> getShipsHit() {
        if (shipHitList == null) shipHitList = new LinkedList<>();
        return shipHitList;
    }

    public void setShipHitList(List<ShipHit> shipHitList) {
        this.shipHitList = shipHitList;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public List<ShipHit> getShipsSunk() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isSunk()) list.add(s);
        }
        return list;
    }

    public List<ShipHit> getShipsDamaged() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isDamaged()) list.add(s);
        }
        return list;
    }

    public int getShipsSunkTonnage() {
        int retVal = 0;
        for (ShipHit s : getShipsHit()) {
            if (s.isSunk()) retVal += s.getTonnage();
        }
        return retVal;
    }

    public int getShipsDamagedTonnage() {
        int retVal = 0;
        for (ShipHit s : getShipsHit()) {
            if (s.isDamaged()) retVal += s.getTonnage();
        }
        return retVal;
    }

    public List<ShipHit> getHitByMine() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isHitByMine()) list.add(s);
        }
        return list;
    }

    public List<ShipHit> getDamagedByMine() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isHitByMine() && s.isDamaged()) list.add(s);
        }
        return list;
    }

    public List<ShipHit> getSunkByMine() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isHitByMine() && s.isSunk()) list.add(s);
        }
        return list;
    }

    public List<ShipHit> getCaptured() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isCaptured()) list.add(s);
        }
        return list;
    }

    public List<ShipHit> getTotalLoss() {
        List<ShipHit> list = new LinkedList<>();
        for (ShipHit s : getShipsHit()) {
            if (s.isTotalLoss()) list.add(s);
        }
        return list;
    }
}
