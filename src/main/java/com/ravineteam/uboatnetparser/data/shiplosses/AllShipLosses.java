package com.ravineteam.uboatnetparser.data.shiplosses;

import com.ravineteam.uboatnetparser.data.patrol.ShipHit;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class AllShipLosses {

    List<MonthlyShipLosses> monthlyShipLossesList;

    public boolean isNotEmpty() {
        return monthlyShipLossesList != null && monthlyShipLossesList.size() > 0;
    }

    public boolean isEmpty() {
        return true;
    }

    public int getNumberOfMonths() {
        if (monthlyShipLossesList != null)
            return monthlyShipLossesList.size();
        else return 0;
    }

    public int getNumberOfAllShipsHit() {
        int retVal = 0;
        for (MonthlyShipLosses m : monthlyShipLossesList) {
            if (m != null && m.getShipsHit() != null) {
                retVal += m.getShipsHit().size();
            }
        }
        return  retVal;
    }

    public int getNumberOfShipsHit(LocalDate date) {
        int retVal = 0;
        List<ShipHit> list = this.getShipsHit(date);
        if (list != null) retVal = list.size();
        return retVal;
    }

    public List<ShipHit> getShipsHit(LocalDate date) {
        List<ShipHit> retVal = new LinkedList<>();
        for (MonthlyShipLosses m : monthlyShipLossesList) {
            if (m.getStartDate().getMonthValue() == date.getMonthValue() && m.getStartDate().getYear() == date.getYear() ) {
                retVal = m.getShipsHit();
            }
        }
        return retVal;
    }

    public List<MonthlyShipLosses> getMonthlyShipLossesList() {
        if (monthlyShipLossesList == null ) monthlyShipLossesList = new LinkedList<>();
        return monthlyShipLossesList;
    }

    public void enumerate() {
        int counter = 1;
        for (MonthlyShipLosses m : monthlyShipLossesList) {
            for (ShipHit s : m.getShipsHit()) {
                s.setId(String.valueOf(counter++));
            }
        }
    }

    public boolean addMonthlyShipLossess(MonthlyShipLosses monthlyShipLosses) {
        return this.getMonthlyShipLossesList().add(monthlyShipLosses);
    }
}
