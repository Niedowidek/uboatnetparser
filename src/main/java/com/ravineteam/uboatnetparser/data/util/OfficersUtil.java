package com.ravineteam.uboatnetparser.data.util;

import com.ravineteam.uboatnetparser.data.patrol.Officer;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class OfficersUtil {

    public Officer getByNamePart(List<Officer> officerList, String name) {
        Officer retVal = new Officer();
        if (officerList != null && !officerList.isEmpty()) {
            for (Officer officer : officerList) {
                if(officer != null && StringUtils.containsIgnoreCase(officer.getName(), name)) {
                    retVal = officer; break;
                }
            }

        }
        return retVal;
    }
}
