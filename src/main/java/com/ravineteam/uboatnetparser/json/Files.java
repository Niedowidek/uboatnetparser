package com.ravineteam.uboatnetparser.json;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Files {

    List<File> getFiles(String fileName) {
        List<File> filesToProcess = new LinkedList<>();
        List<String> filenames = new LinkedList<>();
        try (Stream<String> stream = java.nio.file.Files.lines(Paths.get(fileName))) {
             filenames = stream.collect(Collectors.toList());
        } catch (IOException e) {
            System.err.println("Problem with getting list of files to be parsed.");
        }
        for (String f : filenames) {
            if (StringUtils.isNotEmpty(f)) {
                filesToProcess.add(new File(f));
            }
        }
        return filesToProcess;
    }
}
