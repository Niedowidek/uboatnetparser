package com.ravineteam.uboatnetparser.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ravineteam.uboatnetparser.data.uboat.Fate;
import com.ravineteam.uboatnetparser.data.uboat.ServedFlotilla;
import com.ravineteam.uboatnetparser.data.uboat.Uboat;
import com.ravineteam.uboatnetparser.parsers.uboat.UboatParser;
import com.ravineteam.uboatnetparser.utils.DateConverter;
import com.ravineteam.uboatnetparser.xml.XmlParser;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

class UboatJson {

    private String HEADERS_WITH_DATE = "WKT,date\n";
    private String HEADERS_WITH_DATE_AND_UBOATNUMBER = "WKT,date,uboat\n";

    public static void main(String[] args) throws FileNotFoundException {
        List<File> filesToProcess = new Files().getFiles("src/main/resources/input/uboatlist.txt");
        new UboatJson().process(filesToProcess);
    }

    public void process(List<File> fileList) {
        UboatParser parser = new UboatParser();
        XmlParser xmlParser = new XmlParser();
        List<Uboat> uboatList = getUboats(fileList, parser, xmlParser);
        saveSunkPositions(uboatList);
        saveJson(uboatList);
    }

    private void saveJson(List<Uboat> uboatList) {
        String filename = "src/main/resources/output/uboats"+ LocalDateTime.now().toString().replace(":","")+".json";
        try (OutputStream os = new FileOutputStream(new File(filename))) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            os.write(gson.toJson(uboatList).getBytes());
        } catch (IOException e) {
            System.err.println(e.getCause().getMessage());
        }
    }

    private void saveSunkPositions(List<Uboat> uboatList) {
        StringBuilder common = new StringBuilder();
        StringBuilder commonWithUboatNames = new StringBuilder();
        StringBuilder multipoint = new StringBuilder("MULTIPOINT(");
        int counter = 1;
        Map<String, List<String>> positions = new HashMap<>();
        for (Uboat u : uboatList) {
            u.setId(String.valueOf(counter++));
            DateConverter dateConverter = new DateConverter();
            if (u.getFinalPosition() != null && u.getFinalPosition().isNotEmpty() && u.getFates() != null && u.getFates().size() > 0) {
//                for (Fate f : u.getFates()) {
                    LocalDate ld = null;
//                    if (StringUtils.isNotEmpty(f.getLabel()) && StringUtils.equals("sunk", f.getLabel())) {
//                        if (StringUtils.isNotEmpty(f.getWhen())) {
//                            String when = f.getWhen();
//                            LocalDate ld = dateConverter.getLocalDateFrom_d_MMM_YYYY(when);
//                            if (ld == null) {
                                Collection flotillas = u.getServedFlotillas();
                                if (flotillas != null && flotillas.size() > 0) {
                                    ServedFlotilla lastFlotilla = (ServedFlotilla) flotillas.toArray()[flotillas.size() - 1];
                                    if (lastFlotilla != null) {
                                        ld = dateConverter.getLocalDateFrom_d_MMM_YYYY(lastFlotilla.getDateTo());
                                        if (ld == null) {
                                            System.err.println("Cannot parse end date for " + u.getNumber() +" : " + lastFlotilla.getDateTo());
                                        }
                                    }
                                }
//                            }
//                            if (ld != null) {
                                String keydate = "no_date";
                                if (ld != null) {
                                    keydate = ld.format(DateTimeFormatter.ISO_LOCAL_DATE);
                                }
                                String content = "POINT (" + u.getFinalPosition().getLongitude() + " " + u.getFinalPosition().getLatitude() + ")";
                                multipoint.append("(").append(u.getFinalPosition().getLongitude()).append(" ").append(u.getFinalPosition().getLatitude()).append("),");
                                common.append(content.replace("\n","")).append(", ").append(keydate).append("\n");
                                content = content + ", " + u.getNumber();
                                commonWithUboatNames.append(content).append(", ").append(keydate).append(", ").append(u.getNumber()).append("\n");
                                List<String> values = positions.get(keydate);
                                if (values != null) {
                                    values.add(content);
                                } else {
                                    List<String> newValue = new LinkedList<>();
                                    newValue.add(content);
                                    positions.put(keydate, newValue);
                                }
                                /*try (OutputStream os = new FileOutputStream(new File(filename))) {
                                    os.write(content.getBytes());
                                } catch (FileNotFoundException e) {
                                    System.err.println("Cannot open output stream for file " + filename);
                                } catch (IOException e) {
                                    System.err.println("Cannot write fate-sunk data to file " + filename);
                                }*/
//                            }
                        }
//                    }
                }
//            }

        for (String key : positions.keySet()) {
            List<String> values = positions.get(key);
            String filename = "src/main/resources/output/lastPosition_"+ key + ".wkt";
            String filenameWithUboatName = "src/main/resources/output/lastPositionWithUboatName_"+ key + ".wkt";
            try (OutputStream os = new FileOutputStream(new File(filename));
                 OutputStream osWithUboatName = new FileOutputStream(filenameWithUboatName)) {
                int valuesCounter = 0;
                os.write(HEADERS_WITH_DATE.getBytes());
                osWithUboatName.write(HEADERS_WITH_DATE_AND_UBOATNUMBER.getBytes());
                valuesCounter++;
                for (String content : values) {
                    if (valuesCounter > 0) {
                        content = "\n" + content;
                    }
                    osWithUboatName.write(content.getBytes());
                    int i = content.indexOf("\"");
                    if(i>0) {
                        content = content.substring(0,i);
                    }
                    os.write(content.getBytes());
                    valuesCounter++;
                }
            } catch (FileNotFoundException e) {
                System.err.println("Cannot open output stream for file " + filename);
            } catch (IOException e) {
                System.err.println("Cannot write fate-sunk data to file " + filename);
            }
        }
        String commonFilename = "src/main/resources/output/lastPositions.wkt";
        String commonFilenameWithUboatNames = "src/main/resources/output/lastPositionsWithUboatNames.wkt";
        String multipointFilename = "src/main/resources/output/lastPositionsMultipoint.wkt";
        try (OutputStream os = new FileOutputStream(new File(commonFilename));
             OutputStream osWithUboatName = new FileOutputStream(new File(commonFilenameWithUboatNames));
            OutputStream osMultipoint = new FileOutputStream(new File(multipointFilename))) {
            os.write(HEADERS_WITH_DATE.getBytes());
            osWithUboatName.write(HEADERS_WITH_DATE_AND_UBOATNUMBER.getBytes());
            os.write(common.toString().substring(0,common.length() - 1).getBytes());
            osWithUboatName.write(commonWithUboatNames.toString().substring(0, commonWithUboatNames.length() - 1).getBytes());
            osMultipoint.write((multipoint.substring(0, multipoint.length() - 1) + ")").getBytes());;
        } catch (IOException e) {
            System.err.println("Cannot write fate-sunk data to common file " + commonFilename);
        }
            /*if (u.getFinalPosition() != null && u.getFinalPosition().isNotEmpty()) {
                String line = "POINT (" + u.getFinalPosition().getLatitude() + " " + u.getFinalPosition().getLongitude() + ")\n";
            }*/
        }
//    }

    private List<Fate> getFates(List<Uboat> uboatList) {
        List<Fate> sunkList = new LinkedList<>();
        for (Uboat u : uboatList) {
            Collection<Fate> fateList = u.getFates();
            if (fateList != null && fateList.size() > 0) {
                for (Fate f : fateList) {
                    if (StringUtils.equals("sunk", f.getLabel())) {
                        sunkList.add(f);
                    }
                }
            }
        }
        return sunkList;
    }

    private List<Uboat> getUboats(List<File> fileList, UboatParser parser, XmlParser xmlParser) {
        List<Uboat> uboatList = new LinkedList<>();
        for (File file : fileList) {
            try {
//                Document doc = xmlParser.parse(file);
                Uboat uboat = parser.getUboat(file);
                if (uboat != null) uboatList.add(uboat);
                else System.err.println(file.getName() + ": Cannot parse uboat data");
            } catch (IOException | SAXException | ParserConfigurationException e  ) {
                System.err.println(file.getName() + ": " + e.getCause().getMessage());
            }
        }
        return uboatList;
    }

    void writeJson(Uboat uboat, OutputStream os) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        os.write(gson.toJson(uboat).getBytes());
    }

}
