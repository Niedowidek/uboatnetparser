package com.ravineteam.uboatnetparser.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ravineteam.uboatnetparser.data.patrol.Details;

import java.io.IOException;
import java.io.OutputStream;

public class DetailsJson {

    public void writeJson(Details details, OutputStream os) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        os.write(gson.toJson(details).getBytes());
    }
}
